var fs = require('fs'),
	system = require('system'),
	webpage = require('webpage').create();

/**
 *
 * 200 : Success
 * 300 : Page Redirected
 * 400 : Invalid URL
 * 406 : Not Allowed API. Please check Usage
 * 500 : Internal Server error (failed to run api)
 *
**/

var API_200_SUCCESS         = 200;
var API_300_REDIRECTED_PAGE = 300;
var API_400_INVALID_URL     = 400;
var API_406_NOT_ALLOWED     = 406;
var API_500_SERVER_ERROR    = 500;

var PHANTOMJS_SUCCESS = 'success';

function logger(result) {
	console.log("Status:"+result);
}
function returnValue(result) {
	console.log(result);
}

function captureUrl(url, dest) {
    var webpage = require('webpage').create();

	var capture_content = new Object();
    var redirect = false

	capture_content.url = url;
	capture_content.dest = dest;

    webpage.onNavigationRequested = function(url, type, willNavigate, main) {
        if (main && url!=paramURL) {
            paramURL = url;
            redirect = true
            webpage.close();
            captureUrl(url,dest);
        }
    };

	webpage.open(url, function(status){
		if(status !== PHANTOMJS_SUCCESS) {
			logger(API_400_INVALID_URL);
            setTimeout(function()
            {
                phantom.exit(400);
            },0);
		} else {
		}
	});

    webpage.onLoadFinished = function(){
        if(redirect){
            //captureUrl(url,dest);
        }
        else{
            var render = webpage.render(dest);
            logger(API_200_SUCCESS)
            returnValue(JSON.stringify(capture_content));
            setTimeout(function()
            {
                phantom.exit(0);
            },0);
        }
    }
}

function crawl(url, dest) {
	var crawl_content = new Object();

	// init
	crawl_content.url = url;
	crawl_content.dest = fs.absolute(dest);
	crawl_content.pages = [];

	
	webpage.onUrlChanged = function () {
		// reset
		crawl_content.url = webpage.url;
		crawl_content.pages = [];
	}

	webpage.onResourceRequested = function (requestData) {
		crawl_content.pages.push(requestData.url);
	}

	webpage.onLoadFinished = function (status) {
		if (status === 'success')
			webpage.close();
		else { 
			logger(API_400_INVALID_URL);
            setTimeout(function()
            {
                phantom.exit(400);
            },0);
		}
	};
	webpage.onClosing = function(closingPage) {
		//save html file to destination
        fs.write(crawl_content.dest, closingPage.content, 'w');
        
        logger(API_200_SUCCESS);
        returnValue(JSON.stringify(crawl_content));
        setTimeout(function()
        {
            phantom.exit(0);
        },0);
	};

	webpage.open(url, function(status) {
		if (status !== PHANTOMJS_SUCCESS) {
			logger(API_400_INVALID_URL);
            setTimeout(function()
            {
                phantom.exit(400);
            },0);
		}
	});

}

/*  Main

@parameter
	args[1] : capture / crawl
	args[2] : URL
	args[3] : destination : absolute path
*/
if( system.args.length < 4 ) {
	logger(API_406_NOT_ALLOWED);
} else {
	var api = system.args[1].toLowerCase();
	var paramURL = system.args[2];
	var dest = system.args[3];

	switch(api) {
		case 'capture' :
			captureUrl(paramURL, dest);
			break;
		case 'crawl' :
			crawl(paramURL, dest);
			break;
		default : 
			logger(API_406_NOT_ALLOWED);
            setTimeout(function()
            {
                phantom.exit(406);
            },0);
			break;
	} 	
}

