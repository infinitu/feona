package com.feona.feo.optimize.minify

import java.net.URL

import com.feona.feo.webcontents.impl.CSSContent
import org.scalatest._
import play.api.test._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
 * Created by CK-Kim on 2014. 8. 21..
 */
class MinifySpec extends FlatSpec with Matchers{

  "JSMinify" should "Compile Javascript Code with Closure" in  new WithApplication{
    private val originCode: String =
      """
        |// ==ClosureCompiler==
        |// @compilation_level SIMPLE_OPTIMIZATIONS
        |// @output_file_name default.js
        |// ==/ClosureCompiler==
        |
        |// ADD YOUR CODE HERE
        |function hello(name) {
        |  alert('Hello, ' + name);
        |}
        |hello('New user');
        |
        |
      """.stripMargin
    private val expected = """function hello(a){alert("Hello, "+a)}hello("New user");"""
    JSMinify.JSMinify(originCode).onComplete{
      case Success(str)=>
        str should be (expected)
      case Failure(t)=>
        fail(t)
    }
  }

  "CSSMinify" should "minify CSS Code with phloc Compressor" in {
    val originCode =
      """
        |@charset "utf-8";
        |
        |/**********************************************************
        | commnet : 04. Customer Center
        |**********************************************************/
        |
        |/* 고객센터 집결지 */
        |.list-customer {overflow:hidden;width:100%;}
        |.list-customer li {float:left;width:140px;}
        |.list-customer li a {display:block;padding:90px 0 0;height:30px;text-align:center;}
        |.list-customer li a:hover, .list-customer li a:focus {height:29px;border-bottom:1px solid #424242;}
        |.list-customer li.bg-cust1 a {background:url(../img/com/bg_icon2_1.png) no-repeat 50% 17px;}
        |.list-customer li.bg-cust2 a {background:url(../img/com/bg_icon2_2.png) no-repeat 50% 17px;}
        |.list-customer li.bg-cust3 a {background:url(../img/com/bg_icon2_3.png) no-repeat 50% 17px;}
      """.stripMargin

    val expected = """.list-customer{overflow:hidden;width:100%}.list-customer li{float:left;width:140px}.list-customer li a{display:block;padding:90px 0 0;height:30px;text-align:center}.list-customer li a:hover,.list-customer li a:focus{height:29px;border-bottom:1px solid #424242}.list-customer li.bg-cust1 a{background:url(../img/com/bg_icon2_1.png) no-repeat 50% 17px}.list-customer li.bg-cust2 a{background:url(../img/com/bg_icon2_2.png) no-repeat 50% 17px}.list-customer li.bg-cust3 a{background:url(../img/com/bg_icon2_3.png) no-repeat 50% 17px}"""

    val css = new CSSContent(originCode,new URL("http://a.a"))
    CSSMinify.CSSMinify(css)
    css.toString should be (expected)
  }

}

