name := "feona"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(play.PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "mysql" % "mysql-connector-java" % "5.1.18",
  "org.jsoup"%"jsoup"%"1.7.3",
  "com.helger"%"ph-css"%"3.8.1",
  "com.typesafe.play" %% "play-slick" % "0.8.0",
  "org.scalatestplus" %% "play" % "1.1.0" % "test"
)