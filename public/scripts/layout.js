$(function (){
    init();
});

$(window).resize(function () {
    onResize();
});

var clickToClose = function (obj, opener) {
    if($(obj).is(':visible')) {
        $(window).bind('click', function () {
            $(obj).hide();
            $(window).unbind('click');
            $(opener).removeClass('opened');
        });
        $(obj).click(function (e) {
            e.stopPropagation();
        });
    }
};

var init = function () {
    var projectId = $('body').data('projectid');
    $.ajax({
        url: '/projectList',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = "";
            for (var i = 0; i < res.length; i++) {
                if(projectId == res[i].projectId)
                    str += '<li><a class="selected" href="/' + res[i].projectId + '/dashboard"><span class="icon"></span>' + res[i].domain + '</a></li>';
                else
                    str += '<li><a href="/' + res[i].projectId + '/dashboard"><span class="icon"></span>' + res[i].domain + '</a></li>';
            }

            $('#select-site-list').prepend(str)
        }
    });

    $('#open-select-site').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('opened');
        $('#select-site-list').toggle();
        clickToClose('#select-site-list', '#open-select-site');
    });
    
    $('#optimize-header').children('.optimize-header-left').children('.btn-history').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('opened');
        $('#optimize-history').toggle();
        $('#optimize-history-detail').toggle();
        clickToClose('#optimize-history, #optimize-history-detail', '#optimize-header .btn-history');
    });
    
    $('#dashboard-simulation-title').children('.open-select-region').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('opened');
        $('#dashboard-select-region').toggle();
        clickToClose('#dashboard-select-region', '#dashboard-simulation-title .open-select-region');
    });
    
    $('.file-browser-table li.table-folder .before').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parent().toggleClass('opened');
    });
    
    $('.file-browser-table .child-table').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    
    $('.file-browser-table li.table-folder').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $(this).find('.selected').removeClass('selected');
            removeSelected($(this).parent());
        } else {
            $(this).addClass('selected');
            $(this).find('li').addClass('selected');
            addSelected($(this).parent());
        }
        
        checkSelected($('.table-file.selected'));
    });
    
    $('.file-browser-table li:not(.table-folder)').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        if (!$(this).hasClass('selected')) {
            $(this).addClass('selected');
            addSelected($(this).parent());
        } else {
            $(this).removeClass('selected');
            removeSelected($(this).parent());
        }
        
        checkSelected($('.table-file.selected'));
    });
    
    $('.revert-selection').click(function (e) {
        e.preventDefault();
        $('.table-file').removeClass('selected');
        checkSelected($('.table-file.selected'));
    });
    
    $('.code-table td').each(function (i, block) {
        hljs.highlightBlock(block);
    });

    $('li.toggle a').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('toggle-on');
        $(this).toggleClass('toggle-off');
        $(this).addClass('just-changed');

        $('li.toggle a.just-changed').bind('mouseout', function () {
            $(this).removeClass('just-changed');
        });
    });

    var $spinner = $('#spinner');
    var intervalId = setInterval(function(){
        $spinner.css('background-position-y', '-=40');
        if($spinner.css('background-position-y') == '-360px') $spinner.css('background-position-y', 0);
    }, 60);

    $('#analyzing').data('interval_id', intervalId);

    if($('body').hasClass('page-simulate')) initSimulate();
    else checkAnalyze();
};

var initSimulate = function () {
    $('.page-selector-list .file-tooltip').each(function (e) {
        $(this).css('margin-left', -$(this).width() / 2);
    });

    $pagelist = $('.page-selector-list');

    $pagelist.width($pagelist.children().length * 80);

    $('.page-selector-list-wrapper').mCustomScrollbar({
        theme: 'dark-3',
        axis: 'x',
        mouseWheel: { invert: true }
    });

    $('.page-selector-item').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('selected');

        if($(this).hasClass('selected')) {
            $('#optimize-image-file-wrapper-'+$(this).data('pageid')).show();
        } else {
            $('#optimize-image-file-wrapper-'+$(this).data('pageid')).hide();
        }
    });

    $('.optimize-file-item').click(function () {
        var target = $(this).data('simulation');
        $('.optimize-file-item').removeClass('selected');
        $(this).addClass('selected');
        $('#simulation-iframe').attr('src', target);
    })

    onResize();
};

var checkAnalyze = function () {
    var projectId = $('body').data('projectid');
    $.ajax({
        url: '/' + projectId + '/available',
        type: 'GET',
        dataType: 'json',
        success: function(res) {
            var $body = $('body'), $analyzing = $('#analyzing');

            var intervalId = $analyzing.data('interval_id');
            clearInterval(intervalId);

            $analyzing.empty().remove();
            $('#body').show();
            onResize();

            if($body.hasClass('group-setting')) initSetting();
            if($body.hasClass('group-optimize')) initOptimize(res);
            if($body.hasClass('page-minify-js-css')) initMinify();
            if($body.hasClass('page-merge-js')) initMerge('Js');
            if($body.hasClass('page-merge-css')) initMerge('Cssimport');
            if($body.hasClass('page-css-sprite')) initCssSprite();
            if($body.hasClass('group-deploy')) initDeploy();
            if($body.hasClass('page-dashboard')) {
                if(res.total.img < 10) {
                    $('#video-simulation-after').hide();
                    $('#video-simulation-after-real').show();
                }
                initDashboard();
            }
        }
    });
};

var initDashboard = function (type) {
    var beforePlayer = videojs('video-simulation-before');
    var afterPlayer = videojs('video-simulation-after');
    var realAfterPlayer = videojs('video-simulation-after-real');

    beforePlayer.trigger('play');
    afterPlayer.trigger('play');
    realAfterPlayer.trigger('play');

    beforePlayer.on('ended', function () {
        $('.dashboard-simulation-before strong').text('10.8s');
    });
    afterPlayer.on('ended', function () {
        if($('#video-simulation-after').is(':visible'))
            $('.dashboard-simulation-after strong').text('10.8s');
    });
    realAfterPlayer.on('ended', function () {
        if($('#video-simulation-after-real').is(':visible'))
            $('.dashboard-simulation-after strong').text('7.3s');
    });
};

var initMerge = function (type) {
    var projectId = $('body').data('projectid');
    $.ajax({
        url: '/' + projectId + '/ext' + type,
        type: 'GET',
        dataType: 'json',
        success: function(res) {
            var i, fileList = res[0].list;

            fileList.sort(function (a, b) {
                return a.deployAbs.toLowerCase() > b.deployAbs.toLowerCase() ? 1 : (a.deployAbs.toLowerCase() < b.deployAbs.toLowerCase() ? -1 : 0);
            });

            var j=-1;
            var folderedList = [];

            // 폴더와 파일 분리
            for(i=0; i<fileList.length; i++) {
                if (!(j >= 0 && fileList[i].deployAbs.toLowerCase().match('https?://(.+/)([^/]+)')[1] == folderedList[j].folderName)) {
                    j++;
                    folderedList.push({ 'folderName': fileList[i].deployAbs.toLowerCase().match('https?://(.+/)([^/]+)')[1], 'list': [] });
                }
                folderedList[j].list.push(fileList[i]);
            }

            var fileObj = [];
            // 폴더 분리 + object로 변환
            for(i=0; i<folderedList.length; i++) {
                folderedList[i].folderName = folderedList[i].folderName.substring(0,folderedList[i].folderName.length - 1).split('/');
                fileObj = fileArrayToObj(fileObj, folderedList[i], 0);
            }

            var str = parseJsonToFileList(fileObj) || '';
            $('.optimize-file-browser-table').append(str);

            $('#optimize-file-selection-notice').children('.apply-selection').html('<span class="icon"></span>Merge Files').click(function () {
                var req = [];

                $('.file-browser-table .table-file.selected').each(function () {
                    req.push($(this).data('contenthash'));
                });

                if(req.length == 1) {
                    alert('한 개의 파일을 어떻게 더 합칠 수 있을까요?');
                    return false;
                }

                $.ajax({
                    url: '/' + projectId + '/optimize/merge',
                    type: 'POST',
                    data: JSON.stringify(req),
                    dataType: 'JSON',
                    contentType: 'application/json',
                    statusCode: {
                        200: function(res) {
                            //alert('파일을 성공적으로 합쳤습니다.');
                            document.location.reload();
                        }
                    }
                })
            });

            initFileTable();
        }
    });
};

var initMinify = function () {
    var projectId = $('body').data('projectid');
    $.ajax({
        url: '/' + projectId + '/extJs',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var jsList = res[0].list;

            $.ajax({
                url: '/' + projectId + '/extCss',
                type: 'GET',
                dataType: 'json',
                success: function (res) {
                    for(var i=0; i<res[0].list.length; i++) {
                        jsList.push(res[0].list[i]);
                    }

                    jsList.sort(function (a, b) {
                        return a.deployAbs.toLowerCase() > b.deployAbs.toLowerCase() ? 1 : (a.deployAbs.toLowerCase() < b.deployAbs.toLowerCase() ? -1 : 0);
                    });

                    var j=-1;
                    var folderedList = [];

                    // 폴더와 파일 분리
                    for(var i=0; i<jsList.length; i++) {
                        if (!(j >= 0 && jsList[i].deployAbs.toLowerCase().match('https?://(.+/)([^/]+)')[1] == folderedList[j].folderName)) {
                            j++;
                            folderedList.push({ 'folderName': jsList[i].deployAbs.toLowerCase().match('https?://(.+/)([^/]+)')[1], 'list': [] });
                        }
                        folderedList[j].list.push(jsList[i]);
                    }

                    var fileObj = [];
                    // 폴더 분리 + object로 변환
                    for(var i=0; i<folderedList.length; i++) {
                        folderedList[i].folderName = folderedList[i].folderName.substring(0,folderedList[i].folderName.length - 1).split('/');
                        fileObj = fileArrayToObj(fileObj, folderedList[i], 0);
                    }

                    var str = parseJsonToFileList(fileObj) || '';
                    $('.optimize-file-browser-table').append(str);

                    $('#optimize-file-selection-notice').children('.apply-selection').html('<span class="icon"></span>Minify Files').click(function () {
                        var req = [];
                        $('.file-browser-table .table-file.selected').each(function () {
                            req.push($(this).data('contenthash'));
                        });
                        $.ajax({
                            url: '/' + projectId + '/optimize/minify',
                            type: 'POST',
                            data: JSON.stringify(req),
                            dataType: 'JSON',
                            contentType: 'application/json',
                            statusCode: {
                                200: function(res) {
                                    //alert('파일 크기를 성공적으로 줄였습니다.');
                                    document.location.reload();
                                }
                            }
                        })
                    });

                    initFileTable();
                }
            });
        }
    });
};

var fileArrayToObj = function(subObj, Arr, index) {
    var res = subObj, i;

    if(index == Arr.folderName.length) {
        for(i=0;i<Arr.list.length;i++)
            res.push(Arr.list[i]);
        return res;
    }

    for(i=0; i<res.length; i++) {
        if(res[i].contentType == 'folder' && res[i].folderName == Arr.folderName[index]) {
            // folder match found
            flag = true;
            break;
        }
    }

    if(i == res.length) {
        // match not found
        res.push({contentType: 'folder', folderName: Arr.folderName[index], files: []});
    }

    res[i].files = fileArrayToObj(res[i].files, Arr, index + 1);

    return res;
};

var initCssSprite = function () {
    var projectId = $('body').data('projectid');
    $.ajax({
        url: '/' + projectId + '/spriteInfo',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var jsList = res[0].list;
            jsList.sort(function (a, b) {
                var aPath = a.originImgUrl.toLowerCase().match('(https?://.+/)([^/]+)')[1];
                var bPath = b.originImgUrl.toLowerCase().match('(https?://.+/)([^/]+)')[1];
                var aFile = a.originImgUrl.toLowerCase().match('(https?://.+/)([^/]+)')[2];
                var bFile = b.originImgUrl.toLowerCase().match('(https?://.+/)([^/]+)')[2];
                return aPath > bPath ? 1 : ( aPath < bPath ? -1 : ( aFile > bFile ? 1 : ( aFile < bFile ? -1 : 0 ) ) );
            });

            var j=-1, str = '';
            var folderedList = [];

            for(var i=0; i<jsList.length; i++) {
                if (!(j >= 0 && jsList[i].originImgUrl.toLowerCase().match('(https?://.+/)([^/]+)')[1] == folderedList[j].folderName)) {
                    j++;
                    folderedList.push({ 'folderName': jsList[i].originImgUrl.toLowerCase().match('(https?://.+/)([^/]+)')[1], 'list': [] });
                }
                folderedList[j].list.push(jsList[i]);
            }

            str += '<div id="optimize-image-file-wrapper-' + res[0].pageId + '" class="optimize-image-file-wrapper" data-pageid="' + res[0].pageId + '">';

            for(i=0;i<folderedList.length;i++) {
                str += '<h3>' + folderedList[i].list[0].originImgUrl.match('(https?://.+/)([^/]+)')[1] + '</h3>'
                    + '<a class="optimize-image-folder-select">폴더 전체 선택</a>'
                    + '<div class="optimize-image-wrapper">'
                    + '<div class="optimize-image-list">'
                    + '<ul class="clearfix">';

                for(j=0; j<folderedList[i].list.length; j++) {
                    str += '<li data-bottom="' + folderedList[i].list[j].bottom + '" data-height="' + folderedList[i].list[j].height + '" data-imagehashcode="' + folderedList[i].list[j].imageHashcode + '" data-left="' + folderedList[i].list[j].left + '" data-originimgurl="' + folderedList[i].list[j].originImgUrl + '" data-right="' + folderedList[i].list[j].right + '" data-top="' + folderedList[i].list[j].top + '" data-width="' + folderedList[i].list[j].width + '">'
                        + '<img src="' + '/'+projectId+'/contents/' + folderedList[i].list[j].imageHashcode + '" alt="sample" />'
                        + '<strong>' + folderedList[i].list[j].originImgUrl.match('(https?://.+/)([^/]+)')[2] + '</strong>'
                        + '</li>';
                }

                str += '</ul>'
                    + '</div>'
                    + '</div>';
            }

            str += '</div>';

            $('.optimize-image-browser').html(str);

            $('.optimize-image-list li').click(function (e) {
                e.preventDefault();
                $(this).toggleClass('selected');

                if ($(this).parent().children('li').length === $(this).parent().children('li.selected').length) {
                    $(this).parent().parent().parent().prev().addClass('selected');
                    $(this).parent().parent().parent().prev().text('폴더 선택 취소');
                } else {
                    $(this).parent().parent().parent().prev().removeClass('selected');
                    $(this).parent().parent().parent().prev().text('폴더 전체 선택');
                }

                checkSelected($('.optimize-image-list li.selected'));
            });

            $('.optimize-image-list').each(function () {
                $(this).width($(this).children('ul').children('li').length * 130);
            });

            $('.optimize-image-wrapper').mCustomScrollbar({
                theme: 'dark-3',
                axis: 'x',
                mouseWheel: { invert: true }
            });

            $('.optimize-image-folder-select').click(function (e) {
                e.preventDefault();
                if ($(this).hasClass('selected')) {
                    $(this).next().find('li').removeClass('selected');
                    $(this).toggleClass('selected');
                    $(this).text('폴더 전체 선택');
                } else {
                    $(this).next().find('li').addClass('selected');
                    $(this).toggleClass('selected');
                    $(this).text('폴더 선택 취소');
                }
                checkSelected($('.optimize-image-list li.selected'));
            });
        }
    });

    $('.apply-selection').click(function (e) {
        e.preventDefault();
        var selected = $('.optimize-image-list li.selected');
        var data = [];
        for(var i=0;i<selected.length;i++) {
            data.push({
                'bottom': $(selected[i]).data('bottom'),
                'height': $(selected[i]).data('height'),
                'imageHashcode': $(selected[i]).data('imagehashcode'),
                'left': $(selected[i]).data('left'),
                'originImgUrl': $(selected[i]).data('originimgurl'),
                'right': $(selected[i]).data('right'),
                'top': $(selected[i]).data('top'),
                'width': $(selected[i]).data('width')
            });
        }

        if(req.length == 1) {
            alert('한 개의 파일을 어떻게 더 합칠 수 있을까요?');
            return false;
        }

        $.ajax({
            url: '/' + projectId + '/optimize/csssprite',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            statusCode: {
                200: function () {
                    //alert('CSS Sprite가 적용되었습니다.');
                    document.location.reload(true);
                }
            }
        });
    });

    onResize();
};

var initSetting = function () {
    var projectId = $('body').data('projectid');
    var projectUrl = $('body').data('projectname')
    $('#settings-project-address').val(projectUrl);
    $.ajax({
        url: '/' + projectId + '/pageList',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = '';
            for(var i=0;i<res.length;i++) {
                if(res[i].url.match('https?://[^/]+/(.+)') != null) {
                    str += '<li data-pageid="' + res[i].pageId + '">'
                            + '<a href="#" class="new-page-del">X</a>'
                            + '<p><span class="new-page-address-guide">' + projectUrl + '/</span><input class="new-page-address" type="text" placeholder="Path" value="' + res[i].url.match('https?://[^/]+/(.+)')[1] + '"></p>'
                        + '</li>';
                }
            }

            $('#settings-project-pages-list').append(str).on('click', '.new-page-del', function (e) {
                e.preventDefault();
                var projectId = $('body').data('projectid');
                var pageId = $(this).parent().data('pageid');
                var target = $(this).parent();

                if(!confirm('정말 이 페이지를 삭제하시겠습니까?')) return false;

                $.ajax({
                    url: '/' + projectId + '/page/' + pageId + '/delete',
                    dataType: 'json',
                    type: 'POST',
                    context: target,
                    statusCode: {
                        200: function () {
                            $(this).remove();
                        }
                    }
                });
            });

            $('#add-new-page-url').data('projecturl',projectUrl);
        }
    });

    $('#add-new-page').click(function (e) {
        e.preventDefault();
        var pagePath = $('#add-new-page-url').val() || '';

        if(pagePath == '') {
            alert('페이지 경로가 비어있습니다.');
            return false;
        }

        req = [{ 'pageUrl': $('#add-new-page-url').data('projecturl') + '/' + pagePath }];

        $.ajax({
            url: '/' + $('body').data('projectid') + '/createPage',
            data: JSON.stringify(req),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            context: $('#add-new-page-url'),
            statusCode: {
                200: function (res) {
                    var str = '<li data-pageid="' + res[0] + '">'
                                + '<a href="#" class="new-page-del">X</a>'
                                + '<p><span class="new-page-address-guide">' + $(this).data('projecturl') + '/</span><input class="new-page-address" type="text" placeholder="Path" value="' + $(this).val() + '"></p>'
                            + '</li>';
                    $('#settings-project-pages-list').append(str);
                    $(this).val('');
                }
            }
        })
    });
};

var initOptimize= function (obj) {
    var projectId = $('body').data('projectid');

    $nav = $('#optimize-nav');

    if(obj.total.image == 1) {
        $nav.children('.css-sprite, .resample').find('span').html('<em></em> image available');
    }
    $nav.children('.css-sprite, .resample').find('em').text(obj.total.img);

    if(obj.total.js == 1) {
        $nav.children('.merge-js').find('span').html('<em></em> file available');
    }
    $nav.children('.merge-js').find('em').text(obj.total.js);

    if(obj.total.css == 1) {
        $nav.children('.merge-css').find('span').html('<em></em> file available');
    }
    $nav.children('.merge-css').find('em').text(obj.total.css);

    console.log(obj);
    if(obj.total.js + obj.total.cssimport  == 1) {
        $nav.children('.minify-js-css').find('span').html('<em></em> file available');
    }
    $nav.children('.minify-js-css').find('em').text(obj.total.js + obj.total.cssimport);

    // load history
    $.ajax({
        url:'/' + projectId + '/histories',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = '', nowIndex;
            for(var i=res.History.length-1;i>=0;i--) {
                if(res.History[i].optId != res.nowOptId)
                    str += '<li data-optid="' + res.History[i].optId + '"><a href="#"><strong>' + res.History[i].optimizeType + '</strong><em>' + res.History[i].createTime + '</em></a></li>';
                else {
                    str += '<li class="selected" data-optid="' + res.History[i].optId + '"><a href="#"><strong>' + res.History[i].optimizeType + '</strong><em>' + res.History[i].createTime + '</em></a></li>';
                    nowIndex = i;
                }
            }
            $history = $('#optimize-history');
            $history.children('ul').html(str);

            var $btnWrapper = $('#optimize-header').children('.optimize-header-left');
            var $undoBtn = $btnWrapper.children('.btn-undo');
            var $redoBtn = $btnWrapper.children('.btn-redo');

            if(nowIndex != 0) {
                $undoBtn.removeClass('disabled').data('optid', res.History[nowIndex - 1].optId).click(function(e){
                    // undo
                    e.preventDefault();
                    revertHistory($(this).data('optid'));
                });
            }
            if(nowIndex != res.History.length - 1) {
                $redoBtn.removeClass('disabled').data('optid', res.History[nowIndex + 1].optId).click(function(e){
                    // redo
                    e.preventDefault();
                    revertHistory($(this).data('optid'));
                });
            }

            $history.children('ul').children('li').children('a').click(function (e) {
                e.preventDefault();
                revertHistory($(this).parent().data('optid'));
            });

//            $.ajax({
//                url: '/' + projectId + '/history/' + res.nowOptId,
//                type: 'GET',
//                dataType: 'json',
//                success: function (res) {
//
//                }
//            })
        }
    });

    // load page list
    $.ajax({
        url: '/' + projectId + '/pageList',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = '';
            for (var i = 0; i < res.length; i++) {
                if (res[i].url.match('https?://[^/]+/(.+)') != null) {
                    str += '<div class="optimize-file-item page-selector-item selected" data-pageid="' + res[i].pageId + '"><span class="file-icon"></span><strong>' + res[i].url.match('https?://[^/]+/(.+)')[1] + '</strong><p class="file-tooltip"><strong>' + res[i].url.match('https?://[^/]+/(.+)')[1] + '</strong></p></div>';
                } else {
                    str += '<div class="optimize-file-item page-selector-item selected" data-pageid="' + res[i].pageId + '"><span class="file-icon"></span><strong>/</strong><p class="file-tooltip"><strong>/</strong></p></div>';
                }
            }

            $('.optimize-file-list').html(str);

            $('.page-selector-list .file-tooltip').each(function (e) {
                $(this).css('margin-left', -$(this).width() / 2);
            });

            $pagelist = $('.page-selector-list');

            $pagelist.width($pagelist.children().length * 80);

            $('.page-selector-list-wrapper').mCustomScrollbar({
                theme: 'dark-3',
                axis: 'x',
                mouseWheel: { invert: true }
            });

            $('.page-selector-item').click(function (e) {
                e.preventDefault();
                $(this).toggleClass('selected');

                if($(this).hasClass('selected')) {
                    $('#optimize-image-file-wrapper-'+$(this).data('pageid')).show();
                } else {
                    $('#optimize-image-file-wrapper-'+$(this).data('pageid')).hide();
                }
            });
        }
    });
};

var revertHistory = function (optId) {
    var projectId = $('body').data('projectid');
    var req = {
        "targetOptId": optId
    };

    $.ajax({
        url: '/' + projectId + '/history/revert',
        type: 'POST',
        data: req,
        dataType: 'json',
        statusCode: {
            200: function () {
                //alert('성공적으로 되돌렸습니다.');
                document.location.reload(true);
            },
            default: function () {
                alert('되돌리기에 실패했습니다.');
                document.location.reload(true);
            }
        }
    });
}

var removeSelected = function (obj) {
    if ($(obj).hasClass('child-table')) {
        $(obj).parent().removeClass('selected');
        removeSelected($(obj).parent().parent());
    }
};

var addSelected = function (obj) {
    var flag = true, i;
    
    for (i = 0; i < $(obj).children().length; i++) {
        if (!$($(obj).children()[i]).hasClass('selected')) {
            flag = false;
            break;
        }
    }
    
    if (flag === true) {
        $(obj).parent().addClass('selected');
        addSelected($(obj).parent().parent());
    } else {
        $(obj).parent().removeClass('selected');
    }
};

var checkSelected = function (obj) {
    if (obj.length !== 0) {
        $('#optimize-file-selection-notice').show();
        $('#optimize-file-selection-notice strong').text(obj.length);
        $('#optimize-section, #deploy-files').css('padding-top','40px');
    } else {
        $('#optimize-file-selection-notice').hide();
        $('#optimize-file-selection-notice strong').text(0);
        $('#optimize-section, #deploy-files').css('padding-top','0');
    }
};

var onResize = function () {
    var windowHeight = $(window).height(), windowWidth = $(window).width(), documentHeight = $(document).height();
    
    $('#optimize-section').css('width', windowWidth - 320);
    $('#section-optimize #optimize-file-selection-notice').innerWidth(windowWidth - 320);
    $('#section-deploy #optimize-file-selection-notice').innerWidth(windowWidth / 2);
    $('#dashboard-optimize-analyze').css('width', windowWidth - 480);
    $('#optimize-body').css('min-height', windowHeight - 95);
    $('#deploy-body').css('height', windowHeight - 50);
    $('#simulation-iframe').css('height', windowHeight-106)
};


var initDeploy = function() {
    var projectId = $('body').data('projectid');
    $.ajax({
        url: '/' + projectId + '/pageList',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = '';
            for (var i = 0; i < res.length; i++) {
                str += '<div class="optimize-file-item page-selector-item';
                if(i==0) str += ' selected';
                str += '" data-pageid="' + res[i].pageId + '"><span class="file-icon"></span><strong>';
                if(res[i].url.match('https?://[^/]+/(.+)') != null) {
                    str += res[i].url.match('https?://[^/]+/(.+)')[1] + '</strong><p class="file-tooltip"><strong>' + res[i].url.match('https?://[^/]+/(.+)')[1];
                } else {
                    str += '/</strong><p class="file-tooltip"><strong>/';
                }
               str += '</strong></p></div>';
            }

            $('.page-selector-list').html(str);

            var diffPageId = res[0].pageId;

            $.ajax({
                url: '/' + projectId + '/' + diffPageId + '/diff',
                type: 'GET',
                success: function (res) {
                    var origin = difflib.stringAsLines(res.origin);
                    var recent = difflib.stringAsLines(res.recent);

                    var sm = new difflib.SequenceMatcher(origin, recent);
                    var opcodes = sm.get_opcodes();

                    $('.optimize-code-scroll').html(diffview.buildView({
                        baseTextLines: origin,
                        newTextLines: recent,
                        opcodes: opcodes,
                        viewType: 1
                    }));

                    $('.diff-table td').each(function (i, block) {
                        hljs.highlightBlock(block);
                    });
                }
            });

            $('.page-selector-list .file-tooltip').each(function (e) {
                $(this).css('margin-left', -$(this).width() / 2);
            });

            $('.page-selector-list').width($('.page-selector-list').children().length * 80);

            $('.page-selector-list-wrapper').mCustomScrollbar({
                theme: 'dark-3',
                axis: 'x',
                mouseWheel: { invert: true }
            });

            $('.page-selector-item').click(function (e) {
                e.preventDefault();
                $(this).toggleClass('selected');

                if($(this).hasClass('selected')) {
                    $('#optimize-image-file-wrapper-'+$(this).data('pageid')).show();
                } else {
                    $('#optimize-image-file-wrapper-'+$(this).data('pageid')).hide();
                }
            });

            $('#optimize-file-selection-notice').children('.apply-selection').html('<span class="icon"></span>Download Files').click(function () {
                var req = [];
                $('.file-browser-table .table-file.selected').each(function () {
                    req.push($(this).data('contenthash'));
                });
                $.ajax({
                    url: '/' + projectId + '/deploy/mkZip',
                    type: 'POST',
                    data: JSON.stringify(req),
                    dataType: 'JSON',
                    contentType: 'application/json',
                    statusCode: {
                        200: function(res) {
                            $('#open-file-iframe').attr('src', res.fileUrl);
                        }
                    }
                })
            });
        }
    });

    $.ajax({
        url: '/' + projectId + '/deploy/list',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var i, fileList = res;

            fileList.sort(function (a, b) {
                return a.deployAbs.toLowerCase() > b.deployAbs.toLowerCase() ? 1 : (a.deployAbs.toLowerCase() < b.deployAbs.toLowerCase() ? -1 : 0);
            });

            var j=-1;
            var folderedList = [];

            // 폴더와 파일 분리
            for(i=0; i<fileList.length; i++) {
                if (!(j >= 0 && fileList[i].deployAbs.toLowerCase().match('https?://(.+/)([^/]+)')[1] == folderedList[j].folderName)) {
                    j++;
                    folderedList.push({ 'folderName': fileList[i].deployAbs.toLowerCase().match('https?://(.+/)([^/]+)')[1], 'list': [] });
                }
                folderedList[j].list.push(fileList[i]);
            }

            var fileObj = [];
            // 폴더 분리 + object로 변환
            for(i=0; i<folderedList.length; i++) {
                folderedList[i].folderName = folderedList[i].folderName.substring(0,folderedList[i].folderName.length - 1).split('/');
                fileObj = fileArrayToObj(fileObj, folderedList[i], 0);
            }

            var str = parseJsonToFileList(fileObj) || '';
            $('.deploy-file-browser-table').append(str);
            initFileTable();
        }
    });
};

var parseJsonToFileList = function (obj) {
    var res = '', projectId = $('body').data('projectid');
    for(var i=0; i<obj.length; i++) {
        if(obj[i].contentType == 'folder') {
            res += '<li class="row row-body table-folder opened">'
                + '<span class="before"></span><div class="cell cell-name"><span class="icon icon-folder"></span>' + obj[i].folderName + '</div>'
                + '<div class="cell cell-misc">'
                + '<p class="sub-cell sub-cell-type">Folder</p>'
                + '</div>'
                + '<ul class="child-table">';
            res += parseJsonToFileList(obj[i].files);
            res += '</ul></li>';
        } else {
            var fType = '';
            if(obj[i].contentType == 'application/javascript') fType = 'js';
            else if(obj[i].contentType == 'text/css') fType = 'css';
            else fType = 'etc';

            if(fType !== 'etc')
                res += '<li class="row row-body table-file" data-contenthash="' + obj[i].contentHash + '">'
                        + '<div class="cell cell-name"><span class="icon icon-' + fType + '"></span>' + obj[i].deployAbs.match('(https?://.+/)([^/]+)')[2] + '</div>'
                        + '<div class="cell cell-misc">'
                            + '<p class="sub-cell sub-cell-type">' + obj[i].contentType + '</p>'
                        + '</div>'
                    + '</li>';
            else
                res += '<li class="row row-body table-file" data-contenthash="' + obj[i].contentHash + '">'
                    + '<div class="cell cell-name"><span class="icon" style="background:url(/' + projectId + '/contents/' + obj[i].contentHash + ') 50% 50% no-repeat; background-size:contain;"></span>' + obj[i].deployAbs.match('(https?://.+/)([^/]+)')[2] + '</div>'
                    + '<div class="cell cell-misc">'
                    + '<p class="sub-cell sub-cell-type">' + obj[i].contentType + '</p>'
                    + '</div>'
                    + '</li>';
        }
    }

    return res;
};

var initFileTable = function () {
    $('.file-browser-table li.table-folder .before').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parent().toggleClass('opened');
    });

    $('.file-browser-table .child-table').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    $('.file-browser-table li.table-folder').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $(this).find('.selected').removeClass('selected');
            removeSelected($(this).parent());
        } else {
            $(this).addClass('selected');
            $(this).find('li').addClass('selected');
            addSelected($(this).parent());
        }

        checkSelected($('.table-file.selected'));
    });

    $('.file-browser-table li:not(.table-folder)').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        if (!$(this).hasClass('selected')) {
            $(this).addClass('selected');
            addSelected($(this).parent());
        } else {
            $(this).removeClass('selected');
            removeSelected($(this).parent());
        }

        checkSelected($('.table-file.selected'));
    });

    $('.revert-selection').click(function (e) {
        e.preventDefault();
        $('.table-file, .table-folder').removeClass('selected');
        checkSelected($('.table-file.selected'));
    });
}
