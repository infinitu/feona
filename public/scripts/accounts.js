$(function (){
    init();
});

var init = function () {
    $('#login-btn-signup').click(function (e) {
        e.preventDefault();
        $('#user-new-email').val($('#user-email').val());
        $('#user-new-password').val($('#user-password').val());
        $('#login-form').slideUp();
        $('#signup-form').slideDown();
        $('#user-new-password-again').focus();
    });

    $('#login-btn-signup-cancel').click(function (e) {
        e.preventDefault();
        $('#login-form').slideDown();
        $('#signup-form').slideUp();
        $('#user-email').focus();
    });

    $('#add-project-open').click(function (e) {
        e.preventDefault();
        $('#add-project').show();
    });

    $('#add-project-close').click(function (e) {
        e.preventDefault();
        $('#add-project').hide();
    });

    $('#add-project-address').bind("keyup", function (){
        var txt = $(this).val();

        if(txt[txt.length-1]!='/') txt = txt + '/';

        if(txt.length > 20) txt = txt.substr(0, 20) + '.../';
        else if(txt.length==1) txt = 'http://example.com/';

        $('.new-page-address-guide').text(txt);
    });

    $('#add-new-page').click(function (e) {
        e.preventDefault();

        var txt = $('#add-project-address').val();

        if(txt[txt.length-1]!='/') txt = txt + '/';

        if(txt.length > 20) txt = txt.substr(0, 20) + '.../';
        else if(txt.length==1) txt = 'http://example.com/';


        var newPageHtml = '<li>'
                            + '<a href="#" class="new-page-del">X</a>'
                            + '<p><span class="new-page-address-guide">' + txt + '</span><input class="new-page-address" type="text" placeholder="Path"></p>'
                        + '</li>';
        $('#add-project-pages-list').append(newPageHtml);
    });

    $('#add-project-submit').click(function (e) {
        e.preventDefault();

        var req = {};
        var reqDomain = $('#add-project-address').val() || '';
        var reqCrawlURL = $('#add-project-ts-address').val() || '';

        if(reqDomain === '') {
            alert('Error: Domain is empty.');
            return false;
        }

        if(reqDomain[reqDomain.length-1]!='/') reqDomain = reqDomain + '/';

        list = [];
        for(var i = 0; i < $('#add-project-pages-list').children().length; i++) {
            var reqPageUrl = $($('#add-project-pages-list').children()[i]).find('.new-page-address').val() || '';
            if(reqPageUrl !== '') list.push({'pageUrl': reqDomain + reqPageUrl});
        }

        if(reqCrawlURL === '') {
            req = { domain: reqDomain };
        } else {
            req = { domain: reqDomain, crawlURL: reqCrawlURL };
        }

        if(list.length>0) {
            req['new_page']=list;
        }


        $.ajax({
            url: '/createProject',
            type: 'POST',
            data: JSON.stringify(req),
            dataType: 'json',
            contentType: 'application/json',
            statusCode: {
                200: function(res) {
                    document.location.reload(true);
                },
                400: function(res) {
                    alert('Error 400: 뭔가 이상합니다! 이유는 모르겠습니다!');
                    console.log(res);
                },
                401: function() {
                    alert('Error 401: 로그인 상태를 다시 확인해주세요!');
                }
            }
        });
    });

    $('#add-project-pages-list').on("click", ".new-page-del", function () {
        if($('#add-project-pages-list').children().length > 1)
            $(this).parent().remove();
    });
};

var loadProjects = function () {
    $.ajax({
        url: '/projectList',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = "", i;
            for(i=0;i<res.length;i++) {
                str += '<li id="project-' + res[i].projectId + '">'
                        + '<a href="/' + res[i].projectId + '/dashboard">'
                            + '<span class="projects-screenshot" style="background-image:url(/' + res[i].projectId + '/thumbnail)"></span>'
                            + '<span class="projects-remove" data-target="' + res[i].projectId + '">X</span>'
                            + '<strong>' + res[i].domain + '</strong>'
                            + '<span class="projects-optimize-meter"><em>Optimize-score</em> loading...</span>'
                        + '</a>'
                    + '</li>';
            }
            $('#projects-list').prepend(str);

            for(i=0;i<res.length;i++) {
                $.ajax({
                    url: '/' + res[i].projectId + '/available',
                    type: 'GET',
                    dataType: 'json',
                    context: res[i],
                    success: function () {
                        $('#project-'+this.projectId).children('a').children('.projects-optimize-meter').addClass('not-good').html('<em>Optimize-score</em> <span>64</span>Not Good');
                    }
                });
            }

            $('.projects-remove').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                if(!confirm('정말 이 프로젝트를 삭제하시겠습니까?')) return false;

                var targetId = $(this).data('target');
                var target = $(this).parent().parent();

                $.ajax({
                    url: '/' + targetId,
                    type: 'DELETE',
                    dataType: 'json',
                    context: target,
                    statusCode: {
                        200: function () {
                            $(this).remove();
                        }
                    }
                });
            });
        }
    });
}