
import com.feona.feo.webcontents.WebContent
import controllers.Simulation
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import play.api.mvc._
import play.api.{Application, GlobalSettings}
import play.mvc.Results.Redirect

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._


/**
 * Created by infinitu on 2014. 6. 9..
 */


object Global extends GlobalSettings{

  override def onRouteRequest(request: RequestHeader): Option[Handler] = {
    if(request.host equals Simulation.simulationHost) {
      Some(controllers.Simulation.index)
    }
    else if(request.host equals WebContent.feonaHost){

      super.onRouteRequest(request)
    }
    else Some(controllers.Application.redirectHost)
  }
}
