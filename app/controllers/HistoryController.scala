package controllers

import com.feona.feo.optimize.OptimizeType
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.libs.json.{JsNumber, JsObject, Json}
import play.api.mvc.{Session => mvcSession, _}

/**
 * Created by CK-Kim on 2014. 9. 26..
 */
object HistoryController extends Controller{

  def getProjectHistory(projectId:Int) = Action{req=>
    val loginData = UserAccount.getSessionData(req.session)

    if(!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else{
      val project = ProjectController.getProject(loginData,projectId)
      if(project.isEmpty)
        Forbidden("not allowd project")
      else{
        val nowOptimizeRule =
          DB.withSession{implicit session=>
            models.OptimizeRules.tQuery.filter(_.projectId === projectId).list
          }

        Ok(
          Json.obj(
            "nowOptId"->project.get.snapshotId,
            "History"->Json.toJson(nowOptimizeRule.map(_.json))
          )
        )
      }
    }
  }

  def revertOptimize(projectId:Int) = Action{req=>
    val targetOpt = req.body.asFormUrlEncoded.get.get("targetOptId").map(_.head) //TODO IntTypeError처리
    val loginData = UserAccount.getSessionData(req.session)
    
    if(!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!ProjectController.checkProjectPermission(loginData,projectId))
      Forbidden("not allowd project")
    else if(targetOpt.isEmpty)
      BadRequest("no target Optimize Id Parameter")
    else{
      val nowOptimizeRule =
        DB.withSession{implicit session=>
          models.OptimizeRules.tQuery
            .filter(_.projectId === projectId)
            .filter(_.optimizeId === targetOpt.get.toInt)
            .map(_.optimizeId)
            .list
        }
      if(nowOptimizeRule.size == 0)
        BadRequest("can not find such optimize")
      else{
        DB.withSession { implicit session =>
          models.Projects.tQuery.filter(_.projectId === projectId).map(_.snapshotId).update(targetOpt.get.toInt)
        }
        Ok("Ok")
      }
    }
  }

  def getProjectHistoryWithResult(projectId:Int) = Action{req=>
    val loginData = UserAccount.getSessionData(req.session)

    if(!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else{
      val project = ProjectController.getProject(loginData,projectId)
      if(project.isEmpty)
        Forbidden("not allowd project")
      else{
        Ok {
          Json.obj(
            "nowOptId"->project.get.snapshotId,
            "History"->Json.toJson(
              DB.withSession { implicit session =>
                val optList = models.OptimizeRules.tQuery.filter(_.projectId === projectId).list
                val idList = optList.map(_.optimizeId)

                val results = models.CssSpriteResults.tQuery.filter(_.optimizeId inSet idList).list

                val sprite = models.CssSpriteResults.tQuery
                  .filter(_.optimizeId inSet idList)
                  .list

                val merge = models.MergeRules.tQuery
                  .filter(_.optimizeId inSet idList)
                  .list

                val minify = models.MinifyRules.tQuery
                  .filter(_.optimizeId inSet idList)
                  .list

                val contents = models.WebContents.tQuery
                  .filter(_.contentHash inSet
                  results.map(_.contentHash) ++ sprite.map(_.contentHash)
                    ++ merge.map(_.contentHash) ++ minify.map(_.contentHash))
                  .list

                val targets:List[(Int, JsObject)]=
                  (for {i <- sprite; k <- contents if i.contentHash == k.contentHash} yield i.optimizeId -> k.json) ++
                    (for {i <- minify; k <- contents if i.contentHash == k.contentHash} yield i.optimizeId -> k.json) ++
                    (for {i <- merge; k <- contents if i.contentHash == k.contentHash} yield i.optimizeId -> k.json.+("index" -> JsNumber(i.mergeIdx)))

                val groupedTarget = targets.groupBy(_._1).map(x => x._1 -> x._2.map(_._2.asInstanceOf[JsObject]))

                optList.map { opt =>
                  val ret =
                    if(groupedTarget.get(opt.optimizeId).isDefined)
                      opt.json + ("target" -> Json.toJson(groupedTarget(opt.optimizeId)))
                    else
                      opt.json
                  val resOpt = results.find(_.optimizeId == opt.optimizeId)
                  if (resOpt.isDefined) {
                    val hash = resOpt.get.contentHash
                    ret + ("result" -> contents.find(_.contentHash == hash).get.json)
                  }
                  else
                    ret
                }
              }
            )
          )
        }
      }
    }
  }

  def getOptimizeResult(projectId:Int,optimizeId:Int) = Action { req=>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if (!ProjectController.checkProjectPermission(loginData, projectId))
      Forbidden("not allowd project")
    else {
      val opt =
        DB.withSession { implicit session =>
          models.OptimizeRules.tQuery.filter(_.projectId === projectId).filter(_.optimizeId === optimizeId).list
        }
      if(opt.isEmpty)
        NotFound("can not find such optimize")
      else{
        Ok(
          DB.withSession { implicit session =>
            OptimizeType.DBValue2OptimizeType(opt.head.optimizeType) match {
              case OptimizeType.CssSprite =>
                val spriteTar = models.CssSpriteRules.tQuery
                  .filter(_.optimizeId === optimizeId)
                  .innerJoin(models.WebContents.tQuery).on(_.contentHash === _.contentHash)
                  .map(_._2)
                  .list
                val spriteResult = models.CssSpriteResults.tQuery
                  .filter(_.optimizeId === optimizeId)
                  .innerJoin(models.WebContents.tQuery).on(_.contentHash === _.contentHash)
                  .map(_._2)
                  .first
                Json.obj {
                  "target" -> Json.toJson(spriteTar.map(_.json))
                  "result" -> spriteResult.json
                }
              case OptimizeType.Merge =>
                val mergeTar = models.MergeRules.tQuery
                  .filter(_.optimizeId === optimizeId)
                  .innerJoin(models.WebContents.tQuery).on(_.contentHash === _.contentHash)
                  .map(x => x._1.mergeIdx -> x._2)
                  .list
                val result = models.CssSpriteResults.tQuery
                  .filter(_.optimizeId === optimizeId)
                  .innerJoin(models.WebContents.tQuery).on(_.contentHash === _.contentHash)
                  .map(_._2)
                  .first
                Json.obj {
                  "target" -> Json.toJson(mergeTar.map(x => x._2.json.+("index" -> JsNumber(x._1))))
                  "result" -> result.json
                }
              case OptimizeType.Minify =>
                val mergeTar = models.MinifyRules.tQuery
                  .filter(_.optimizeId === optimizeId)
                  .innerJoin(models.WebContents.tQuery).on(_.contentHash === _.contentHash)
                  .map(_._2)
                  .list
                Json.obj {
                  "target" -> Json.toJson(mergeTar.map(_.json))
                }
              case OptimizeType.Initialize =>
                Json.obj {
                  "initialize"->true
                }

            }
          }
        )
      }
    }
  }

}
