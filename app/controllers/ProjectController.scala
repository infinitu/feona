package controllers

import java.io.File
import java.net.URL

import com.feona.feo.Util
import com.feona.feo.history.OptimizeHistory
import com.feona.feo.io.{WebContentManager, DashboardContentManager}
import com.feona.feo.optimize.OptimizeType
import controllers.UserAccount.SessionData
import models.{OptimizeRule, OptimizeRules}
import play.Logger
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.libs.json.{JsArray, Json}
import play.api.mvc.{Session => mvcSession, _}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._

/**
 * Created by CK-Kim on 2014. 9. 25..
 */
object ProjectController extends Controller{

  def switchProject = Action { req=>
    if(!UserAccount.isLogined(req.session))
      Redirect("/login")
    else {
      Ok(views.html.switchProject())
    }
  }

  def addProject()= Action{ req=>
    if(!UserAccount.isLogined(req.session))
      Unauthorized("not logined")
    else{
      val requestData = req.body.asJson
      val loginData = UserAccount.getSessionData(req.session)
      val domain = requestData.map(_ \ "domain").map(_.as[String]).getOrElse("")
      val crawl_url = requestData.map(_ \ "crawlURL").flatMap(_.asOpt[String])


      if(domain.matches(Util.urlPattern.regex)) {
        val projectURL = new URL(domain)
        val projectHost = projectURL.getProtocol+"://"+projectURL.getHost
        val projectDefaultPath = projectHost+"/"
        DB.withSession { implicit session =>

          val pid =
            models.Projects.tQuery.returning(models.Projects.tQuery.map(_.projectId)) +=
              models.Project(0, loginData.uid, projectHost, crawl_url, 0, None)

          val optId : Int =
            OptimizeRules.tQuery returning OptimizeRules.tQuery.map(_.optimizeId) +=
              OptimizeRule(0, pid, OptimizeType.Initialize, new java.sql.Timestamp(System.currentTimeMillis()))

          models.Projects.tQuery.filter(_.projectId === pid).map(_.snapshotId).update(optId)


          val newPages:Seq[String] = requestData.map(_ \ "new_page")
            .flatMap(_.asOpt[JsArray])
            .map(_.value)
            .map(_.map(_ \ "pageUrl"))
            .map(_.map(_.as[String]))
            .getOrElse(Seq(projectDefaultPath))

          addPages(newPages,pid,optId)

          Ok(Json.obj("projectId" -> pid.toInt))
        }
      }
      else
        BadRequest("not validated domain")
    }
  }

  def getProjectList = Action { req=>
    if(!UserAccount.isLogined(req.session))
      Unauthorized("not logined")
    else{
      val loginData = UserAccount.getSessionData(req.session)
      val projectList =
        DB.withSession { implicit session=>
          models.Projects.tQuery.filter(_.userId === loginData.uid).list
        }
      Ok(Json.toJson(projectList.map(_.json)))
    }
  }

  def deleteProject(pid:Int) = Action { req =>
    if(!UserAccount.isLogined(req.session))
      Unauthorized("not logined")
    else{
      val loginData = UserAccount.getSessionData(req.session)
      val lines =
        DB.withSession { implicit session=>
          models.Projects.tQuery.filter(_.userId === loginData.uid).filter(_.projectId === pid).delete
        }
      if(lines == 0)
        BadRequest("no such project")
      else {
        Ok("Done")
      }
    }
  }


  def getProjectThumbnail(pid:Int) = Action.async{ req=>
    if(!UserAccount.isLogined(req.session))
      Future{Unauthorized("not logined")}
    else{
      val loginData = UserAccount.getSessionData(req.session)
      val project =
        DB.withSession { implicit session=>
          models.Projects.tQuery.filter(_.userId === loginData.uid).filter(_.projectId === pid).list
        }
      if(project.isEmpty)
        Future{BadRequest("no such project")}
      else {
        if(project.head.thumbnail.isDefined && !project.head.thumbnail.get.equals(""))
          Future {Ok.sendFile(new File(project.head.thumbnail.get), inline = true)}
        else {
          val page =
            DB.withSession { implicit session =>  models.Pages.tQuery.filter(_.projectId === pid).first }
          DashboardContentManager.mkSnapshot(page)
            .map { filePath =>
              DB.withSession { implicit session => models.Projects.tQuery.filter(_.projectId === pid).map(_.thumbnail).update(Some(filePath))}
              new File(filePath)
            }
            .map{ file => Ok.sendFile(file, inline = true)}

        }
      }
    }
  }

  def getProject(sessionData:SessionData,projectId:Int):Option[models.Project]=DB.withSession{ implicit session=>
    models.Projects.tQuery.filter(_.projectId === projectId).filter(_.userId === sessionData.uid).firstOption
  }

  def checkProjectPermission(sessionData:SessionData,projectId:Int):Boolean = getProject(sessionData,projectId).isDefined

  def addPage(projectId:Int)=Action{req=>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!checkProjectPermission(loginData,projectId))
      Forbidden("not allowd project")
    else{
      val requestData = req.body.asJson.get.as[JsArray].value
      val pageURLs = requestData.map(_.\("pageUrl")).map(_.as[String])


      val optId : Int = DB.withSession { implicit session =>
        OptimizeRules.tQuery.filter(_.projectId === projectId).filter(_.optimizeType === OptimizeType.Initialize.dbValue).map(_.optimizeId).first
      }
      val pids = addPages(pageURLs,projectId,optId)

      if(pids.isEmpty)
        BadRequest("no validated domain")
      else {
        Ok(Json.toJson(pids))
      }
    }
  }

  def addPages(pageURLs:Seq[String],projectId:Int,initialOptId:Int):Seq[Int]={
    val pids =
      DB.withSession { implicit session =>
        models.Pages.tQuery.returning(models.Pages.tQuery.map(_.pageId)) ++=
          pageURLs.filter(_.matches(Util.urlPattern.regex)).map(x => models.Page(0, projectId, x, "", ""))
      }
    DB.withSession { implicit session => models.Pages.tQuery.filter(_.pageId inSet pids).list}
      .par.foreach(WebContentManager.updatePage(_,initialOptId)(projectId))
    pids
  }

  def getPageList(projectId:Int) = Action { req=>
    val loginData = UserAccount.getSessionData(req.session)

    if(!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!checkProjectPermission(loginData,projectId))
      Forbidden("not allowd project")
    else{
      val pageList =
        DB.withSession { implicit session=>
          models.Pages.tQuery.filter(_.projectId === projectId).list
        }
      Ok(Json.toJson(pageList.map(_.json)))
    }
  }

  def deletePage(projectId:Int,pageId:Int) = Action { req =>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!checkProjectPermission(loginData,projectId))
      Forbidden("not allowd project")
    else {
      val lines =
        DB.withSession { implicit session =>
          models.Pages.tQuery.filter(_.projectId === projectId).filter(_.pageId === pageId).delete
        }
      if (lines == 0)
        BadRequest("no such project")
      else {
        Ok("Done")
      }
    }
  }


}
