package controllers


import com.feona.feo.history.OptimizeHistory
import com.feona.feo.webcontents.{ContentType, WebContent}
import com.feona.feo.webcontents.impl.{ExternalCSSContent, HtmlContent}
import controllers.OptimizeController._
import controllers.ProjectController._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.http.HeaderNames
import play.api.libs.json.JsArray
import play.api.mvc.{Session => mvcSession, _}

/**
 * Created by CK-Kim on 2014. 9. 28..
 */
object Simulation extends Controller{
  lazy val simulationHost = play.api.Play.current.configuration.getString("feona.simulationHost").getOrElse("simulation.feona.kr")
  def simulationURL(projectId:Int, contentHash:String) = "//"+simulationHost + "/%d?feona_simulation=%s".format(projectId,contentHash)
  val refererPattern = (simulationHost.replace(".","\\.") + """/([0-9]+)/?\?feona_simulation=([0-9|a-f]+)""").r
  val pathPattern = "/([0-9]+)/?".r
  def index = Action{req=>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Redirect("//"+WebContent.feonaHost+"/login")
    else {
      val query = req.getQueryString("feona_simulation")
      if(query.isDefined) {
        val project = pathPattern.findFirstMatchIn(req.path).map(_.group(1).toInt)
        val hash = query.get
        if(project.isEmpty){
          NotFound("no Such Content")
        }
        if (!checkProjectPermission(loginData, project.get)) {
          Forbidden("not allowd project")
        }
        else {
          val wc = DB.withSession { implicit session =>
            models.WebContents.tQuery.filter(_.projectId === project.get).filter(_.contentHash === hash).firstOption
          }
          if (wc.isEmpty) {
            NotFound("no Such Content")
          }
          else {
            if(wc.get.contentType == ContentType.HTML)
              Ok(WebContent(wc.get).asInstanceOf[HtmlContent].mkSimulation.toString).as(wc.get.contentType.name)
            else if(wc.get.contentType == ContentType.CSS)
              Ok(WebContent(wc.get).asInstanceOf[ExternalCSSContent].mkSimulation.toString).as(wc.get.contentType.name)
            else
              Ok.sendFile(wc.get.localFile, inline = true).as(wc.get.contentType.name)
          }
        }
      }else{
        val referMatch = refererPattern.findFirstMatchIn(req.headers.get(HeaderNames.REFERER).getOrElse(""))
        if(referMatch.isDefined){
          val project = getProject(loginData,referMatch.get.group(1).toInt)
          if(project.isEmpty)
            NotFound("i dont know project is")
          else
            TemporaryRedirect(project.get.crawlServerUrl + req.path)
        }
        else{
          NotFound("i dont know what this mean")
        }
      }
    }
  }

  def simulationConsole(projectId:Int) = Action { req =>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Redirect("/login")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden("not allowd project")
      else {
        DB.withSession{implicit session=>
          val pages = OptimizeHistory.getSnapshotPages(projectId,project.get.snapshotId)
          val lastReg = "[^/]+$".r
          val pageList = pages.map{x=>
            lastReg.findFirstMatchIn(x._1.urlStr).get.group(0)->simulationURL(projectId,x._2.dbRow.contentHash)
          }
          Ok(views.html._simulate(project.get.domain,projectId,pageList))
        }
      }
    }
  }


}
