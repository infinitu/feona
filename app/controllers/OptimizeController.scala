package controllers


import com.feona.feo.history.OptimizeHistory
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.optimize.OptimizeType.OptimizeType
import com.feona.feo.optimize.merge.Merge
import com.feona.feo.optimize.minify.Minify
import com.feona.feo.optimize.sprite.{CSSSprite, SpriteInfo}
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.{Session => mvcSession, _}

/**
 * Created by CK-Kim on 2014. 9. 27..
 */
object OptimizeController extends Controller{

  def optimizeViewInit(projectId:Int)=optimizeView(projectId,OptimizeType.Initialize)
  def optimizeViewCssSprite(projectId:Int)=optimizeView(projectId,OptimizeType.CssSprite)
  def optimizeViewMinify(projectId:Int)=optimizeView(projectId,OptimizeType.Minify)
  def optimizeViewMergeJs(projectId:Int)=optimizeView(projectId,OptimizeType.Merge,"js")
  def optimizeViewMergeCss(projectId:Int)=optimizeView(projectId,OptimizeType.Merge,"css")

  def optimizeView(projectId:Int,optT:OptimizeType, hint:String = "")= Action {req=>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Redirect("/login")
    else {
      val project = ProjectController.getProject(loginData,projectId)
      if (project.isEmpty)
        Forbidden(views.html._error("error", "error", "Error: 403")(views.html.error_403()))
      else {
        optT match {
          case OptimizeType.Initialize =>
            Ok(views.html._base("css-sprite", "optimize", "Optimize", project.get.domain, projectId)(views.html._optimize(projectId)(views.html.imageBrowser())))
          case OptimizeType.CssSprite =>
            Ok(views.html._base("css-sprite", "optimize", "Optimize - Make CSS Sprite", project.get.domain, projectId)(views.html._optimize(projectId)(views.html.imageBrowser())))
          case OptimizeType.Minify =>
            Ok(views.html._base("minify-js-css", "optimize", "Optimize - Minify JS/CSS", project.get.domain, projectId)(views.html._optimize(projectId)(views.html.fileBrowser())))
          case OptimizeType.Merge if hint equals "js" =>
            Ok(views.html._base("merge-js", "optimize", "Optimize - Merge JS/CSS", project.get.domain, projectId)(views.html._optimize(projectId)(views.html.fileBrowser())))
          case OptimizeType.Merge if hint equals "css" =>
            Ok(views.html._base("merge-css", "optimize", "Optimize - Merge JS/CSS", project.get.domain, projectId)(views.html._optimize(projectId)(views.html.fileBrowser())))
        }
      }
    }
  }

  def optimizeCssSprite(projectId:Int)=optimize(projectId,OptimizeType.CssSprite)
  def optimizeMinify(projectId:Int)=optimize(projectId,OptimizeType.Minify)
  def optimizeMerge(projectId:Int)=optimize(projectId,OptimizeType.Merge)

  def optimize(projectId:Int,optT:OptimizeType) = Action { req =>
    val loginData = UserAccount.getSessionData(req.session)
    val inp =
      try {
        req.body.asJson.map(_.as[JsArray])
      } catch {
        case _: Exception => None
      }

    if (!UserAccount.isLogined(loginData))
      Redirect("/login")
    else if (inp.isEmpty)
      BadRequest("wrong parameter")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden("not allowd project")
      else {
        try {
          OptimizeHistory.optimize(project.get.projectId, project.get.snapshotId)(
            optT match {
              case OptimizeType.CssSprite =>
                cssSprite(projectId, inp.get)
              case OptimizeType.Minify =>
                minify(projectId, inp.get)
              case OptimizeType.Merge =>
                merge(projectId, inp.get)
            }
          )
        Ok("ok")
        } catch {
          case ex: Exception =>
            Logger.debug("optimize Error", ex)
            BadRequest("bad parameter or internal server error")
          case _: Throwable =>
            BadRequest("WTF... i dont know")
        }
      }
    }
  }

  private def cssSprite(projectId:Int,json:JsArray)= {
    import com.feona.feo.optimize.sprite.SpriteInfo.SpriteInfoReads
    CSSSprite.mkSprite(projectId, json.value.map(_.as[SpriteInfo]))
  }

  private def merge(projectId:Int,json:JsArray) =
    Merge.mkMerge(projectId,json.value.map(_.as[JsString].value))

  private def minify(projectId:Int,json:JsArray) =
    Minify.mkMinify(projectId,json.value.map(_.as[JsString].value))

}
