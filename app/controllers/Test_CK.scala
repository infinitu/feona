package controllers


import java.io.File

import com.feona.feo.history.OptimizeHistory
import com.feona.feo.io.WebContentManager
import com.feona.feo.optimize.sprite.CSSSprite
import com.feona.feo.optimize.minify.Minify
import com.feona.feo.optimize.merge.Merge
import com.feona.feo.webcontents.impl.ExternalCSSContent

import models.Users
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.mvc._


/**
 * Created by infinitu on 2014. 9. 15..
 */
object Test_CK extends Controller{

  def crawlTest(pid:Int) = Action{

    /*
      Crawl All Pages in project
     */
    WebContentManager.updateAll(pid)
    Ok
  }

  def parseTest(pid:Int) = Action{


    /*
     show parsed javascript list
    */
    //    DB.withSession{implicit session=>
    //      val recent = OptimizeHistory.getSnapshotPages(pid,9999)
    //
    //      Ok(recent(0)._2.jsHash.map(_._2).map{
    //        case external:ExternalJavascriptContent=>
    //          external.dbRow.originUrlStr
    //        case js=>
    //          "internal (" + js.toString.substring(0,10).replace("\n","\\n") +")"
    //      }.foldLeft("jsHash : \n\n")(_ + "\n" + _))
    //    }


//        /*
//          show parsed css list
//        */
//
//        DB.withSession{implicit session=>
//
//          val recent = OptimizeHistory.getSnapshotPages(pid,9999)
//
//          Ok(recent(0)._2.cssHash.flatMap(x=> x._2.allStyleRules.map(_._2).distinct).map{
//            case external:ExternalCSSContent=>
//              external.dbRow.originUrlStr
//            case css=>
//              "internal (" + css.toString.substring(0,10).replace("\n","\\n") +")"
//          }.foldLeft("cssHash : \n\n")(_ + "\n" + _))
//
//        }


    /*
     show parsed spritable Image
   */

    DB.withSession{implicit session=>

      val recent = OptimizeHistory.getSnapshotPages(pid,9999)

      Ok(recent(0)._2.spritableInfo.map(x=> x._2).distinct.foldLeft("spriteInfo : \n\n")(_ + "\n" + _))

    }
  }

  def optimizeTest(pid:Int) = Action{

    val recent =
      DB.withSession { implicit session =>
        OptimizeHistory.getSnapshotPages(pid, 9999).apply(0)
      }

    //CSSSprite
    //OptimizeHistory.optimize(recent._1.projectId)(CSSSprite.mkSprite(recent._1.projectId, recent._2.spritableInfo.map(_._2)))

    //Minify
//    OptimizeHistory.optimize(recent._1.projectId)(Minify.mkMinify(recent._1.projectId,recent._2.cssHash
//      .map(_._2).filter(_.isInstanceOf[ExternalCSSContent])
//      .map(_.asInstanceOf[ExternalCSSContent].dbRow.contentHash)))

    //Merge
    OptimizeHistory.optimize(recent._1.projectId)(Merge.mkMerge(recent._1.projectId,recent._2.cssHash
      .map(_._2).filter(_.isInstanceOf[ExternalCSSContent])
      .map(_.asInstanceOf[ExternalCSSContent].dbRow.contentHash).toSeq))

    DB.withSession { implicit session =>
      Ok(OptimizeHistory.getSnapshotPages(pid, 9999).apply(0).toString())
    }
  }

  def test() = Action{
    Ok.sendFile(new File("/Users/CK-Kim/Desktop/스크린샷 2014-07-11 오후 5.51.22.png"),inline = true)
  }


}
