package controllers


import com.feona.feo.deploy.Deploy
import com.feona.feo.history.OptimizeHistory
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.optimize.sprite.SpriteInfo
import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{ExternalCSSContent, ExternalJavascriptContent, HtmlContent}
import controllers.OptimizeController._
import controllers.ProjectController._
import models.{OptimizeRules, PageHistories}
import org.jsoup.Jsoup
import play.api.Logger
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.libs.json._
import play.api.mvc.{Session => mvcSession, _}

/**
 * Created by CK-Kim on 2014. 9. 27..
 */
object DeployController extends Controller {

  def getActiveDeployContents(projectId: Int) = Action { req =>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden("not allowd project")
      else {
        val activeContents =
          DB.withSession { implicit session =>
            val recent = OptimizeHistory.getSnapshotPages(projectId, project.get.snapshotId)
            recent.flatMap { case (page, html) =>
              (html.cssHash.map(_._2)++html.cssHash.map(_._2)).flatMap{
                _.importedCSS
              }.flatMap{
                case css: ExternalCSSContent =>
                  Some(css.dbRow)
                case _ =>
                  None
              } ++
                html.jsHash.map(_._2).flatMap {
                  case js: ExternalJavascriptContent =>
                    Some(js.dbRow)
                  case _ =>
                    None
                } ++
                html.spritableInfo.map(_._2.imageHashcode).map(WebContent(_).dbRow)
            }
          }
        Ok(
          Json.toJson(activeContents.distinct.filter(_.isGen).map{row=>
            row.json
          })
        )
      }
    }
  }

  def mkDeployDownload(projectId:Int) = Action {req=>
    val loginData = UserAccount.getSessionData(req.session)
    val inp =
      try {
        req.body.asJson.map(_.as[JsArray]).map(_.value).map(_.flatMap(_.asOpt[String]))
      } catch {
        case _: Exception => None
      }
    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!ProjectController.checkProjectPermission(loginData,projectId))
      Forbidden("not allowd project")
    else if(inp.isEmpty)
      BadRequest("bad parameter")
    else {
      val zip = Deploy.deploy(inp.get)(projectId)
      if(zip.isEmpty)
        InternalServerError("can't make zip File")
      else{
        val project = ProjectController.getProject(loginData,projectId)
        Ok(Json.obj("fileUrl" -> "/%d/deploy/%s".format(projectId,zip.get.getName)))
      }
    }
  }

  def downloadDeployZip(projectId:Int,name:String)=Action{req=>
    val loginData = UserAccount.getSessionData(req.session)
    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!ProjectController.checkProjectPermission(loginData,projectId))
      Forbidden("not allowd project")
    else {
      val file = Deploy.getFile(name)(projectId)
      if(!file.exists())
        NotFound(views.html._error("error", "error", "Error: 404")(views.html.error_404()))
      else{
        Ok.sendFile(file,false,{x=>"deploy.zip"})
      }
    }


  }

}