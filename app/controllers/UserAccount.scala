package controllers

import com.feona.feo.io.HashGenerator
import models.Users
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.mvc.{Session=>mvcSession}
import play.api.mvc._

/**
 * Created by infinitu on 2014. 9. 24..
 */
object UserAccount extends Controller with HashGenerator{
  val LOGIN_UID = "loginedUID"
  val LOGIN_EMAIL = "loginedEmail"
  val LOGIN_NAME = "loginedName"

  case class SessionData(uid:Int,email:String,name:String)

  def getSessionData(session:mvcSession):SessionData = SessionData( session.get(LOGIN_UID   ).getOrElse("-1").toInt,
                                                                    session.get(LOGIN_EMAIL ).getOrElse("not logined"),
                                                                    session.get(LOGIN_NAME  ).getOrElse("not logined"))
  def isLogined(session:mvcSession):Boolean=isLogined(getSessionData(session))
  def isLogined(session:SessionData):Boolean=session.uid>0

  def mkPassword(plain:String):String=getSHA256(plain)

  def loginPage = Action{
    Ok(views.html.login())
  }

  def register = Action{req=>
    val reqParam = req.body.asFormUrlEncoded.get
    val passwd =  mkPassword(reqParam.get("passwd").get.head)
    val email =  reqParam.get("email").get.head
    val name =  reqParam.get("name").get.head
    val mobile =  reqParam.get("mobile").get.head

    val user = models.User(0,name,passwd,email,mobile)
    DB.withSession{ implicit  session=>
      if(Users.tQuery.filter(_.email === email).list.isEmpty){Users.tQuery += user;Ok}
      else  BadRequest("duplicated email")
    }
  }


  def login = Action{req=>
    val reqParam = req.body.asFormUrlEncoded.get
    val passwd =  mkPassword(reqParam.get("passwd").get.head)
    val email =  reqParam.get("email").get.head
    DB.withSession{ implicit  session=>
      val user = Users.tQuery.filter(_.email === email).filter(_.pwd === passwd).list
      if(user.isEmpty) BadRequest("No Such User")
      else
        Redirect("/projects")
          .withSession(
            LOGIN_UID->user.head.userId.toString,
            LOGIN_EMAIL->user.head.email,
            LOGIN_NAME->user.head.name
          )
    }
  }

  def logout = Action{req=>
    Ok("ok").withNewSession
  }
}
