package controllers

import com.feona.feo.history.OptimizeHistory
import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{HtmlContent, HtmlSnapshotContent}
import controllers.Test_MF._
import models.{Page, WebContent => dbWebContent, WebContents => dbWebContents}
import play.api.Play.current
import play.api.db.slick.DB
import play.api.libs.json.{JsArray, Json}
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

object Application extends Controller {

  def index = Action {req=>
    if(!UserAccount.isLogined(req.session))
      Redirect("/login")
    else
      Redirect("/projects")
  }

  def redirectHost = Action {req=>
    Redirect("http://"+WebContent.feonaHost)
  }

//    def getWebContents(pid : Int, hashCode : String) = Action {
//      val inline = true
//      val wc : dbWebContent= DB.withSession { implicit session =>
//        dbWebContents.tQuery.filter(_.projectId == pid).filter(_.contentHash == hashCode).first
//      }
//
//      Ok.sendFile(wc.localFile, inline)
//    }
//
//  def simulation(pid: Int, pageId: Int) = Action {
//
//    /* todo */
//    Ok
//  }
//
//
//  def capture(pageId: Int) = Action {
//    Ok
//  }
//
//  def crawl(pageId : Int) = Action {
////???
//    Ok
//  }
//  /**
//   * type Snapshot_CSS, Snapshot_JS, Snapshot_Image 의 list를 가져옴
//   * @param projectId
//   * @return
//   */
//  def getWebContents(projectId: Int) = Action {
//    val snapshot_pages: Seq[(Page, HtmlSnapshotContent)] = DB.withSession { implicit session =>
//      OptimizeHistory.getSnapshotPages(projectId, 99999).map{x=> x._1 -> x._2.asInstanceOf[HtmlSnapshotContent]}
//    }
//
//    //
////    val js_csss = snapshot.getSnapshotScripts().map{x=> x._2.dbRow}
////    val images = snapshot.getImageContents().map{x=> x._2.dbRow}
//    val wcs : Seq[(Page, Seq[dbWebContent])] = snapshot_pages.map{case (page, snapshot)=>
//      page -> (snapshot.getSnapshotScripts().map(_._2.dbRow) ++ snapshot.getImageContents().map(_._2.dbRow))
//    }
//
//    val ret = JsArray(
//      wcs.map{ case (page, wcs) =>
//        Json.obj(
//          "page" -> page.urlStr,
//          "webcontents" -> JsArray(wcs.map(_.json))
//        )
//      }
//    )
//
//    Ok(ret)
//  }
//
//  def getPageList(projectId : Int) = Action {
//    val snapshot_pages : Seq[(Page, HtmlContent)] = DB.withSession { implicit session =>
//      OptimizeHistory.getSnapshotPages(projectId, 99999)
//    }
//
//    Ok(JsArray(
//      snapshot_pages.map(_._1.json)
//    ))
//  }
//
//  def getPageHistory(projectId: Int) = play.mvc.Results.TODO
//
//
//  case class ProjectId_N_PageList(projectId: Int, pages: List[Int])
//  val PID_PGIDS = Form (
//    mapping(
//      "project_id" -> number,
//      "page_ids" -> list(number)
//    )(ProjectId_N_PageList.apply)(ProjectId_N_PageList.unapply)
//  )
//
//  //POST
//  def getImageList() = Action{ implicit request  =>
//    val ppl = PID_PGIDS.bindFromRequest.get
//    val snapshot_pages : Seq[(Page, HtmlContent)] = DB.withSession { implicit session =>
//      OptimizeHistory.getSnapshotPages(ppl.projectId, ppl.pages, 99999)
//    }
//
//    val res = snapshot_pages.flatten{case (page, htmlcontent) =>
//      htmlcontent.spritableInfo.map{x=> (page, x._2)}
//    }
//
//    val ret = JsArray(
//      res.map { case (page, imageContent) =>
//        Json.obj(
//          "page" -> page.urlStr,
//          "image" -> Json.obj(
//              "url" -> imageContent.originImgUrl,
//              "hash" -> imageContent.imageHashcode,
//              "width" -> imageContent.width,
//              "height" -> imageContent.height,
//              "top" -> imageContent.top,
//              "bottom" -> imageContent.bottom,
//              "left" -> imageContent.left,
//              "right" -> imageContent.right
//          )
//        )
//      }
//    )
//    Ok(ret)
//  }
//
//  def codeview(pageId: Int) = play.mvc.Results.TODO
//
//  def getSnapshotJsList() = Action{ implicit request =>
//    val ppl = PID_PGIDS.bindFromRequest.get
//    val snapshot_pages : Seq[(Page, HtmlSnapshotContent)] = DB.withSession { implicit session =>
//      OptimizeHistory.getSnapshotPages(ppl.projectId, ppl.pages, 99999).map{x=> x._1 -> x._2.asInstanceOf[HtmlSnapshotContent]}
//    }
//
//    val pageCss : Seq[(Page, Seq[dbWebContent])]= snapshot_pages.map{case (page, snapshot) =>
//      page-> snapshot.getSnapshotJS().map{x=> x._2.dbRow}
//    }
//
//    val ret = JsArray(
//      pageCss.map{ case (page, dbwc) =>
//        Json.obj(
//          "page" -> page.urlStr,
//          "csslist" -> JsArray(dbwc.map(_.json))
//        )
//      }
//    )
//
//    Ok(ret)
//  }
//
//
//  def getSnapshotCssList() = Action{implicit request =>
//    val ppl = PID_PGIDS.bindFromRequest.get
//    val snapshot_pages : Seq[(Page, HtmlSnapshotContent)] = DB.withSession { implicit session =>
//      OptimizeHistory.getSnapshotPages(ppl.projectId, ppl.pages, 99999).map{x=> x._1 -> x._2.asInstanceOf[HtmlSnapshotContent]}
//    }
//
//    val pageCss : Seq[(Page, Seq[dbWebContent])]= snapshot_pages.map{case (page, snapshot) =>
//      page-> snapshot.getSnapshotCSS().map{x=> x._2.dbRow}
//    }
//
//    val ret = JsArray(
//      pageCss.map{ case (page, dbwc) =>
//        Json.obj(
//          "page" -> page.urlStr,
//          "csslist" -> JsArray(dbwc.map(_.json))
//        )
//      }
//    )
//
//
//    Ok(ret)
//  }

}

