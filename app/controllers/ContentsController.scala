package controllers


import com.feona.feo.history.OptimizeHistory
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.optimize.sprite.SpriteInfo
import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{ExternalCSSContent, ExternalJavascriptContent, HtmlContent}
import controllers.DeployController._
import controllers.ProjectController._
import models.{OptimizeRules, PageHistories}
import org.jsoup.Jsoup
import play.api.Logger
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.libs.json._
import play.api.mvc.{Session => mvcSession, _}

/**
 * Created by CK-Kim on 2014. 9. 27..
 */
object ContentsController extends Controller {

  def getActiveExternalCSSContents(projectId:Int)=getActiveContents(projectId,"externalCSS")
  def getActiveExternalCSSContentsWithImport(projectId:Int)=getActiveContents(projectId,"externalCSSImport")
  def getActiveExternalJSContents(projectId:Int)=getActiveContents(projectId, "externalJS")
  def getActiveSpriteInfoContents(projectId:Int)=getActiveContents(projectId, "spriteInfo")

  def getActiveContents(projectId:Int,resourceType:String) = Action { req =>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden("not allowd project")
      else {
        DB.withSession{implicit session=>
          val recent = OptimizeHistory.getSnapshotPages(projectId,project.get.snapshotId)
          Ok{resourceType match{
            case "externalCSS"=>activeExternalCSS(recent)
            case "externalJS"=>activeExternalJS(recent)
            case "spriteInfo"=>activeSpriteInfo(recent)
            case "externalCSSImport"=>activeExternalCSSincludeImport(recent)
          }}

        }
      }
    }
  }

  def activeExternalCSS(list:Seq[(models.Page,HtmlContent)]) = Json toJson list.map{case (page,html)=>
    Json.obj(
      "pageId"->page.pageId,
      "list"->Json.toJson(
        html.cssHash.map(_._2).flatMap{
          case css:ExternalCSSContent=>
            Some(css.dbRow.json)
          case _=>
            None
        }
      )
    )
  }
  def activeExternalCSSincludeImport(list:Seq[(models.Page,HtmlContent)]) = Json toJson list.map{case (page,html)=>
    Json.obj(
      "pageId"->page.pageId,
      "list"->Json.toJson(
        (html.cssHash.map(_._2)++html.cssHash.map(_._2).distinct.flatMap{
          _.importedCSS
        }).flatMap{
          case css:ExternalCSSContent=>
            Some(css.dbRow.json)
          case _=>
            None
        }
      )
    )
  }

  def activeExternalJS(list:Seq[(models.Page,HtmlContent)]) = Json toJson list.map{case (page,html)=>
    Json.obj(
      "pageId"->page.pageId,
      "list"->Json.toJson(
        html.jsHash.map(_._2).flatMap{
          case js:ExternalJavascriptContent=>
            Some(js.dbRow.json)
          case _=>
            None
        }
      )
    )
  }
  private def max(a:Int,b:Int)=if(a>b) a else b
  def activeSpriteInfo(list:Seq[(models.Page,HtmlContent)]) = Json toJson list.map { case (page, html) =>
    Json.obj(
      "pageId" -> page.pageId,
      "list" -> Json.toJson {
        import com.feona.feo.optimize.sprite.SpriteInfo.SpriteInfoWrites
        html.spritableInfo.map(_._2)
          .groupBy(_.imageHashcode)
          .map{x=>
            x._2.fold(x._2.head)((x,y)=>
              SpriteInfo(x.imageHashcode,x.originImgUrl,
                max(x.top,y.top),
                max(x.right,y.right),
                max(x.left,y.left),
                max(x.bottom,y.bottom),
                x.width,
                x.height))
            }
          .toBuffer
          .sortWith(_.originImgUrl < _.originImgUrl)
          .map{info=>
            val lastSlash = info.originImgUrl.lastIndexOf('/')
            val dir = info.originImgUrl.substring(0,lastSlash)
            val name = info.originImgUrl.substring(lastSlash+1)
            Json.toJson(info).as[JsObject] + ("dir"->JsString(dir)) + ("name"->JsString(name))
          }
      }
    )
  }


  def getWebContents(pid : Int, hashCode : String) = Action { req=>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else if(!checkProjectPermission(loginData,pid))
      Forbidden("not allowd project")
    else {
      val wc = DB.withSession { implicit session =>
        models.WebContents.tQuery.filter(_.projectId === pid).filter(_.contentHash === hashCode).firstOption
      }
      if(wc.isEmpty){
        NotFound("no Such Content")
      }
      else{
        Ok.sendFile(wc.get.localFile, inline = true).as(wc.get.contentType.name)
      }
    }
  }

  def getWebContentOrigin(projectId:Int,pageId:Int)= Action { req =>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else {
      val proj = getProject(loginData, projectId)
      if (proj.isEmpty)
        Forbidden("not allowd project")
      else
        DB.withSession { implicit session =>
          val page = models.Pages.tQuery.filter(_.pageId === pageId).filter(_.projectId === projectId).firstOption
          if(page.isEmpty)
            NotFound("no such page page in this project")
          else {
            val snapshot = PageHistories.tQuery.filter(_.pageId === pageId).sortBy(_.optId.asc).first
            Ok(WebContent(snapshot.contentHash).asInstanceOf[HtmlContent].mkDeploy)
          }
        }
    }
  }

  def getWebContentRecent(projectId:Int,pageId:Int)=Action { req =>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else{
      val proj = getProject(loginData,projectId)
      if (proj.isEmpty)
        Forbidden("not allowd project")
      else DB.withSession { implicit session =>
        val page = models.Pages.tQuery.filter(_.pageId === pageId).filter(_.projectId === projectId).firstOption
        if(page.isEmpty)
          NotFound("no such page page in this project")
        else {
          Ok(OptimizeHistory.getSnapshotPages(projectId,Seq(pageId),proj.get.snapshotId).head._2.mkDeploy)
        }
      }
    }
  }

  def getWebContentDiff(projectId:Int,pageId:Int) = Action {req=>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else{
      val proj = getProject(loginData,projectId)
      if (proj.isEmpty)
        Forbidden("not allowd project")
      else DB.withSession{ implicit session =>
        val page = models.Pages.tQuery.filter(_.pageId === pageId).filter(_.projectId === projectId).firstOption
        if(page.isEmpty)
          NotFound("no such page page in this project")
        else {
          val snapshot = PageHistories.tQuery.filter(_.pageId === pageId).sortBy(_.optId.asc).first
          Ok(Json.obj(
            "origin"-> WebContent(snapshot.contentHash).asInstanceOf[HtmlContent].mkDeploy,
            "recent"-> OptimizeHistory.getSnapshotPages(projectId, Seq(pageId), proj.get.snapshotId).head._2.mkDeploy))
        }
      }
    }
  }


  def getAvailableContentsCount(projectId: Int) = Action { req =>
    val loginData = UserAccount.getSessionData(req.session)

    if (!UserAccount.isLogined(loginData))
      Unauthorized("not logined")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden("not allowd project")
      else {

        DB.withSession { implicit session =>
          val recent = OptimizeHistory.getSnapshotPages(projectId, project.get.snapshotId)
          val list=
          recent.map{ case (page, html) =>
            val css =
              html.cssHash.map(_._2).flatMap{
                case css: ExternalCSSContent =>
                  Some(css.dbRow)
                case _ =>
                  None
              }

            val cssWithImport =
              (html.cssHash.map(_._2)++html.cssHash.map(_._2).distinct.flatMap{
                _.importedCSS
              }).flatMap{
                case css: ExternalCSSContent =>
                  Some(css.dbRow)
                case _ =>
                  None
              }


            val js =
              html.jsHash.map(_._2).flatMap {
                case js: ExternalJavascriptContent =>
                  Some(js.dbRow)
                case _ =>
                  None
              }

            val img =
              html.spritableInfo.map(_._2.imageHashcode).map(WebContent(_).dbRow)

            Json.obj(
              "pageId"-> page.pageId,
              "cssimport"->css.distinct.size,
              "css"->cssWithImport.size,
              "js"->js.distinct.size,
              "img"->img.distinct.size
            ) -> (css,cssWithImport,js,img)
          }
          val totalCSS = list.flatMap(_._2._1).distinct
          val totalCSSImport = list.flatMap(_._2._2).distinct
          val totalJs = list.flatMap(_._2._3).distinct
          val totalImg = list.flatMap(_._2._4).distinct

          val finalList = Json.obj(
            "list"  -> Json.toJson(list.map(_._1)),
            "total" -> Json.obj(
              "css" -> totalCSS.size ,
              "cssimport" -> totalCSSImport.size ,
              "js"  -> totalJs.size ,
              "img" -> totalImg.size
            )
          )
          Ok(finalList)
        }
      }
    }
  }

}
