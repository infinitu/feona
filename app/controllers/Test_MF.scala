package controllers

import controllers.OptimizeController._
import models.{WebContents => dbWebContents, WebContent => dbWebContent}

import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.mvc._

object Test_MF extends Controller {

  def dashboard(projectId:Int) = Action {req=>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Redirect("/login")
    else {
      val project = ProjectController.getProject(loginData,projectId)
      if (project.isEmpty)
        Forbidden(views.html._error("error", "error", "Error: 403")(views.html.error_403()))
      else
        Ok(views.html._base("dashboard", "dashboard", "Dashboard", project.get.domain, projectId)(views.html.dashboard(projectId)))
    }
  }

  def simulate(projectId:Int) = Action {req=>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Redirect("/login")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden(views.html._error("error", "error", "Error: 403")(views.html.error_403()))
      else
        Ok
    }
  }

  def setting(projectId:Int) = Action {req=>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Redirect("/login")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden(views.html._error("error", "error", "Error: 403")(views.html.error_403()))
      else
        Ok(views.html._base("setting", "setting", "Project Settings", project.get.domain, projectId)(views.html.settings(projectId)))
    }
  }

  def deploy(projectId:Int) = Action {req=>
    val loginData = UserAccount.getSessionData(req.session)
    if(!UserAccount.isLogined(loginData))
      Redirect("/login")
    else {
      val project = ProjectController.getProject(loginData, projectId)
      if (project.isEmpty)
        Forbidden(views.html._error("error", "error", "Error: 403")(views.html.error_403()))
      else
        Ok(views.html._base("deploy", "deploy", "Deploy", project.get.domain, projectId)(views.html._deploy(projectId)(views.html.diffViewer())))
    }
  }

  def error_404= Action {
    Ok(views.html._error("error", "error", "Error: 404")(views.html.error_404()))
  }

  def error_403= Action {
    Ok(views.html._error("error", "error", "Error: 403")(views.html.error_403()))
  }

//  def getWebContents(pid : Int, hashCode : String) = Action {
//    val inline = true
//    val wc : dbWebContent= DB.withSession { implicit session =>
//      dbWebContents.tQuery.filter(_.projectId == pid).filter(_.contentHash == hashCode).first
//    }
//
//    Ok.sendFile(wc.localFile, inline)
//  }

  def simulation(pid : Int, pageId : Int) = Action {

    /* todo */
    Ok
  }
}