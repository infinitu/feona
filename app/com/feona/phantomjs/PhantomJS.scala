package com.feona.phantomjs

import java.io.File
import java.net.URL
import java.util.concurrent.{FutureTask, Executors, ExecutorService}

import play.api.libs.json.{JsValue, Json}

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Promise, Await, Future}
import scala.sys.process._

//import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try

object PhantomJS {

  object PHANTOM_API extends Enumeration {
    val CAPTURE = Value("capture")
    val CRAWL = Value("crawl")
  }

  val PATH_KEY = "phantom.path"
  val FILE_KEY = "phantom.file"
  val POOL_KEY = "phantom.pool"


  lazy val conf = play.api.Play.current.configuration
  lazy val installedPath = conf.getString(PATH_KEY).getOrElse("phantomjs")
  lazy val phantomFile = conf.getString(FILE_KEY).getOrElse("feona.js")
  lazy val maxThread = conf.getString(POOL_KEY).getOrElse("3").toInt

  val SUCCESS = "Status:200"
  implicit val executionContent = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(maxThread))

  case class PhantomTask(task: PHANTOM_API.Value, pageUrl: URL, dest: File, future: Future[Any])

  private[phantomjs] val captureSingleton = mutable.HashMap[String, PhantomTask]()
  private[phantomjs] val crawlSingleton = mutable.HashMap[String, PhantomTask]()

  def getTask(task: PHANTOM_API.Value, pageUrl: String) = task match {
    case PHANTOM_API.CAPTURE =>
      captureSingleton.get(pageUrl)
    case PHANTOM_API.CRAWL =>
      crawlSingleton.get(pageUrl)
  }

  private def newTask(api: PHANTOM_API.Value, url: URL, dest: File): Future[String] = Future {
    println(s"$installedPath $phantomFile $api ${url.toString} ${dest.toString}")
    val str = Process(s"$installedPath $phantomFile $api ${url.toString} ${dest.toString}").!!
    api match {
      case PHANTOM_API.CAPTURE =>
        captureSingleton.synchronized(captureSingleton -= url.toExternalForm)
      case PHANTOM_API.CRAWL =>
        crawlSingleton.synchronized(crawlSingleton -= url.toExternalForm)
    }
    str
  }

  def capture(pageUrl: URL, dest: File) = {
    val single = captureSingleton.synchronized {
      captureSingleton.get(pageUrl.toString)
    }
    if (single.isDefined)
      single.get.future
    else {
      val task = PhantomTask(PHANTOM_API.CAPTURE, pageUrl, dest, newTask(PHANTOM_API.CAPTURE, pageUrl, dest).map(x=>dest.toString))
      captureSingleton.synchronized {
        captureSingleton += pageUrl.toString -> task
      }
      task.future.asInstanceOf[Future[String]]
    }
  }

  case class CrawlContent(url: URL, dest: File, pages: Array[URL])

  def crawl[T](pageUrl: URL, dest: File, func:(CrawlContent)=>T): Future[T] = {
    val single = crawlSingleton.synchronized {
      crawlSingleton.get(pageUrl.toString)
    }
    if (single.isDefined)
      single.get.future.asInstanceOf[Future[T]]
    else {
      val task = PhantomTask(PHANTOM_API.CRAWL, pageUrl, dest,
        newTask(PHANTOM_API.CRAWL, pageUrl, dest)
          .map { case result: String =>
            val json: JsValue = Json.parse(result.split(SUCCESS)(1))
            val r_url = (json \ "url").as[String]
            val r_dest = (json \ "dest").as[String]
            val r_pages = (json \ "pages").as[Array[String]].filter(x => !x.contains("about:blank"))

            CrawlContent(new URL(r_url), new File(r_dest), r_pages.map(new URL(_)))
          }.map(func))
      crawlSingleton.synchronized {
        crawlSingleton += pageUrl.toString -> task
      }
      task.future.asInstanceOf[Future[T]]
    }
  }
}