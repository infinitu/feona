package com.feona.feo.webcontents.impl


import java.io.File
import javax.imageio.ImageIO

import com.feona.feo.webcontents.WebContent
import models.{WebContent => dbWebContent}

/**
 * Created by CK-Kim on 2014. 8. 1..
 */
class ImageContent(val dbRow:dbWebContent) extends WebContent{
  lazy val image = ImageIO.read(new File(dbRow.localPath))
  lazy val width:Int = image.getWidth
  lazy val height:Int = image.getHeight
}
