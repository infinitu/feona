package com.feona.feo.webcontents.impl

import java.net.URL
import java.nio.charset.Charset

import com.feona.feo.io.GenContentManager
import com.feona.feo.{ProjectIdWrapper, Util}
import com.feona.feo.webcontents.{ChangeableContent, WebContent}
import com.helger.css.ECSSVersion
import com.helger.css.decl._
import com.helger.css.parser.Token
import com.helger.css.reader.CSSReader
import com.helger.css.reader.errorhandler.{LoggingCSSParseErrorHandler, DoNothingCSSParseErrorHandler}
import com.helger.css.writer.{CSSCompressor, CSSWriter, CSSWriterSettings}
import controllers.Simulation
import models.{WebContent => dbWebContent}

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
 * Created by CK-Kim on 2014. 8. 1..
 */
object CSSContentConst{
  val CssWriter = new CSSWriter(new CSSWriterSettings(ECSSVersion.CSS30))
  CssWriter.setContentCharset("UTF-8")
  private[impl] val defaultGetImport:(URL => ExternalCSSContent) = {url:URL => WebContent(url).asInstanceOf[ExternalCSSContent]}
}

trait CSSContent extends ChangeableContent {

  val parsedCSS : CascadingStyleSheet
  val refURL:URL
  val getImport:(URL => ExternalCSSContent)

  private[feo] var minify:Boolean = false

  override def toString: String ={

    val cssStr = CSSContentConst.CssWriter.getCSSAsString(parsedCSS)
    if(minify)
      CSSCompressor.getCompressedCSS(cssStr,Charset.forName("UTF-8"),ECSSVersion.CSS30)
    else
      cssStr
  }


  lazy val importRules:Seq[(CSSImportRule,ExternalCSSContent)] = Option(parsedCSS).map(_.getAllImportRules.map{rule=>
    val url = rule.getLocationString
    WebContent.FEONA_URL_PATTERN.findFirstMatchIn(url) match{
      case Some(urlMatch)=>
        rule->WebContent(urlMatch.group(2)).asInstanceOf[ExternalCSSContent]
      case None=>
        this.setChanged()
        val css = getImport(Util.AbsoluteURL(refURL,url))
        implicit val pidWrap = ProjectIdWrapper(css.dbRow.projectId)
        rule.setLocation(new CSSURI(WebContent.getFEONA_URL(css.dbRow.contentHash)))
        rule->css
    }
    }).getOrElse{
    println("null!!! : " + refURL.toString)
    Seq()
  }

  lazy val importedCSS = importRules.map(_._2)

  def mkSnapshot():this.type ={
    importRules.foreach{ case (rule,content)=>
      content.mkSnapshot()
      if(content.isChanged) {
        val dbRow = content.databaseExport()
        implicit val pidWrap = ProjectIdWrapper(dbRow.projectId)
        rule.setLocation(new CSSURI(WebContent.getFEONA_URL(dbRow.contentHash)))
        setChanged()
      }
    }
    this
  }

  def mkSimulation={
    importRules.foreach{case (rule,ext)=>
      rule.setLocationString(Simulation.simulationURL(ext.dbRow.projectId,ext.dbRow.contentHash))
    }
    this
  }

  def mkDeploy={
    WebContent.FEONA_URL_PATTERN.replaceAllIn(this.toString,{urlMatch=>
      WebContent(urlMatch.group(2)).dbRow.deployPath
    })
  }


  //변경사항 유무
  override private[feo] def isChanged: Boolean = {
    importedCSS.foreach{x=>if(x.isChanged){return true}}
    super.isChanged
  }

  /**
   * 지정된 위치에 새로운 스타일 정의문을 삽입한다.
   * @param rule 새로운 정의문을 넣을 스타일 룰 객체
   * @param decl 삽입될 새로운 정의문
   */
  def addStyle(rule:CSSStyleRule)(decl:CSSDeclaration){
    rule.addDeclaration(decl)
    setChanged()
  }


  def allStyleRules:Seq[(CSSStyleRule, CSSContent)] =
    importedCSS.flatMap(_.allStyleRules) ++ Option(parsedCSS).map(_.getAllStyleRules.map(_->this)).getOrElse(Seq())

  def allFontFaceRules:Seq[(CSSFontFaceRule, CSSContent)] =
    importedCSS.flatMap(_.allFontFaceRules) ++ Option(parsedCSS).map(_.getAllFontFaceRules.map(_->this)).getOrElse(Seq())

}

class InternalCSSContent(val parsedCSS : CascadingStyleSheet, val refURL:URL,val getImport:(URL => ExternalCSSContent) ) extends CSSContent{

  //String 으로 부터 로딩
  def this(content:String,refURL:URL,getImport:(URL => ExternalCSSContent) = CSSContentConst.defaultGetImport) =
    this(CSSReader.readFromString(content,ECSSVersion.CSS30,DoNothingCSSParseErrorHandler.getInstance()), refURL,getImport)

}

class ExternalCSSContent(val dbRow:dbWebContent,val getImport:(URL => ExternalCSSContent) = CSSContentConst.defaultGetImport)
    extends WebContent with CSSContent {

  override val parsedCSS = CSSReader.readFromFile(dbRow.localFile,Charset.defaultCharset()
    ,ECSSVersion.CSS30,new LoggingCSSParseErrorHandler{
      override def onCSSParseError(aLastValidToken: Token, aExpectedTokenSequencesVal: Array[Array[Int]], aTokenImageVal: Array[String], aLastSkippedToken: Token): Unit = {
        println(dbRow.originUrl + " has parse error")
        println(aLastValidToken.toString+"\\"+aLastValidToken.beginLine+":"+aLastValidToken.beginColumn)
        println(aLastSkippedToken.toString)
        super.onCSSParseError(aLastValidToken, aExpectedTokenSequencesVal, aTokenImageVal, aLastSkippedToken)
      }
    })

  override def databaseExport(deploy:Option[String]=None): dbWebContent = {
    println("newCon!")
    GenContentManager.save(dbRow.contentType,dbRow.projectId,Some(dbRow),deployPath = {
      if(deploy.isDefined)
        deploy
      else if(dbRow.deployPath.endsWith("min.css")&&minify){
        None
      }else{
        Some("\\.css$".r.replaceFirstIn(dbRow.deployPath,".min.css"))
      }
    }){stream=>
      fileExport(stream)
    }
  }

  val refURL: URL = dbRow.originUrl
}

class InlineStyleRule(val styles:mutable.Buffer[CSSDeclaration]) extends ChangeableContent{
  def addStyle(decl:CSSDeclaration){
    styles+=decl
    setChanged()
  }

  override def toString={
    val list = new CSSDeclarationList()
    styles.foreach(list.addDeclaration)
    CSSContentConst.CssWriter.getCSSAsString(list)
  }
}