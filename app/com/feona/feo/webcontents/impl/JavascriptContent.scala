package com.feona.feo.webcontents.impl

import java.io.File

import com.feona.feo.Util
import com.feona.feo.io.GenContentManager
import com.feona.feo.webcontents.{ChangeableContent, WebContent}
import models.{WebContent => dbWebContent}

/**
 * Created by CK-Kim on 2014. 8. 1..
 */
class JavascriptContent(content:String) extends ChangeableContent{

  override def toString: String = content

  //File로 로딩
  def this(file:File) =
    this(Util.fileToString(file))


}

class ExternalJavascriptContent(override val dbRow:dbWebContent)
  extends JavascriptContent(new File(dbRow.localPath)) with WebContent{
  var minify = false
  private var content:String = Util.fileToString(dbRow.localFile)
  def contentChange(newContent:String)={content = newContent;setChanged();this}
  override def toString: String = content

  override def databaseExport(deploy:Option[String]=None): dbWebContent = {
    println("newCon!")
    GenContentManager.save(dbRow.contentType,dbRow.projectId,Some(dbRow),deployPath = {
      if(deploy.isDefined)
        deploy
      else if(dbRow.deployPath.endsWith("min.js")&&minify){
        None
      }else{
        Some("\\.js".r.replaceFirstIn(dbRow.deployPath,".min.js"))
      }
    }){stream=>
      fileExport(stream)
    }
  }

}
