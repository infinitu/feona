package com.feona.feo.webcontents.impl

import java.net.URL

import com.feona.feo.io.WebContentManager
import com.feona.feo.optimize.sprite.SpriteInfo
import com.feona.feo.optimize.sprite.SpriteInfo.CSSDeclerationWithEditor
import com.feona.feo.webcontents.WebContent
import com.feona.feo.{ProjectIdWrapper, Util}
import com.feona.util.SourceInputStream
import com.helger.css.ECSSVersion
import com.helger.css.decl.{CSSExpressionMemberTermURI, CSSExpression, CSSDeclaration}
import com.helger.css.reader.CSSReaderDeclarationList
import controllers.Simulation
import models.{WebContent => dbWebContent}
import org.jsoup.Jsoup
import org.jsoup.nodes.{DataNode, Element, Node}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, Json}

import scala.collection.JavaConversions._
import scala.collection.{immutable, mutable}

/**
 * Created by CK-Kim on 2014. 8. 1..
 */

object HtmlContent{
  val SNAPSHOT_CSS = "feona:snapshot_css"

  val INLINE_CSS = "feona:inline_css"
  val SNAPSHOT_JS = "feona:snapshot_js"
  val INLINE_JS = "feona:inline_js"
  val SNAPSHOT_IMG = "feona:snapshot_img_sprite"
}

abstract class HtmlContent(val dbRow:dbWebContent) extends WebContent {

  val html = Jsoup.parse(new SourceInputStream(source),"UTF-8",dbRow.originUrlStr)

  private[webcontents] def getScripts():Seq[(Node, Any)]
  private[webcontents] def getImageContent(url:String):Option[ImageContent]
  private[webcontents] def getSpritableInfo:Seq[(Node,SpriteInfo)]

  /**
   * Snapshot을 만들기위한 Tag를 달아준다.
   */
  def mkSnapshot = {
    if (isChanged)
      scriptHash.foreach {
        case (elem: Element, extCSS: ExternalCSSContent) =>
          extCSS.importRules
          val newCon = if (extCSS.isChanged) {
            extCSS.mkSnapshot()
            extCSS.databaseExport()
          }
          else extCSS.dbRow
          elem.attr(HtmlContent.SNAPSHOT_CSS, newCon.contentHash)
          if (elem.tagName.equals("style"))
            elem.attr("src", WebContent.getFEONA_URL(newCon))
          else
            elem.attr("href", WebContent.getFEONA_URL(newCon))
        case (elem, extJS: ExternalJavascriptContent) =>
          val newCon = if (extJS.isChanged) extJS.databaseExport() else extJS.dbRow
          elem.attr(HtmlContent.SNAPSHOT_JS, newCon.contentHash)
          elem.attr("src", WebContent.getFEONA_URL(newCon))
        case (elem: DataNode, inCSS: InternalCSSContent) =>
          if (inCSS.isChanged) {
            elem.childNode(0).asInstanceOf[DataNode].setWholeData(inCSS.toString)
          }
          elem.parent().attr(HtmlContent.INLINE_CSS, "true")
        case (elem, inJS: JavascriptContent) =>
          elem.attr(HtmlContent.INLINE_JS, "true")
        case (elem, inStyle: InlineStyleRule) =>
          elem.attr("style", inStyle.toString)
        case x =>
          println(x.toString())
      }

    import com.feona.feo.optimize.sprite.SpriteInfo.SpriteInfoWrites
    spritableInfo.foreach{case (elem,info)=>
      elem.attr(HtmlContent.SNAPSHOT_IMG,Json.toJson(info).toString())
    }
    this
  }

  def mkSimulation = {
    scriptHash.foreach {
      case (elem: Element, extCSS: ExternalCSSContent) =>
        val newCon = extCSS.dbRow
        if (elem.tagName.equals("style"))
          elem.attr("src", Simulation.simulationURL(newCon.projectId,newCon.contentHash))
        else
          elem.attr("href", Simulation.simulationURL(newCon.projectId,newCon.contentHash))
      case (elem, extJS: ExternalJavascriptContent) =>
        val newCon = extJS.dbRow
        elem.attr("src", Simulation.simulationURL(newCon.projectId,newCon.contentHash))
      case x =>
    }
    setChanged()
    this
  }

  def mkDeploy:String = {
    scriptHash.foreach {
      case (elem: Element, extCSS: ExternalCSSContent) =>
        val newCon = extCSS.dbRow
        elem.removeAttr(HtmlContent.SNAPSHOT_CSS)
        if (elem.tagName.equals("style"))
          elem.attr("src", newCon.deployPath)
        else
          elem.attr("href", newCon.deployPath)
      case (elem, extJS: ExternalJavascriptContent) =>
        val newCon = extJS.dbRow
        elem.removeAttr(HtmlContent.SNAPSHOT_JS)
        elem.attr("src", newCon.deployPath)
      case (elem: DataNode, inCSS: InternalCSSContent) =>
        elem.removeAttr(HtmlContent.INLINE_CSS)
        inCSS.setChanged()
        val newStr = WebContent.FEONA_URL_PATTERN.replaceAllIn(inCSS.toString,{urlMatch=>
          WebContent(urlMatch.group(2)).dbRow.deployPath
        })
        elem.childNode(0).asInstanceOf[DataNode].setWholeData(newStr)
      case (elem, inStyle: InlineStyleRule) =>
        val newStr = WebContent.FEONA_URL_PATTERN.replaceAllIn(inStyle.toString,{urlMatch=>
          WebContent(urlMatch.group(2)).dbRow.deployPath
        })
        elem.attr("style", newStr)

      case x =>
        //println(x.toString())
    }

    html.toString
  }

  override def toString: String = html.toString

  lazy val scriptHash:mutable.Buffer[(Node, Any)] = getScripts().toBuffer

  /**
   * CssObject와 해당 Object를 불러오는 Html Element를 매핑해놓은 맵
   */
  lazy val cssHash:mutable.Buffer[(Node, CSSContent)] =
    scriptHash.filter(_._2.isInstanceOf[CSSContent]).map(x=>x._1->x._2.asInstanceOf[CSSContent])

  /**
   * JsObject와 해당 Object를 불러오는 Html Element를 매핑해놓은 맵
   */
  lazy val jsHash:mutable.Buffer[(Node, JavascriptContent)] =
    scriptHash.filter(_._2.isInstanceOf[JavascriptContent]).map(x=>x._1->x._2.asInstanceOf[JavascriptContent])
  /**
   * inlineCss리스트
   */
  val inlineStyleList :mutable.Buffer[(Node, InlineStyleRule)] =
    scriptHash.filter(_._2.isInstanceOf[InlineStyleRule]).map(x=>x._1->x._2.asInstanceOf[InlineStyleRule])

  /**
   * Html내의 모든 imgTag
   */
  def imgTagList: mutable.Buffer[Element] = html.getElementsByTag("img")

  /**
   * spritable 이미지 연산결과 margin프리셋
   * object_id->image with margin
   */
  lazy val spritableInfo:mutable.Buffer[(Node,SpriteInfo)] = getSpritableInfo.toBuffer


  /**
   * StyleHash를 연산.
   * 하위 모든 Css를 모두 합침.
   * @return StyleHash
   */
  lazy val styleHash:Map[Node, mutable.Buffer[CSSDeclerationWithEditor]] = {
    val hash = mutable.HashMap[Node, mutable.Buffer[CSSDeclerationWithEditor]]()
    cssHash.flatMap(_._2.allStyleRules).foreach{rule =>
      val selector = rule._1.getAllSelectors

      val cssUrl = rule._2 match {
        case external: ExternalCSSContent => external.dbRow.originUrl
        case _ => dbRow.originUrl
      }

      val declBuffer = rule._1.getAllDeclarations.map {
        //declaration을 오브젝트 정보와 함께 묶음.
        x => new CSSDeclerationWithEditor(x, cssUrl, rule._2.addStyle(rule._1), rule._2.setChanged)
      }

      selector
        .map(CSSContentConst.CssWriter.getCSSAsString(_))
        .filter(!_.contains(":"))
        .flatMap(html.select)
        .distinct
        .foreach(addStyleRule(_,declBuffer))


      //inlineStyle를 추가함.
      //inline은 가장 우선순위가 높아서 마지막에 추가.
      inlineStyleList.foreach{case (element,inline)=>
        addStyleRule(element,inline.styles
          .map(x => new CSSDeclerationWithEditor(x, new URL(dbRow.originUrlStr), inline.addStyle, inline.setChanged)).toBuffer
        )
      }
    }

    //recentStyleRule에 추가함.
    def addStyleRule(element: Node, rule: mutable.Buffer[CSSDeclerationWithEditor]) {
      val styles = hash getOrElse (element,
        {val newBuf = mutable.Buffer[CSSDeclerationWithEditor]()
          hash += element -> newBuf
          newBuf}
        )
      styles ++= rule
    }

    hash.toMap
  }

  //변경사항 유무
  override private[feo] def isChanged: Boolean = {
    this.scriptHash.foreach {
      case (_, wc: WebContent) =>
        if (wc.isChanged) return true
      case _ =>
    }
    super.isChanged
  }
}

class HtmlPlainContent(dbRow:dbWebContent,subContents:immutable.Map[String,dbWebContent]) extends HtmlContent(dbRow){
  this.setChanged()
  implicit val pidWrap = ProjectIdWrapper(dbRow.projectId)

  override def mkSnapshot = {
    spritableInfo.foreach{case(node,info)=>
      spritableBackground.get(node).map(x=>Option(x.background_image_ref)).getOrElse(None).foreach{back=>
        Logger.debug("replace URL : "+CSSContentConst.CssWriter.getCSSAsString(back.decl))
        back.decl.getExpression.getAllMembers.foreach{
          case url:CSSExpressionMemberTermURI=>
            url.setURIString(WebContent.getFEONA_URL(info.imageHashcode))
          case _=>
        }
        back.setChanged.apply()
      }
    }
    super.mkSnapshot
  }
  
  override def getScripts(): Seq[(Node, Any)] ={
    val hash = mutable.Buffer[(Element,Any)]()
    initialTravel(html)

    def initialTravel(node: Node): Unit = node match {
      case element: Element =>
        element.tagName match {
          case "script" =>                                                  //script tag
            val src = element.attr("src")

            if (src.equals("")) {
              if (node.childNodes.size() > 0)
                node.childNode(0) match {
                  case dataNode: DataNode =>
                    addInternalJs(element, dataNode)
                  case _ =>
                    System.err.println("no DataNode in link Node")
                }
            }
            else {
              addExternalJs(element, src)
            }
          case "style" if element.attr("language").equals("javascript") =>  //javascript가있는 style태그
            val src = element.attr("src")
            if (src.equals("")) {
              if (node.childNodes.size() > 0)
                node.childNode(0) match {
                  case dataNode: DataNode =>
                    addInternalJs(element, dataNode)
                  case _ =>
                    System.err.println("no DataNode in link Node")
                }
            }
            else {
              addExternalJs(element, src)
            }
          case "link" if element.attr("rel").equals("stylesheet") =>        //stylesheet인 link
            val src = element.attr("href")
            if (src.equals("")) {
              if (node.childNodes.size() > 0)
                node.childNode(0) match {
                  case dataNode: DataNode =>
                    addInternalCss(element, dataNode)
                  case _ =>
                    System.err.println("no DataNode in link Node")
                }
            }
            else {
              addExternalCss(element, src)
            }
          case "style" =>                                                   //Style태그
            val src = element.attr("src")
            if (src.equals("")) {
              if (node.childNodes.size() > 0)
                node.childNode(0) match {
                  case dataNode: DataNode =>
                    addInternalCss(element, dataNode)
                  case _ =>
                    System.err.println("no DataNode in link Node")
                }
            }
            else {
              addExternalCss(element, src)
            }
          case _ =>                                                         //그외
            val inlineStyle = element.attr("style")
            if (!inlineStyle.equals(""))
              try {
                addInlineCss(element, CSSReaderDeclarationList
                  .readFromString(inlineStyle, ECSSVersion.CSS30)
                  .getAllDeclarations)
              }
              catch {
                case e: Exception =>
              }


            node.childNodes.foreach {
              x => initialTravel(x)
            }
        }

      case _ =>
        node.childNodes.foreach {
          x => initialTravel(x)
        }
    }

    /**
     * InternalCss를 생성하고 추가한다.
     * @param parent  Tag Element
     * @param content Internal Css의 내용
     */
    def addInternalCss(parent: Element, content: DataNode) {
      val cssObj = new InternalCSSContent(content.getWholeData,dbRow.originUrl,getSubCSSContent _)
      hash += parent -> cssObj
    }

    //subContents
    def getSubCSSContent(url:URL):ExternalCSSContent ={
      new ExternalCSSContent(subContents(url.getProtocol+"://"+url.getHost+url.getPath),getSubCSSContent)
    }

    /**
     * ExternalCss를 생성하고 추가한다.
     * @param parent  Tag Element
     * @param url 새로운 Css의 URL
     */
    def addExternalCss(parent: Element, url: String) {
      //todo 되돌려야함.!
      val absURL = Util.AbsoluteURL(dbRow.originUrl



        ,url)
      val cssObj = new ExternalCSSContent(subContents(absURL.getProtocol+"://"+absURL.getHost+absURL.getPath),getSubCSSContent)
      hash += parent -> cssObj
    }

    /**
     * Inline를 생성하고 추가한다.
     * @param parent  Tag Element
     * @param rule 파싱된 CSS Style Rule
     */
    def addInlineCss(parent: Element, rule: mutable.Buffer[CSSDeclaration]) {
      val inlineCss = new InlineStyleRule(rule)
      hash += parent -> inlineCss

    }

    /**
     * InternalJs를 생성하고 추가한다.
     * @param parent  Tag Element
     * @param content Internal Js의 내용
     */
    def addInternalJs(parent: Element, content: DataNode) {
      val jsObj = new JavascriptContent(content.getWholeData)
      hash += parent -> jsObj
    }

    /**
     * ExternalJs를 생성하고 추가한다.
     * @param parent  Tag Element
     * @param url 새로운 Js의 URL
     */
    def addExternalJs(parent: Element, url: String) {
      val absURL = Util.AbsoluteURL(this.dbRow.originUrl,url)
      val jsObj = new ExternalJavascriptContent(subContents(absURL.getProtocol+"://"+absURL.getHost+absURL.getPath))
      hash += parent -> jsObj
    }

    hash.toSeq
  }

  override def getImageContent(url:String):Option[ImageContent]={

    val absURL:URL = Util.AbsoluteURL(this.dbRow.originUrl,url)
    val dbRow = Option(subContents.getOrElse(absURL.getProtocol + "://" + absURL.getHost + absURL.getPath,
    { val result = WebContentManager.updatePageContent(this.dbRow.originUrl,Array(absURL))
      if(result.size ==0) null
      else result.apply(0)}))

    dbRow.map(new ImageContent(_))
  }
  lazy val spritableBackground =  styleHash
    .map{case (node,style)=>node -> SpriteInfo.extractBackgroundStyleRule(style.toSeq)}
    
  override def getSpritableInfo: Seq[(Node,SpriteInfo)] = {
    spritableBackground
      .map{case (node,backInfo) => node->SpriteInfo.calcSpriteInfo(backInfo,getImageContent)}
      .filter(_._2.isDefined)
      .map(x=>x._1->x._2.get)
      .toSeq  ++
      imgTagList.map(img=>img->getImageContent(img.attr("src"))).filter(_._2.isDefined)
        .map{case (node,Some(img:ImageContent))=>
        node -> SpriteInfo(img.dbRow.contentHash,img.dbRow.originUrlStr,0,0,0,0,img.width,img.height)}
  }
}

class HtmlSnapshotContent(dbRow:dbWebContent) extends HtmlContent(dbRow){

  override def getScripts(): Seq[(Element, Any)] ={
    //코드에 포함된 feona HashCode를 읽어들임.

    html.getElementsByAttribute(HtmlContent.SNAPSHOT_CSS)                                                                   //External CSS
      .map{x => val res= x->WebContent(x.attr(HtmlContent.SNAPSHOT_CSS));x.removeAttr(HtmlContent.SNAPSHOT_CSS);res}    ++
      html.getElementsByAttribute(HtmlContent.SNAPSHOT_JS)                                                                    //External JS
        .map{x => val res= x->WebContent(x.attr(HtmlContent.SNAPSHOT_JS));x.removeAttr(HtmlContent.SNAPSHOT_JS);res}     ++
      html.getElementsByAttribute(HtmlContent.INLINE_CSS)                                                                     //Inline CSS
        .map{x => val res = x -> new InternalCSSContent(x.childNode(0).asInstanceOf[DataNode].getWholeData, new URL(dbRow.originUrlStr))
        x.removeAttr(HtmlContent.INLINE_CSS);res}     ++
      html.getElementsByAttribute(HtmlContent.INLINE_JS)                                                                      //Inline JS
        .map{x => val res = x -> new JavascriptContent(x.childNode(0).asInstanceOf[DataNode].getWholeData)
        x.removeAttr(HtmlContent.INLINE_JS);res}
  }

  override def getImageContent(url:String):Option[ImageContent] = Option(WebContent(new URL(url)).asInstanceOf[ImageContent])


  private val imgSpriteInfo:Seq[(Node,String)] = html.getElementsByAttribute(HtmlContent.SNAPSHOT_IMG)
    .map{x => val res = x->x.attr(HtmlContent.SNAPSHOT_IMG)
    x.removeAttr(HtmlContent.SNAPSHOT_IMG);res}

  override def getSpritableInfo: Seq[(Node,SpriteInfo)] = {
    import com.feona.feo.optimize.sprite.SpriteInfo.SpriteInfoReads
    imgSpriteInfo
      .map{case (node,json) => node->Json.parse(json)}
      .map{case (node,json) => json.validate[SpriteInfo] match {
      case success: JsSuccess[SpriteInfo] =>
        Some(node->success.get)
      case error: JsError =>
        None
    }
    }
      .filter(_.isDefined)
      .map(_.get)
  }
}