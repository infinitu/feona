package com.feona.feo.webcontents.impl

import com.feona.feo.webcontents.WebContent
import models.{WebContent => dbWebContent}

/**
 * Created by CK-Kim on 2014. 8. 1..
 */
class DefaultWebContent(val dbRow : dbWebContent) extends WebContent{ }
