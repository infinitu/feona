package com.feona.feo.webcontents

import java.io.{PrintWriter, OutputStream}

/**
 * Created by CK-Kim on 2014. 8. 21..
 */
trait ChangeableContent {

  //변경사항 유무
  private[webcontents] var changed:Boolean=false

  //변경사항 유무
  private[feo] def isChanged:Boolean=changed

  /**
   * 변경사항이 있음을 체크한다..
   */
  private[feo] def setChanged(){changed=true}


  def fileExport(output:OutputStream): Unit = {
    val writer = new PrintWriter(output)
    writer.write(toString)
    writer.flush()
  }
}

