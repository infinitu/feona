package com.feona.feo.webcontents


import java.io._
import java.net.URL
import com.feona.feo.{ProjectIdWrapper, Util}
import com.feona.feo.io.GenContentManager
import com.feona.feo.webcontents.impl._
import models.{WebContent => dbWebContent, WebContents}
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import play.api.Play.current

import scala.collection.mutable
import scala.io.Source
import scala.ref.WeakReference
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB

/**
 * Created by CK-Kim on 2014. 8. 1..
 */

object WebContent {
  lazy val feonaHost = play.api.Play.current.configuration.getString("feona.host").getOrElse("www.feona.kr")
  val FEONA_URL_FORMAT = s"http://$feonaHost/%d/contents/%s"
  def getFEONA_URL(contentHash:String)(implicit pid : ProjectIdWrapper) = FEONA_URL_FORMAT.format(pid.value,contentHash)
  def getFEONA_URL(content:dbWebContent) = FEONA_URL_FORMAT.format(content.projectId,content.contentHash)
  val FEONA_URL_PATTERN = s"""http://$feonaHost/([0-9]+)/contents/(.+)""".r
  val contentWeakSingethon = mutable.HashMap[String,WeakReference[WebContent]]()

  def apply(dbRow : dbWebContent) : WebContent = {
    contentWeakSingethon.get(dbRow.contentHash).foreach(_.get.foreach{content =>
      return content.asInstanceOf[WebContent]
  })
    dbRow.contentType match {
      case ContentType.HTML =>  //pain html
        HtmlContent(dbRow)
      case ContentType.HTML =>  //snapshot html
        HtmlContent(dbRow)
      case ContentType.CSS =>  //plain css
        CSSContent(dbRow)
      case ContentType.CSS =>  //snapshot css
        CSSContent(dbRow)
      case ContentType.JS =>  //plain js
        JavascriptContent(dbRow)
      case ContentType.IMAGE =>  //plain image
        ImageContent(dbRow)
      case ContentType.IMAGE_PNG =>  //plain image
        ImageContent(dbRow)
      case ContentType.IMAGE_JPEG =>  //plain image
        ImageContent(dbRow)
      case ContentType.IMAGE_GIF =>  //plain image
        ImageContent(dbRow)
    }
  }

  def apply(dbRows : Seq[dbWebContent]):Seq[WebContent] = {
    dbRows.map(WebContent(_))
  }

  def apply(contentURL : URL):WebContent = {
    //dbRows.map(WebContent(_))
    //TODO Database연동
    ???
  }

  def withURLs(contentsURL : Seq[URL]):Seq[WebContent] = {
    //fileHashs.map(WebContent(_))
    //TODO Database연동
    ???
  }

  def apply(fileHash : String):WebContent = {
    contentWeakSingethon.get(fileHash).foreach(_.get.foreach(content => return content))
    WebContent(DB.withSession{implicit session=>WebContents.tQuery.filter(_.contentHash === fileHash).first})
  }

  def withHashes(fileHashs : Seq[String]):Seq[WebContent] = {
    DB.withSession{implicit session=>
      WebContents.tQuery.filter(_.contentHash inSet fileHashs).list
    }.map(WebContent(_))
  }
  def apply(file : File):WebContent = {
    //dbRows.map(WebContent(_))
    //TODO Database등록
    ???
  }

  def HtmlContent(dbRow:dbWebContent):HtmlContent={
    new HtmlSnapshotContent(dbRow)
  }

  def CSSContent(dbRow:dbWebContent):ExternalCSSContent={
    new ExternalCSSContent(dbRow)
  }

  def JavascriptContent(dbRow:dbWebContent):ExternalJavascriptContent={
    new ExternalJavascriptContent(dbRow)
  }

  def ImageContent(dbRow:dbWebContent):ImageContent={
    new ImageContent(dbRow)
  }
}

trait WebContent  extends ChangeableContent{
  val dbRow: dbWebContent
  lazy val source = Source.fromFile(dbRow.localPath)

  override def toString = Util.fileToString(dbRow.localFile)

  WebContent.contentWeakSingethon += dbRow.contentHash->WeakReference(this)

  def databaseExport(deploy:Option[String]=None): dbWebContent = {
    println("newCon!")
    GenContentManager.save(dbRow.contentType,dbRow.projectId,Some(dbRow),deploy){stream=>
      fileExport(stream)
    }
  }
}
