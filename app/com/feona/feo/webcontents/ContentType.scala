package com.feona.feo.webcontents

/**
 * DB에 들어가는 ContentType정의 및 ContentType으로 변환/복원
 */
object ContentType extends Enumeration {
  type ContentType = ContentTypeVal

  class ContentTypeVal(val name: String,val value : Int) extends Val(nextId, name)
  protected final def Value(name: String, value : Int): ContentTypeVal = new ContentTypeVal(name,value)

  val CONTENT_TYPE = "Content-Type"

  val ANY   = Value("*/*", 0)
  val HTML  = Value("text/html", 1)
  val CSS   = Value("text/css", 2)
  val JS    = Value("application/javascript", 3)
  val IMAGE = Value("image/*", 4)
  val IMAGE_JPEG = Value("image/jpeg", 5)
  val IMAGE_PNG = Value("image/png", 6)
  val IMAGE_GIF = Value("image/gif", 7)


  def fromInt : Int => ContentTypeVal = Array(ContentType.ANY,
    ContentType.HTML,
    ContentType.CSS,
    ContentType.JS,
    ContentType.IMAGE,
    ContentType.IMAGE_JPEG,
    ContentType.IMAGE_PNG,
    ContentType.IMAGE_GIF

  )

  def toInt : ContentTypeVal => Int = Map(ContentType.ANY   -> 0,
    ContentType.HTML  -> 1,
    ContentType.CSS   -> 2,
    ContentType.JS    -> 3,
    ContentType.IMAGE -> 4,
    ContentType.IMAGE_JPEG -> 5,
    ContentType.IMAGE_PNG -> 6,
    ContentType.IMAGE_GIF -> 7

  )

  def getNumFromContentType(seqContentType:Seq[String]) : ContentTypeVal = seqContentType.mkString match {
    case x if x.contains("html") => ContentType.HTML
    case x if x.contains("css") => ContentType.CSS
    case x if x.contains("stylesheet") => ContentType.CSS
    case x if x.contains("javascript") => ContentType.JS
    case x if x.contains("image/png") => ContentType.IMAGE_PNG
    case x if x.contains("image/jpeg") => ContentType.IMAGE_JPEG
    case x if x.contains("image/jpg") => ContentType.IMAGE_JPEG
    case x if x.contains("image/gif") => ContentType.IMAGE_GIF
    case x if x.contains("image") => ContentType.IMAGE
    case _ => ContentType.ANY
  }

}
