package com.feona.feo.history

import com.feona.feo._
import com.feona.feo.io.WebContentManager
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{HtmlContent, HtmlPlainContent}
import models.{WebContent=>dbWebContent,_}
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB

import scala.collection.immutable
import scala.concurrent.{Future,Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.slick.jdbc.JdbcBackend


///PageHistory(pageId: Int, contentHash: String, optId: Int)
object OptimizeHistory {
  type invoker = (Seq[HtmlContent])=>OptimizeRule
//  def newSnapshot(pages:Seq[Page])(implicit pid:ProjectIdWrapper):Seq[PageHistory] = {
////    val pageHistories = pages.filter(_.projectId == pid.toInt).map { page =>
////      val (dbOpt, dbPage, dbWC) = WebContentManager.updatePage(page)
////      PageHistory(dbPage.pageId, dbWC.contentHash, dbOpt.optimizeId)
////    }
////
////    /* db register*/
////    DB.withSession { implicit session =>
////      PageHistories.tQuery ++= pageHistories
////    }
////
////    pageHistories
//  }

//  def newSnapshot(pages:Seq[Page], pid : Int) = new WebContentManager
  //TODO Optimizer

  def optimize(projectId:Int,nowSnapshotInput:Int = -1):(invoker)=>Unit = {
    val snapshots =
      DB.withTransaction { implicit session =>
        val nowSnapshotId = if(nowSnapshotInput<0)
          Projects.tQuery.filter(_.projectId === projectId).map(_.snapshotId).first
        else
          nowSnapshotInput

        val snapshots = getSnapshotPages(projectId, nowSnapshotId)

        //현재 스냅샷 이후 Deprecated된 옵티마이즈들을 전부 지워버림.
        //cascading설정으로 Optimize History까지 전부 지워져야됨.
        OptimizeRules.tQuery.filter(_.projectId === projectId).filter(_.optimizeId > nowSnapshotId).delete
        snapshots
      }
    (optimizer: invoker) => DB.withTransaction { implicit session =>
      val optRule = optimizer(snapshots.map(_._2))

      val newSnapshots = snapshots.filter(_._2.isChanged).map { case (page, content) =>
        val newCon = content.mkSnapshot.databaseExport()
        PageHistory(page.pageId, newCon.contentHash, optRule.optimizeId)
      }

      PageHistories.tQuery ++= newSnapshots
      Projects.tQuery.filter(_.projectId === projectId).map(_.snapshotId).update(optRule.optimizeId)
      Future.successful(null)
    }

  }

  //session은 직접 디비 커넥션을 이용하지 않기 위함임. 호출하는 주체가 제공하도록 함.
  //TODO Check Join이 잘되는지.
  def getSnapshotPages(projectId:Int,snapshotId:Int)(implicit session:JdbcBackend.Session):Seq[(Page,HtmlContent)]={
    Pages.tQuery.filter(_.projectId === projectId).list.map{page=>
      page->WebContent(getSnapshotPage(page.pageId,snapshotId)).asInstanceOf[HtmlContent]
    }.toSeq
  }
  def getSnapshotPages(projectId : Int, pageIds:Seq[Int],snapshotId:Int)(implicit session:JdbcBackend.Session):Seq[(Page,HtmlContent)]={
    Pages.tQuery.filter(_.projectId === projectId).filter(_.pageId inSet pageIds).list.map{page : Page =>
      page->WebContent(getSnapshotPage(page.pageId,snapshotId)).asInstanceOf[HtmlContent]
    }.toSeq
  }

  def getSnapshotPage(pageId:Int,snapshotId:Int)(implicit session:JdbcBackend.Session)={
    PageHistories.tQuery
      .filter(_.pageId === pageId)
      .filter(_.optId <= snapshotId)
      .sortBy(_.optId.desc)
      .take(1)
      .innerJoin(WebContents.tQuery)
      .on(_.contentHash === _.contentHash)
      .map(x=>x._2)
      .firstOption
      .getOrElse{
        val page = Pages.tQuery.filter(_.pageId === pageId).first
        implicit val pidWrap = ProjectIdWrapper(page.projectId)
        val conHash =
          Await.result(WebContentManager.updatePage(page,
            OptimizeRules.tQuery
              .filter(_.projectId === pidWrap.value)
              .filter(_.optimizeType === OptimizeType.Initialize.dbValue)
              .first.optimizeId),60000 millis)
        WebContents.tQuery.filter(_.contentHash === conHash.contentHash).first
      }
  }

}

object Optimize_Initialize {
  def mkNewFeonaHtml(dbRow : dbWebContent, subContents :immutable.Map[String,dbWebContent] ) : dbWebContent = {
    val htmlCont = new HtmlPlainContent(dbRow, subContents)
    htmlCont.mkSnapshot.databaseExport()
  }

}
