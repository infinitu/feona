/**
 * Created by YeonjuMac on 2014. 7. 26..
 */
var port, server, service,
    system = require('system'),
    fs = require('fs');

/* query need url & dest
 ** (please give me .png dest)
 ** @return String{success, fail + why failed}
 */
function captureUrl( query, callback ) {

    page = require('webpage').create();

    var url = get(query, "url");
    var dest = get(query, "dest");

    if( url == null ) callback('fail : Usage : /capture?url={url}&dest={dest} ');
    if( dest == null ) dest = './'+url.split('//')[1] + '.png';

    page.open( url, function(status){
        if(status !== 'success') {
            callback('fail : url invalid');
        }
        else {
            var timeout = window.setTimeout(function () {
                var render = page.render(dest);
                console.log(page.url);
                if (render == true) callback(status);
                else callback('fail to generate rendering image');
            }, 10000 );
        }
    });
}

function initResult(url, dest) {
    var r = new Object();

    r.url = url;
    r.dest = fs.absolute(dest);
    if( r.dest.charAt(dest.length-1) == '/') r.dest = r.dest + "gen-index.html";
    r.pages = [];

    return r;
}

/* query need url & dest
 ** @return url, dest, request urls
 */
function crawl ( query, callback ) {
    var page = require('webpage').create();

    var url = get(query, "url");
    var dest = get(query, "dest");

    if( url == null ) callback('fail : Usage : /crawl?url={url}&dest={dest} ');
    if( dest == null ) { dest = './'+url.split('//')[1] }

    var result = initResult(url, dest);

    console.log(4)
    page.onUrlChanged = function () {
        result = initResult(page.url, fs.absolute(dest));

        if( dest == null ) result.dest = fs.absolute('./'+page.url.split('//')[1]);
        if( result.dest.charAt(dest.length-1) == '/') result.dest = result.dest + "gen-index.html";

        console.log( "page url is changed : " + page.url);

        console.log(JSON.stringify(result));
    }

    page.onResourceReceived = function (response) {
        if(response.stage == 'start') {
            console.log("_"+response.url);
            return;
        }
        console.log(response.url);
        result.pages.push(response.url);
    };

    page.onClosing = function(closingPage) {

        fs.makeTree(result.dest.substring(0, result.dest.lastIndexOf('/')));
        fs.write(result.dest, closingPage.content, 'w');

        console.log("downloaded in "+result.dest);

        callback( JSON.stringify(result) );
    }

    page.open(url, function (status) {
        if (status !== 'success') {
            callback('fail : url invalid');
        } else {
            setTimeout(function() {
                page.close();
            }, 20000);
        }
    });

}

//handling Query
function hQ ( query ) {
    if ( typeof query[0] === "object") return query;
    for( var i = 0 ; i < query.length ; i++) {
        query[i] = query[i].split('=');
    }

    return query;
}
function get ( query, key ) {
    query = hQ(query);
    for(var i = 0 ;i < query.length ; i++){
        if( query[i][0].toLowerCase() == key.toLowerCase() )
            return query[i][1];
    }
    return null;
}


if( system.args.length !== 2) {
    console.log('Usage : feedle.js <portnumber>');
    phantom.exit();
} else {
    port = system.args[1];
    server = require('webserver').create();

    service = server.listen(port, function(request, response) {
        console.log('Request at ' + new Date());

        var _slash = request.url.indexOf('/');
        var _quest = request.url.indexOf('?');


        var api = request.url.substring(_slash+1, _quest);
        var query = request.url.substring(_quest+1).split('&');
        console.log( "api : " + api.toUpperCase());

        response.statusCode = 200;
        response.headers = {
            'Cache' : 'no-cache',
            'Content-Type' : 'text/html'
        }

        switch(api.toUpperCase()) {
            case "CAPTURE" :
            case 'CAPTURE' :
                captureUrl(query, mkHTML(response));
                break;

            case "CRAWL" :
            case 'CRAWL' :
                crawl(query, mkHTML(response));
                break;

            default :
                mkHTML(response)(" api list : capture / crawl ");
                break;
        }


    });

    if (service) {
        console.log('Web server running on port ' + port);
    } else {
        console.log('Error : Could not create web server listen on port' + port);
        phantom.exit();
    }
}

function mkHTML(response) {
    return function (str) {
        console.log(str);
        response.write(str);
        response.close();
    }
}