package com.feona.feo.deploy

import java.io.File
import java.nio.file.{Paths, Files}

import com.feona.feo.ProjectIdWrapper
import com.feona.feo.io.ContentManager
import models.WebContents
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB

object Deploy extends ContentManager{

  def hint( ) = {

  }
  def deploy(hashcodeList:Seq[String])(implicit pWrapper : ProjectIdWrapper) : Option[File] = {
    // localpath -> deploypath
    val filesToZip : List[(String, String)]= DB.withSession { implicit session =>
      WebContents.tQuery.filter(_.projectId === pWrapper.value).filter(_.contentHash inSet hashcodeList).list
    }.map{wc=> wc.localPath -> wc.deployPath.replaceAll("https?://","")}

    if(filesToZip.nonEmpty)
      Some(zipping(filesToZip, PROJECT_PATH))
    else
      None
  }

  def getFile(name:String)(implicit pWrapper : ProjectIdWrapper)={
    new File(PROJECT_PATH+name)
  }

  private def zipping(listFiles : List[(String, String)], root : String) : File = {
    import java.io.{BufferedInputStream, BufferedOutputStream, FileInputStream, FileOutputStream}
    import java.util.zip.{ZipEntry, ZipOutputStream}

    /*해당 project folder에서 생성되게함 ex : /{HOME DIR}/FEONA/{PID}/{SystemCurrentTimeMillis}.zip */
    val out = root + System.currentTimeMillis()+".zip"

    Files.createDirectories(Paths.get(root))
    val file = Files.createFile(Paths.get(out))

    val fos = new FileOutputStream(out)
    val zip = new ZipOutputStream(new BufferedOutputStream(fos))

    listFiles.foreach { case (originPath, entryPath) =>
      zip.putNextEntry(new ZipEntry(entryPath))
      val in = new BufferedInputStream(new FileInputStream(originPath))
      var b = in.read()
      while (b > -1) {
        zip.write(b)
        b = in.read()
      }
      in.close()
      zip.closeEntry()
    }
    zip.close()

    new File(out)
  }
}
