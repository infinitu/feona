package com.feona.feo.io

import java.io.File

import com.feona.feo.ProjectIdWrapper
import com.feona.phantomjs.PhantomJS
import models.Page
import scala.concurrent._

object DashboardContentManager extends ContentManager {
  def mkSnapshot(page : Page) : Future[String] = {
    implicit val pidWrap = ProjectIdWrapper(page.projectId)
    val snapshot_dest = PROJECT_PATH + "CAPTURE/"  + System.currentTimeMillis() + ".png"
    PhantomJS.capture(page.url, new File(snapshot_dest)).asInstanceOf[Future[String]]
  }


  def getSnapshot()= {/* todo:getSnapshot */}
}
