package com.feona.feo.io

import java.io.File
import java.net.URL

import com.feona.feo.ProjectIdWrapper
import models.{WebContent, WebContents}
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB

trait ContentManager extends HashGenerator{
  private val FEONA_PATH = System.getProperty("user.home") + "/FEONA/"
  protected def PROJECT_PATH(implicit pid : ProjectIdWrapper) = FEONA_PATH + pid.value + "/"
  protected def TEMP_PATH(implicit pid : ProjectIdWrapper) = PROJECT_PATH + "_temp/"

  lazy val feonaHost = play.api.Play.current.configuration.getString("feona.host").getOrElse("www.feona.kr")
  private val FEONA_URL = s"http://$feonaHost/content/"
  private def PROJECT_URL(implicit pid : ProjectIdWrapper) = FEONA_URL + pid.value + "/"




  def getPagePath(p : URL => String)(url : URL)(implicit pid : ProjectIdWrapper) : String = {
    val pathLength = url.getPath.length
    if (pathLength != -1 || url.getPath.charAt(pathLength - 1) != '/') p(new URL(url.toString + ".html"))
    else p(new URL(url.toString + "index.html"))
  }

  //remove protocol and query
  def getLocalPath(url : URL)(implicit pid : ProjectIdWrapper) : String = PROJECT_PATH + url.getHost + url.getPath +
    {if(url.getQuery == null || url.getQuery.equals("")) "" else "@"+getQueryForFileName(url.getQuery) }
  def getCapturePath(implicit pid : ProjectIdWrapper) : String = PROJECT_PATH +"CAPTURE/"
  def getTempPath(url : URL)(implicit pid : ProjectIdWrapper) : String = TEMP_PATH + url.getHost + url.getPath +
    {if(url.getQuery == null || url.getQuery.equals("")) "" else "@"+getQueryForFileName(url.getQuery) }
  def getGenContentLocalPath(contentHash:String)(implicit pid : ProjectIdWrapper) : String = PROJECT_PATH + "/gen/" + contentHash
  def getDefaultTempPath(implicit pid : ProjectIdWrapper) : String = TEMP_PATH
  def getQueryForFileName(str:String)={
    if(str.length<32)
      str
    else
      getMD5(str)
  }

  //filePath after pid folder
  def getGenFileURL(contentHash : String)(implicit pid : ProjectIdWrapper) = new URL(PROJECT_URL + contentHash)

  //remove protocol and encoding query
//  def getLocalPath(url : URL, hash : String) : File= {
//    val path = url.getPath
//    val dot_index = path.lastIndexOf('.')
//    val slash_index = path.lastIndexOf('/')
//
//    val url2path = s"${url.getHost + path.substring(slash_index+1, dot_index)}.$hash.${ path.substring(dot_index+1) }"+
//      s"${if(url.getQuery!=null){"@"+URLEncoder.encode(url.getQuery, "UTF-8")}}"
//
//    new File(PROJECT_PATH + url2path)
//  }


  def registerDB_withNoHashDuplication(wc : WebContent) : Option[WebContent] = {
    val existOption : Option[WebContent] = DB.withSession { implicit session =>
      implicit val pidWrap = ProjectIdWrapper(wc.projectId)
      WebContents.tQuery.filter(_.contentHash === wc.contentHash).firstOption match {
        case None =>
          val feona_file = new File(getGenContentLocalPath(wc.contentHash))
          feona_file.getParentFile.mkdirs()
          if(feona_file.exists())feona_file.delete()
          wc.localFile.renameTo(feona_file)

          val new_wc = WebContent(wc.contentHash, wc.originUrlStr, feona_file.toString,wc.deployPath, wc.contentTypeVal, wc.projectId)
          WebContents.tQuery += new_wc

          Some(new_wc)
        case Some(x)  => Some(x)//throw new Exception("already have same Hash content.");
      }
    }

    existOption
  }
  def registerDB_withNoHashDuplication(raw_webContent : Array[WebContent])(implicit pidWrap : ProjectIdWrapper) : List[WebContent] = {
    val nonDuplicatedContents : Array[WebContent] = raw_webContent.groupBy[String](_.contentHash).map(_._2.head).toArray
    val already_exists = DB.withSession { implicit session =>
      val q = for (
        w <- WebContents.tQuery.filter(_.projectId === pidWrap.value) if w.contentHash inSetBind raw_webContent.map(_.contentHash)
      ) yield w

      q.list
    }

    val not_exist_hash = nonDuplicatedContents.map(_.contentHash) diff already_exists.map(_.contentHash)

    if( not_exist_hash.nonEmpty ) {
      //pid 하위 폴더로 이동하고 db 등록
      val new_web_content = nonDuplicatedContents.filter{x => not_exist_hash.contains(x.contentHash)}.par.map { wc =>
        val feona_file = new File(getLocalPath(wc.originUrl))
        feona_file.getParentFile.mkdirs()
        if(feona_file.exists())feona_file.delete()
        wc.localFile.renameTo(feona_file)
        WebContent(wc.contentHash, wc.originUrlStr, feona_file.toString, wc.deployPath, wc.contentTypeVal, wc.projectId, wc.isGen)
      }.toList

//      new_web_content.sortBy(_.contentHash).foreach{x=>
//        println(x.contentHash+"  ->  "+x.originUrlStr)
//      }

      DB.withSession { implicit session =>
        WebContents.tQuery ++= new_web_content
      }

      already_exists ++: new_web_content
    } else {
      already_exists
    }
  }
}



