package com.feona.feo.io

import java.io.{FileInputStream, File}
import java.security.{DigestInputStream, DigestOutputStream, MessageDigest}
import scala.io.Source

trait HashGenerator {
  def MD = MessageDigest.getInstance("MD5")

  //todo digestOutputStream, digestInputstream 둘다 생성한곳에서 flush 하고 close하는게 맞는거아닌가..
  def getMD5(digestOutputStream : DigestOutputStream) : String = {
    val ret = digestOutputStream.getMessageDigest.digest().map("%02x".format(_)).mkString
    digestOutputStream.flush()
    digestOutputStream.close()
    ret
  }

  def getMD5( content : String ) : String = {
    MD.digest(content.getBytes).map("%02x".format(_)).mkString
  }

  def getMD5( digestInputStream : DigestInputStream ) : String = {
    val ret = digestInputStream.getMessageDigest.digest().map("%02x".format(_)).mkString
    digestInputStream.close()
    ret
  }

  def getMD5( file : File ) : String = {
    getMD5(Source.fromFile(file).mkString)
  }


  def getSHA256( content : String ) : String = {
    val MD = MessageDigest.getInstance("SHA-256")
    MD.digest(content.getBytes).map("%02x".format(_)).mkString
  }

}
