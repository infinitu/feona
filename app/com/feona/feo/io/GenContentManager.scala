package com.feona.feo.io

import java.io.{OutputStream, FileOutputStream}
import java.security.DigestOutputStream

import com.feona.feo.ProjectIdWrapper
import com.feona.feo.webcontents.ContentType.ContentType
import models.WebContent
import com.feona.feo.webcontents

object GenContentManager extends ContentManager with HashGenerator {

  //filePath after pid folder
  def save(contentType : ContentType, pid : Int, originContent:Option[WebContent] = None, deployPath:Option[String] = None)(f : OutputStream => Unit) : WebContent = {
    implicit val pidWrap = ProjectIdWrapper(pid)
    val new_file = TEMP_PATH + getMD5((System.currentTimeMillis() + pid).toString)

    val outputStream = new FileOutputStream(new_file)
    val digestOutputStream = new DigestOutputStream(outputStream, MD)


    var ok = false

    try{
      digestOutputStream.on(true)
      f(digestOutputStream)
      //outputStream.flush()
      val contentHash = getMD5(digestOutputStream)

      ok = true

      val new_wc = WebContent(contentHash,
                              originContent.map(_.originUrlStr).getOrElse(webcontents.WebContent.getFEONA_URL(contentHash)),
                              new_file.toString,
                              deployPath getOrElse originContent.map(_.deployPath).getOrElse(webcontents.WebContent.getFEONA_URL(contentHash)),
                              contentType.value, pid)

      println("save for "+ new_wc)
      registerDB_withNoHashDuplication(new_wc).get

    }catch{
      case ex:Exception=>ex.printStackTrace();null
    }
    finally {
      if(ok) {
        outputStream.close()
        digestOutputStream.close()
      }
      else {
        try{ outputStream.close(); digestOutputStream.close() }catch { case _ : Throwable => }
      }
    }
  }
}