package com.feona.feo.io

import java.io.{File, FileOutputStream}
import java.net.{ConnectException, URL}
import java.security.{MessageDigest, DigestOutputStream}

import com.feona.feo.{Util, ProjectIdWrapper}
import com.feona.feo.history.Optimize_Initialize
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.webcontents.ContentType
import com.feona.phantomjs.PhantomJS
import models.{WebContent => dbWebContent, _}
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB
import play.api.libs.iteratee.Iteratee
import play.api.libs.ws.WS

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{Promise, Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object WebContentManager extends ContentManager with HashGenerator{

  /*get all dbWebContent*/
  def getAll(implicit pid : ProjectIdWrapper) = DB.withSession { implicit session =>
    WebContents.tQuery.filter(_.projectId === pid.toInt).list
  }

  /*update contents(html/js/css.. etc.) included in all DB-registered pages*/
  def updateAll(implicit pid : ProjectIdWrapper) = {
    //TODO 기능 리패키징,
    val optId : Int = DB.withSession { implicit session =>
      OptimizeRules.tQuery returning OptimizeRules.tQuery.map(_.optimizeId) +=
        OptimizeRule(0, pid, OptimizeType.Initialize, new java.sql.Timestamp(System.currentTimeMillis()))
    }
    DB.withSession { implicit session =>
      Projects.tQuery.filter(_.projectId === pid.value).map(_.snapshotId).update(optId)
    }
    val pages : List[Page] = DB.withSession { implicit session =>
      Pages.tQuery.filter(_.projectId === pid.toInt).list
    }

    pages.map(updatePage(_,optId))
  }

  val crawlSingleton = mutable.HashMap[Int,Promise[PageHistory]]()
  def updatePage(page : Page, optId : Int)(implicit pid : ProjectIdWrapper):Future[PageHistory]={
    val tempFile = new File(getPagePath(getTempPath)(page.url))
    /*crawl html from phantom JS*/
    PhantomJS.crawl(page.url, tempFile,{result=>
      /*download content*/
      val tempPage = dbWebContent(getMD5(result.dest), result.url.toString, tempFile.toString, result.url.toString, ContentType.HTML.value, pid, false)
      val subContents = updatePageContent(page.url,result.pages)
      /*generate html with feona hash and move to pid folder*/
      val newContent = Optimize_Initialize.mkNewFeonaHtml(tempPage, subContents.map { x => x.originUrl.getProtocol + "://" + x.originUrl.getHost + x.originUrl.getPath -> x}.toMap)
      DB.withSession { implicit session =>
        PageHistories.tQuery += PageHistory(page.pageId, newContent.contentHash, optId)
      }
      PageHistory(page.pageId, newContent.contentHash, optId)
    })
  }

  private[feo] def updatePageContent(pageUrl:URL,urls : Array[URL])(implicit pid : ProjectIdWrapper) : List[dbWebContent]= {
    /*download content to temp folder*/
    val raw_web_content : Array[dbWebContent] = urls.distinct.par.flatMap{ url =>
      try {
        val temp_file = new File(getTempPath(url))
        val futureResponse = WS.url(url.toString).getStream()

        case class DownloadContent(hash: String, contentType: ContentType.ContentTypeVal, file: File)
        val f_download: Future[DownloadContent] = futureResponse.flatMap {
          case (headers, body) =>
            if(headers.status >= 300) throw new Exception("status 300 미만임")
            val contentType = ContentType.getNumFromContentType(headers.headers("Content-Type"))
            if (!temp_file.exists) {
              temp_file.getParentFile.mkdirs()
              temp_file.createNewFile()
            }
            val outputStream = new FileOutputStream(temp_file)
            val digestOutputStream = new DigestOutputStream(outputStream, MD)

            val iter = Iteratee.foreach[Array[Byte]] { bytes =>
              digestOutputStream.write(bytes)
            }

            (body |>>> iter).andThen {
              case result =>
              outputStream.close()
                result.get
            }.map { _ =>
              val contentHash = getMD5(digestOutputStream)
              digestOutputStream.close()
              DownloadContent(contentHash, contentType, temp_file)
            }
        }

        val download : DownloadContent = Await.result(f_download, 30 seconds)

        Some(dbWebContent(download.hash, url.toString, download.file.toString, Util.RelativeURL(pageUrl,url),download.contentType.value, pid, false))
      }catch {
        case e : ConnectException =>
          println("DEBUG in WCM.updatePageContent(ConnectException) : " + url)
          None
        case e : Exception =>
          println("DEBUG in WCM.updatePageContent(Exception) : " + url)
          None
      }
    }.toArray

    if(raw_web_content.nonEmpty)
      registerDB_withNoHashDuplication(raw_web_content)
    else
      List()
  }
}