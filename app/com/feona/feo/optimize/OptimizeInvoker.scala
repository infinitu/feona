package com.feona.feo.optimize

import java.io.File

import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.HtmlContent
import models.{WebContent => dbWebContent, _}
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._
import play.api.Play.current

/**
  * Created by CK-Kim on 2014. 8. 24..
  */
trait OptimizeInvoker {
  private[optimize] def applyChanges(pageId:Int, htmlContent:HtmlContent, optimizeId:Int){
    if(htmlContent.isChanged) {
      htmlContent.mkSnapshot.databaseExport()
    }
  }

  def invoke(rule:OptimizeRule, htmlContents:Seq[HtmlContent])

  // pid->HtmlContent
  def applyOptimize(rule:OptimizeRule,htmlContents:Seq[(Int,HtmlContent)]){
    val pages = //TODO
      DB.withSession { implicit session =>

        val recentSnapshotQuery =
          for {
            page <- Pages.tQuery.filter(_.projectId === rule.projectId)
            recentSnapshot <- PageHistories.tQuery
                                .filter(_.pageId === page.pageId)
                                .sortBy(_.optId.desc)
                                .take(1)
            content <- WebContents.tQuery.filter(_.contentHash === recentSnapshot.contentHash)
          } yield (page.pageId,content)

        recentSnapshotQuery.list.map(x=>x._1->WebContent.HtmlContent(x._2))
      }
    invoke(rule,pages.map(_._2))
    pages.foreach(x=>applyChanges(x._1,x._2,rule.optimizeId))
  }
}
