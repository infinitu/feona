package com.feona.feo.optimize.replace

import com.feona.feo.Util
import com.feona.feo.webcontents.ContentType
import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{CSSContent, ExternalCSSContent, ExternalJavascriptContent, HtmlContent}
import com.helger.css.decl.CSSURI
import models.{WebContent => dbWebContent}

import scala.collection.JavaConversions._

/**
 * Created by CK-Kim on 2014. 8. 27..
 */
object Replace {
  case class OldAndNew(oldContent:dbWebContent,newContent:dbWebContent)

  def apply(oldContent:dbWebContent, newContent:dbWebContent)={
    //종류에 따라 변경방식이 변함.
    oldContent.contentType match {
      case ContentType.CSS =>
        (htmlobj: HtmlContent) => {
          //Html에 직접 기술된 External Css를 먼저 바꿈.
          //이것부터 바꿔야 이미 변경사항이 적용되어 등록된 CSS를 또 바꾸지 않게됨.
          htmlobj.cssHash.foreach{
            case (node,external:ExternalCSSContent) if external.dbRow.originUrl.equals(oldContent.originUrl)=>
              node.attr("href", newContent.originUrl.toString)
              val oldIdx = htmlobj.cssHash.indexOf(node->external)
              htmlobj.cssHash.remove(oldIdx)
              htmlobj.cssHash.insert(oldIdx,node -> WebContent(newContent).asInstanceOf[CSSContent])
            case _=>
          }

          //다음은 CssObject들이 Import한 애들을 바꿈.
          htmlobj.cssHash.foreach(x=>replaceImportRules(x._2))

          //모든 CSSRule에 Import된 CssObject를 탐색함.
          def replaceImportRules(cssobj:CSSContent){
            cssobj.importedCSS.foreach(replaceImportRules)
            Option(cssobj.parsedCSS).foreach {
              _.getAllImportRules
                .filter(x=>Util.AbsoluteURL(cssobj.refURL,x.getLocation.getURI).toString equals oldContent.originUrl.toString)
                .foreach { x =>
                  x.setLocation(new CSSURI(newContent.originUrlStr))
              }
            }
          }



        }
      case ContentType.JS =>
        (htmlobj: HtmlContent) => {
          //JS의 경우 단순히 External만 바꿔주는 형태.
          htmlobj.jsHash.foreach{
            case (node,external:ExternalJavascriptContent)
              if external.dbRow.originUrl.equals(oldContent.originUrl)=>
              node.attr("src", newContent.originUrlStr)
            case _=>
          }
        }

      case ContentType.IMAGE  => imageInvoker(newContent)
      case ContentType.IMAGE_JPEG  => imageInvoker(newContent)
      case ContentType.IMAGE_GIF  => imageInvoker(newContent)
      case ContentType.IMAGE_PNG  => imageInvoker(newContent)

      case _=>
        (htmlobj: HtmlContent) => {}
    }
  }

  def imageInvoker(newContent:dbWebContent) = (htmlobj: HtmlContent) => {
    //Image의 경우 Img Tag와 Background 로 나눠서 변경
    import Util._
    htmlobj.imgTagList.foreach {
      case x if AbsoluteURL(htmlobj.dbRow.originUrl, x.attr("src")).toString.equals(newContent.originUrlStr) =>
        x.attr("src", newContent.originUrl)
    }

    //CSSBackground
    htmlobj.styleHash.foreach(_._2.foreach { x =>
      x.decl.getExpression.getAllMembers.foreach {
        case uri: CSSURI =>
          if (Util.AbsoluteURL(x.url, uri.getURI).toString.equals(newContent.originUrlStr)) {
            uri.setURI(newContent.originUrl.toString)
            x.setChanged()
          }
        case _ =>
      }
    })
  }


}
