package com.feona.feo.optimize.externalize

import com.feona.feo.io.GenContentManager
import com.feona.feo.webcontents.ContentType
import com.feona.feo.webcontents.impl.{HtmlContent, JavascriptContent, CSSContent}
import org.jsoup.nodes.{Element, Node}
import scala.collection.JavaConversions._

/**
 * Created by CK-Kim on 2014. 8. 28..
 */
object externalize {

  def mkExternal(htmlContent: HtmlContent, node: Element, script: Any) =
    (htmlContents: Seq[HtmlContent]) => htmlContents.filter(_ == htmlContent).foreach { htmlContent =>
      script match {
        case js: JavascriptContent =>
          val newContent =
            GenContentManager.save(ContentType.JS,htmlContent.dbRow.projectId) { stream =>
              js.fileExport(stream)
            }
          val url = newContent.originUrlStr
          node.childNodes.foreach(_.remove)
          node.tagName("script")
          node.attributes.iterator.foreach(key => node.removeAttr(key.getKey))
          node.attr("src", url)

        case css: CSSContent =>
          val newContent =
            GenContentManager.save(ContentType.JS,htmlContent.dbRow.projectId) { stream =>
              css.fileExport(stream)
            }
          val url = newContent.originUrlStr
          node.childNodes.foreach(_.remove)
          node.tagName("link")
          node.attributes.iterator.foreach(key => node.removeAttr(key.getKey))
          node.attr("href", url)
          node.attr("rel", "stylesheet")
      }
    }
}
