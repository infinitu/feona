package com.feona.feo.optimize.merge

import java.sql.Timestamp

import com.feona.feo.optimize.OptimizeType
import com.feona.feo.optimize.replace.Replace
import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.webcontents.{ContentType, WebContent}
import com.feona.feo.webcontents.impl.{ExternalCSSContent, ExternalJavascriptContent, HtmlContent}
import models.{WebContent => dbWebContent, _}
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._

object Merge {
  def mkMerge(projectId:Int,contentHashList:Seq[String]) = DB.withSession{ implicit session=>
    val newOptRuleId = OptimizeRules.tQuery returning OptimizeRules.tQuery.map(_.optimizeId) +=
      new OptimizeRule(0, projectId, OptimizeType.Merge, new Timestamp(System.currentTimeMillis))
    val newOptRule = new OptimizeRule(newOptRuleId , projectId, OptimizeType.Merge, new Timestamp(System.currentTimeMillis))
    MergeRules.tQuery ++= contentHashList.zipWithIndex.map{x => MergeRule(x._1, newOptRuleId, x._2)}
    val newCon= createMergeContent(contentHashList,newOptRuleId)
    CssSpriteResults.tQuery += CssSpriteResult(newOptRuleId,newCon.contentHash)
    Invoker.invoke(contentHashList,newCon, newOptRule) _
  }

  def createMergeContent(contentHashList:Seq[String],newOptId:Int, removeImports:Boolean = true):dbWebContent={
    val contents = WebContent.withHashes(contentHashList)
    contents.head match {
      case css:ExternalCSSContent=>
        CSSMerge.CSSMergeExport(contents.asInstanceOf[Seq[ExternalCSSContent]],newOptId, removeImports)
      case js:ExternalJavascriptContent=>
        JSMerge.JSMergeExport(contents.asInstanceOf[Seq[ExternalJavascriptContent]],newOptId)
    }
  }

  private object Invoker{
    def invoke(oldHashes:Seq[String], newCon:dbWebContent, newOpt:OptimizeRule)(htmlContents: Seq[HtmlContent]) = DB.withSession{ implicit session=>

      htmlContents.filter { html =>

        html.scriptHash.map {
          case (node, wc: WebContent) =>
            Option(wc.dbRow.contentHash)
          case _ =>
            None
        }.filter {
          case Some(hash) =>
            oldHashes.contains(hash)
          case None =>
            false
        }.map(_.get).distinct.size == oldHashes.size
      }
      .foreach{ html=>
        val scripts = html.scriptHash.filter {
          case (node, wc: WebContent) =>
            oldHashes.contains(wc.dbRow.contentHash)
          case _ => false
        }

        scripts.map(_._1).foreach(println)

        html.scriptHash+=scripts.head._1->WebContent(newCon)
        scripts.tail.foreach(_._1.remove())
        html.scriptHash --= scripts
        html.setChanged()
      }
      newOpt
    }
  }

}
