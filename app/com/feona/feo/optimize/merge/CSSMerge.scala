package com.feona.feo.optimize.merge

import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.webcontents.impl.{CSSContentConst, CSSContent, ExternalCSSContent}
import models.WebContent

private[merge] object CSSMerge {

  def CSSMergeExport(css:Seq[ExternalCSSContent],newOptId:Int, removeImports:Boolean):WebContent = {
    val merged = css(0)
    merged.setChanged()
    merged.parsedCSS.removeAllImportRules()
    merged.parsedCSS.removeAllRules()
    css.flatMap(_.allStyleRules).map(_._1).distinct.foreach(merged.parsedCSS.addRule(_))
    css.flatMap(_.allFontFaceRules).map(_._1).distinct.foreach(merged.parsedCSS.addRule(_))
    merged.mkSnapshot().databaseExport(Some("/merge/merge_%d.css".format(newOptId)))
  }
}
