package com.feona.feo.optimize.merge

import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.webcontents.impl.ExternalJavascriptContent
import models.WebContent
import play.api.libs.ws.WS
import play.api.Play.current

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Await}
import scala.concurrent.duration._

private[merge] object JSMerge {
  def JSMergeExport(js:Seq[ExternalJavascriptContent],newOptId:Int):WebContent = {
    val merge = Await.result(mergeJS(js.mkString), 30 seconds)
    js.head.contentChange(merge).databaseExport(Some("/merge/merge_%d.js".format(newOptId)))
  }

  //미니파니 해서 합치고 다시미니파이 하면 되지 않을까
  private[merge] def mergeJS(code:String):Future[String] = {
    val ws = WS.client
    ws.url("http://closure-compiler.appspot.com/compile")
      .post(Map(
        "js_code" -> Seq(code),
        "output_info" -> Seq("compiled_code"),
        "output_format" -> Seq("text"),
        "compilation_level" -> Seq("SIMPLE_OPTIMIZATIONS")
        ))
      .map{ response=>
        response.body
      }
  }
}
