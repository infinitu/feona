package com.feona.feo.optimize

/**
 * Created by CK-Kim on 2014. 8. 24..
 *
 * DB에 들어가는 OptmizeType를 정의 및 OptimizeType변환/복원
 */
object OptimizeType extends Enumeration{
  type OptimizeType = OptimizeTypeValue

  class OptimizeTypeValue(val dbValue:Int) extends Val(nextId)
  val Initialize = new OptimizeType(0)
  val CssSprite = new OptimizeType(1)
  val Image = new OptimizeType(2)
  val Merge = new OptimizeType(3)
  val Minify = new OptimizeType(4)
  val Cdn = new OptimizeType(5)

  implicit def OptimizeType2DBValue(optType:OptimizeType) = optType.dbValue
  implicit def DBValue2OptimizeType(dbVal:Int) = dbVal match {
    case Initialize.dbValue=>Initialize
    case CssSprite.dbValue=>CssSprite
    case Image.dbValue=>Image
    case Merge.dbValue=>Merge
    case Minify.dbValue=>Minify
    case Cdn.dbValue=>Minify
  }
  implicit def DBValue2StringVal(dbVal:Int) = dbVal match {
    case Initialize.dbValue=>"Initialize"
    case CssSprite.dbValue=>"CssSprite"
    case Image.dbValue=>"Image"
    case Merge.dbValue=>"Merge"
    case Minify.dbValue=>"Minify"
    case Cdn.dbValue=>"Minify"
  }

}
