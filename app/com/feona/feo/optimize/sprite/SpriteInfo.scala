package com.feona.feo.optimize.sprite

import java.net.URL

import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{CSSContentConst, CSSContent, ImageContent}
import com.helger.css.decl.CSSDeclaration
import play.api.libs.json.{Reads, JsPath, Writes}
import play.api.libs.functional.syntax._

/**
 * Created by CK-Kim on 2014. 8. 19..
 */
object SpriteInfo {

  //정의가 되어있지 않을때에 기본 설정값.
  private val default_background_image: String = "none"
  private val default_background_position: String = "0% 0%"
  private val default_background_repeat: String = "repeat"
  private val default_background_size: String = "auto"
  private val default_width: String = "auto"
  private val default_height: String = "auto"


  /**
   * position을 파싱하기위한 regex
   */
  private val positionPattern=("(left|right|center|(-|)[0-9]+(px|%|in|cm|mm|em|ex|pt|pc))" +
    "( (top|bottom|center|(-|)[0-9]+(px|%|in|cm|mm|em|ex|pt|pc))|)").r
  /**
   * imageUrl을 파싱하기위한 regex
   */
  private val imageUrlPattern= """url\(("|'|)([^)"']+)("|'|)\)""".r

  /**
   * repeat를 파싱하기위한 regex
   */
  private val repeatPattern="(no-repeat|repeat(-x|-y|))".r


  /**
   * 스타일 룰셋에서 background, width, height를 파싱
   * @param rulSet 파싱할 룰셋
   * @return 파싱된 background info
   */
  def extractBackgroundStyleRule(rulSet:Seq[CSSDeclerationWithEditor]):CssBackgroundInfo={

    var background_image: String = default_background_image
    var background_position: String = default_background_position
    var background_repeat: String = default_background_repeat
    var background_size: String = default_background_size
    var width: String = default_width
    var height: String = default_height
    var ref_path: URL = null
    var background_image_ref: CSSDeclerationWithEditor = null
    var background_position_ref:CSSDeclerationWithEditor = null

    rulSet.foreach{rule=>
      val expressionStr = CSSContentConst.CssWriter.getCSSAsString(rule.decl.getExpression)
      rule.decl.getProperty match{
        case "background"=>
          //background의 경우 여러 속성이 합쳐져 있기 때문에 적용이 가능하다.
          val image = imageUrlPattern.findFirstIn(expressionStr).getOrElse(default_background_image)
          val position = positionPattern.findFirstIn(expressionStr).getOrElse(default_background_position)
          val repeat = repeatPattern.findFirstIn(expressionStr).getOrElse(default_background_repeat)
          background_image=image
          background_position=position
          background_repeat=repeat
          background_size=default_background_size
          ref_path = rule.url
          background_image_ref=rule
          background_position_ref=rule

        case "background-image"=>
          background_image=expressionStr
          ref_path = rule.url
          background_image_ref=rule
        case "background_position"=>
          background_position=expressionStr
          background_position_ref=rule
        case "background_repeat"=>  background_repeat=expressionStr
        case "background_size"=>    background_size=expressionStr
        case "width"=>              width=expressionStr
        case "height"=>             height=expressionStr
        case _=>
      }
    }
    new CssBackgroundInfo(
      background_image,
      background_position,
      background_repeat,
      background_size, width,
      height,
      ref_path,
      background_image_ref,
      background_position_ref
    )
  }

  /**
   * Sprite할 수 있는가 검사를 한다.
   * repeat가 있거나, url이 아니거나, base64로 인코딩된것은 불가하다.
   * @param obj 검사할  background info
   * @return t/f
   */
  private[sprite] def canSprite(obj: CssBackgroundInfo): Boolean = {
    if (obj.background_repeat == "repeat") return false
    if (!obj.background_image.toLowerCase.contains("url")) return false
    if (obj.background_image.contains(";base64,")) return false
    true
  }

  /**
   * px를 파싱하기 위한 regex
   */
  private val pxPattern = "((-|)[0-9]+)px".r
  /**
   * %를 파싱하기위한 regex
   */
  private val percentPattern ="((-|)[0-9]+)%".r

  /**
   * Left 와 Top Margin을 추측한다.
   * todo issue :: inherit, initial 처리.
   * @param str 파싱할 스트링
   * @param imageLength 적용할 image의 크기 (width 혹은 height)
   * @param targetLength 적용될 Dom의 크기 (width 혹은 height)
   * @return 추측한 값
   */
  private[sprite] def guessLeftTopMargin(str:String, imageLength:Int, targetLength:String):Option[Int]={
    var percent=1.0f
    str match{
      case pxPattern(pxStr,_)=>
        if(pxStr.toInt<0)
          return Some(0)
        return Some(pxStr.toInt)
      case "center"=>percent=0.5f
      case "top"=>return Some(0)
      case "bottom"=> percent=1.0f
      case "left"=> return Some(0)
      case "right"=> percent=1.0f
      case percentPattern(pxStr,_)=>
        percent= pxStr.toFloat /100
    }

    val tarMatch=pxPattern.findFirstMatchIn(targetLength)
    if(tarMatch.isEmpty) return None
    Some(((tarMatch.get.group(1).toFloat-imageLength.toFloat)*percent).toInt)
  }

  /**
   * Right 와 Bottom Margin을 추측한다.
   * todo issue :: inherit, initial 처리.
   * @param str 파싱할 스트링
   * @param imageLength 적용할 image의 크기 (width 혹은 height)
   * @param targetLength 적용될 Dom의 크기 (width 혹은 height)
   * @return 추측한 값
   */
  private def guessRightBottomMargin(str:String, imageLength:Int, targetLength:String):Option[Int]={

    val tarMatch=pxPattern.findFirstMatchIn(targetLength)
    val   tarLen = if(tarMatch.isEmpty) -1 else tarMatch.get.group(1).toInt

    str match{
      case pxPattern(pxStr,_)=>
        if(tarLen<0)
          None
        else {
          if (pxStr.toInt < 0)
            Some(tarLen - imageLength)
          val last = tarLen - pxStr.toInt - imageLength
          if (last < 0)
            Some(0)
          Some(last)
        }

      case "center"=>
        if(tarLen<0)
          None
        else
          Some((tarLen-imageLength)/2)
      case "top"=>
        if(tarLen<0)
          None
        else
          Some(tarLen-imageLength)

      case "bottom"=>
        Some(0)

      case "left"=>
        if(tarLen<0)
          None
        else
          Some(tarLen-imageLength)

      case "right"=>
        Some(0)

      case percentPattern(pxStr,_)=>
        val percent= pxStr.toFloat /100
        if(percent==1) return Some(0)
        if(tarLen<0)
          None
        else
          Some(((tarLen-imageLength).toFloat*percent).toInt)

    }
  }

  /**
   * CssBackground Info를 받아 실제 Margin으로 파싱/연산 해준다.
   * @param backInfo 연산할 CssBackgroundInfo
   * @return 연산결과
   */
  private def parseCSSBackground(backInfo:CssBackgroundInfo,imageContent:ImageContent)={
    val width = imageContent.width
    val height = imageContent.height

    val posString = backInfo.background_position.split(" ")

    val margin_left = guessLeftTopMargin(posString(0),width,backInfo.width).getOrElse(-1)
    val margin_top =
      if(posString.size>1)
        guessLeftTopMargin(posString(1),height,backInfo.height).getOrElse(-1)
      else
        0

    val margin_right = guessRightBottomMargin(posString(0),width,backInfo.width).getOrElse(-1)

    val margin_bottom =
      if(posString.size>1)
        guessRightBottomMargin(posString(1),height,backInfo.height).getOrElse(-1)
      else
        guessRightBottomMargin("0px",height,backInfo.height).getOrElse(-1)

    new SpriteInfo(imageContent.dbRow.contentHash,imageContent.dbRow.originUrlStr,margin_top,margin_right,margin_left,margin_bottom,width,height)
  }


  /**
   * CssBackground 정보를 분리해서 저장해놓은 케이스 클래스
   */
  case class CssBackgroundInfo(background_image: String,
                               background_position: String,
                               background_repeat: String,
                               background_size: String,
                               width:String,
                               height:String,
                               ref_path:URL,
                               background_image_ref:CSSDeclerationWithEditor,
                               background_position_ref:CSSDeclerationWithEditor)

  case class CSSDeclerationWithEditor(decl: CSSDeclaration, url: URL, addFunc: CSSDeclaration => Unit, setChanged: () => Unit)



  def calcSpriteInfo(backInfo:CssBackgroundInfo,
                     getImage:(String => Option[ImageContent]) =
                     {url:String => Option(WebContent(new URL(url)).asInstanceOf[ImageContent])}):Option[SpriteInfo]={
    if(canSprite(backInfo)){
      val img = getImage(backInfo.background_image.substring(4,backInfo.background_image.size-1))
      img.map(parseCSSBackground(backInfo,_))
    }
    else
      None
  }

  implicit val SpriteInfoWrites:Writes[SpriteInfo] = (
    (JsPath \ "imageHashcode").write[String] and
      (JsPath \ "originImgUrl").write[String] and
      (JsPath \ "top").write[Int] and
      (JsPath \ "right").write[Int] and
      (JsPath \ "left").write[Int] and
      (JsPath \ "bottom").write[Int] and
      (JsPath \ "width").write[Int] and
      (JsPath \ "height").write[Int]
    )(unlift(SpriteInfo.unapply))

  implicit val SpriteInfoReads:Reads[SpriteInfo] = (
    (JsPath \ "imageHashcode").read[String] and
      (JsPath \ "originImgUrl").read[String] and
      (JsPath \ "top").read[Int] and
      (JsPath \ "right").read[Int] and
      (JsPath \ "left").read[Int] and
      (JsPath \ "bottom").read[Int] and
      (JsPath \ "width").read[Int] and
      (JsPath \ "height").read[Int]
    )(SpriteInfo.apply _)
}

case class SpriteInfo(imageHashcode:String, originImgUrl:String, top : Int, right : Int, left : Int, bottom : Int, width : Int , height : Int)
