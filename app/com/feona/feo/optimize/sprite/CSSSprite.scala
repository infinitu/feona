package com.feona.feo.optimize.sprite

import java.awt.Graphics
import java.awt.image.BufferedImage
import java.io.OutputStream
import java.sql.Timestamp
import javax.imageio.ImageIO

import com.feona.feo.Util
import com.feona.feo.io.GenContentManager
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.webcontents.impl.{HtmlContent, ImageContent}
import com.feona.feo.webcontents.{ContentType, WebContent}
import com.helger.css.decl.{CSSDeclaration, CSSExpression}
import models.{WebContent => dbWebContent, _}
import org.jsoup.nodes.Element
import org.jsoup.parser.Tag
import scala.collection.mutable
import scala.slick.jdbc.JdbcBackend

import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick.DB

/**
 * Created by CK-Kim on 2014. 8. 24..
 */
object CSSSprite {

  case class Position(x :Int, y : Int)
  case class spriteInfoMapping(imgContent:ImageContent,info:SpriteInfo)


  private val intervalPixel = 5


  def mkSprite(projectId:Int, spriteList:Seq[SpriteInfo])=DB.withSession{implicit session=>

    val timeStamp = new Timestamp(System.currentTimeMillis)

    val newOptRuleId = OptimizeRules.tQuery.returning(OptimizeRules.tQuery.map(_.optimizeId)) +=
      OptimizeRule(0, projectId,OptimizeType.CssSprite, timeStamp)

    val newOptRule= OptimizeRule(newOptRuleId, projectId,OptimizeType.CssSprite, timeStamp)

    var spriteRule:Seq[CssSpriteRule] = null
    val spriteResult = GenContentManager.save(ContentType.IMAGE_PNG,projectId,deployPath = Some("/sprite/sprite%d.png".format(newOptRule.optimizeId))) { stream =>
      val spriteMap = createImage(stream, spriteList)


      spriteRule =
        spriteMap.toSeq.map{row => new CssSpriteRule( row._1.info.imageHashcode,
          newOptRuleId,
          row._2.x, row._2.y,
          row._1.info.width, row._1.info.height)}
      CssSpriteRules.tQuery ++= spriteRule
    }

    CssSpriteResults.tQuery += CssSpriteResult(newOptRule.optimizeId, spriteResult.contentHash)

    Invoker.invoke(newOptRule,spriteRule,spriteResult) _
  }

  /**
   * CSS내부에 사용하는 이미지들의 정보를 입력하면 Sprite된 이미지를 생성.
   * @param combinedImageOutput 합친 결과물의 출력 위치
   * @param spriteList Sprite할때 SpriteInfo
   * @return 이미지 배치 정보
   */
  def createImage(combinedImageOutput : OutputStream, spriteList:Seq[SpriteInfo]):mutable.HashMap[spriteInfoMapping,Position]={

    val hashList = spriteList.map(_.imageHashcode)
    val imgList:Seq[ImageContent] = WebContent.withHashes(hashList).map(_.asInstanceOf[ImageContent])
    val imagePositionMap = mutable.HashMap[spriteInfoMapping,Position]()

    val imgInfoMap=
      for{
        i<-imgList
        j<-spriteList if i.dbRow.contentHash == j.imageHashcode
      } yield new spriteInfoMapping(i,j)

    //nextImagePos 는 다음에 이미지가 들어가야할 곳을 추론한 위치.
    var nextImagePosY = 0
    var nextImagePosX = 0
    var maxHeightOfHrizontal = 0
    var maxWidthOfVertical = 0
    var previousImageBottom = Int.MaxValue
    var previousImageRight= Int.MaxValue

    //세로로 일자로 배열
    imgInfoMap.filter(_.info.bottom>=0) foreach { sprite =>
      if(previousImageBottom<sprite.info.top)
        nextImagePosY+=  sprite.info.top-previousImageBottom

      imagePositionMap+=sprite->new Position(nextImagePosX,nextImagePosY)

      nextImagePosY += sprite.info.bottom + intervalPixel+sprite.info.height
      previousImageBottom = sprite.info.bottom

      //일렬로 붙이는 것중에 너비가 제일 넓은것을 구함.
      if(maxWidthOfVertical< sprite.info.width)
        maxWidthOfVertical= sprite.info.width
    }

    //bottom이 무한대일경우 가로로 배치.
    imgInfoMap.filter(_.info.bottom < 0) foreach { sprite =>
      if(previousImageRight< sprite.info.left)
        nextImagePosX+= sprite.info.left-previousImageRight

      imagePositionMap+=sprite->new Position(nextImagePosX,nextImagePosY)

      nextImagePosX +=  sprite.info.right+intervalPixel+ sprite.info.width
      previousImageRight =  sprite.info.right

      if(maxHeightOfHrizontal< sprite.info.height)
        maxHeightOfHrizontal= sprite.info.height
    }

    //todo Issue::마지막줄에 top margin이 그 위쪽의 bottom margin보다 클경우를 예상하지 못함.
    //todo Issue::마지막줄에서 Left or Right가 무한대일경우는 예상하지 못함.

    //마지막 전체 사진 크기
    val canvasHeight:Int = nextImagePosY+maxHeightOfHrizontal
    val canvasWidth:Int = math.max(nextImagePosX-previousImageBottom,maxWidthOfVertical)

    // create image
    // 이때 TYPE_4BYTE_ABGR등의 옵션으로 화질을 줄 수 있다.
    //combine시 gif, jpg의 경우는 BufferedImage.TYPE_INT_RGB의 파라미터를 사용해야하고(jpg 경우 배경이 검은색)
    //png의 경우에는 BufferedImage.TYPE_INT_ARGB라고 명시해주어야 배경이 투명하게 나옴!
    //default로 png로 잡고 개발
    val combinedImage : BufferedImage = new BufferedImage( canvasWidth, canvasHeight, BufferedImage.TYPE_INT_ARGB )
    // merge image
    val mergeGraphics : Graphics = combinedImage.getGraphics
    imagePositionMap.foreach { image =>
      mergeGraphics.drawImage(image._1.imgContent.image,
        image._2.x, image._2.y, null)
    }

    ImageIO.write(combinedImage, "PNG", combinedImageOutput)

    imagePositionMap
  }

  private object Invoker{

    def invoke(optimizeRule:OptimizeRule, spriteRules:Seq[CssSpriteRule],spriteResult:dbWebContent)(htmlContents: Seq[HtmlContent])={
      //SpriteRule을 로딩
      val cssR = spriteRules

      //로딩된 룰을 사용하기 쉽게 해쉬로 변환
      val objH = WebContent.withHashes(cssR.map(_.contentHash)).map(x => x.dbRow.contentHash -> x.dbRow).toMap
      val urlH = cssR.map(x => objH.get(x.contentHash).get.originUrlStr -> x).toMap

      //이 파일로 변경될 예정
      val dest = spriteResult
      val wcDest = WebContent.ImageContent(dest)
      val newSpriteInfo = SpriteInfo(dest.contentHash,dest.originUrlStr,-1,-1,-1,-1,wcDest.width,wcDest.height)
      val imageHashes = spriteRules.map(_.contentHash)
      htmlContents.foreach { html =>

        html.spritableInfo --=
          html.spritableInfo.filter(x=>imageHashes.contains(x._2.imageHashcode))

        html.styleHash
          .map { case (node, style) => node -> SpriteInfo.extractBackgroundStyleRule(style.toSeq)}
          .filter(x => SpriteInfo.canSprite(x._2))
          .foreach { case (node, backg) =>
          //CSS Background 를 사용하고 있는 돔 엘리먼트
          val url = Util.AbsoluteURL(backg.ref_path, backg.background_image.substring(4, backg.background_image.size - 1))
          val ruleOpt = urlH.get(url.toString)
          if (ruleOpt.isDefined) {
            //그중에 대상이 되는 아이
            val rule = ruleOpt.get
            //CSS 변경
            optimizeCssBackground(backg, rule, dest)
            html.spritableInfo += node->newSpriteInfo
          }
        }

        //img Tag를 모두 변경
        html.imgTagList.foreach { x =>
          val src = x.attr("src")
          val ruleOpt = urlH.get(Util.AbsoluteURL(html.dbRow.originUrl, src).toString)
          if (ruleOpt.isDefined) {
            val rule = ruleOpt.get
            x.removeAttr("src")
            x.tagName("div")
            x.children().add(new Element(Tag.valueOf("span"), x.baseUri()))
            html.setChanged()
            val backgStyle = "background:url('%s') %dpx %dpx;width:%dpx;height:%dpx".format(dest.originUrl.toString, -rule.posX, -rule.posY, rule.width, rule.height)
            x.attr("style", backgStyle)
            html.spritableInfo += x->newSpriteInfo
          }
        }
      }
      optimizeRule
    }

  }

  /**
   * 실제 CSS에 Sprite를 적용
   * @param back_info 변경할 Background의 정보
   * @param opt_info 변경 Rule
   * @param dest 목적이 되는 이미지
   */
  def optimizeCssBackground(back_info:SpriteInfo.CssBackgroundInfo, opt_info:CssSpriteRule, dest:models.WebContent){

    //적혀 있는 포지션
    val posString = back_info.background_position.split(" ")

    //현재 적용되어있는 포지션 파싱
    val oriX = SpriteInfo.guessLeftTopMargin(posString(0),opt_info.width,back_info.width).getOrElse(0)
    val oriY =
      if(posString.size>1)
        SpriteInfo.guessLeftTopMargin(posString(1),opt_info.height,back_info.height).getOrElse(0)
      else
        0


    //적용
    back_info.background_image_ref.decl.getProperty match{
      case "background_image" if back_info.background_position_ref == null =>
        back_info.background_image_ref.decl.setExpression(CSSExpression.createSimple("url(\""+dest.originUrl+"\")"))
        back_info.background_image_ref.addFunc(new CSSDeclaration("background_position",CSSExpression.createSimple(-opt_info.posX+"px "+(-opt_info.posY)+"px")))
      case "background_image"=>
        back_info.background_image_ref.decl.setExpression(CSSExpression.createSimple("url(\""+dest.originUrl+"\")"))
        back_info.background_position_ref.decl.setExpression(CSSExpression.createSimple((oriX-opt_info.posX)+"px "+(oriY-opt_info.posY)+"px"))
        back_info.background_image_ref.setChanged()
        back_info.background_position_ref.setChanged()
      case "background" if back_info.background_image_ref == back_info.background_position_ref=>
        back_info.background_image_ref.decl.setExpression(CSSExpression.createSimple("url(\""+dest.originUrl+"\") "+(oriX-opt_info.posX)+"px "+(oriY-opt_info.posY)+"px"))
        back_info.background_image_ref.setChanged()
      case "background"=>
        back_info.background_image_ref.decl.setExpression(CSSExpression.createSimple("url(\""+dest.originUrl+"\")"+ back_info.background_position + back_info.background_repeat))
        back_info.background_position_ref.decl.setExpression(CSSExpression.createSimple((oriX-opt_info.posX)+"px "+(oriY-opt_info.posY)+"px"))
        back_info.background_image_ref.setChanged()
        back_info.background_position_ref.setChanged()
      case _=>
    }
  }
}
