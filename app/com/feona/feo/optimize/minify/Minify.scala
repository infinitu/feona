package com.feona.feo.optimize.minify

import java.io.File
import java.sql.Timestamp

import com.feona.feo.optimize.replace.Replace
import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.optimize.{OptimizeType, OptimizeInvoker}
import com.feona.feo.webcontents.WebContent
import com.feona.feo.webcontents.impl.{ExternalJavascriptContent, ExternalCSSContent, HtmlContent}
import models.{MinifyRule, MinifyRules, OptimizeRules, OptimizeRule,WebContent => dbWebContent}
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._

import scala.slick.jdbc.JdbcBackend

/**
 * Created by CK-Kim on 2014. 8. 26..
 */
object Minify {
  def mkMinify(projectId:Int,contentHashList:Seq[String])=DB.withSession{implicit session=>
    val newOptRuleID = OptimizeRules.tQuery.returning(OptimizeRules.tQuery.map(_.optimizeId)) +=
      OptimizeRule(0, projectId,OptimizeType.Minify, new Timestamp(System.currentTimeMillis))
    val newOptRule = OptimizeRule(newOptRuleID, projectId,OptimizeType.Minify, new Timestamp(System.currentTimeMillis))
    MinifyRules.tQuery ++= contentHashList.map(MinifyRule(_,newOptRule.optimizeId))
    val oldAndNew = createMinContent(contentHashList)
    Invoker.invoke(newOptRule,oldAndNew) _
  }

  def createMinContent(contentHashList:Seq[String], removeImports:Boolean = true):Seq[OldAndNew]={
    val contents = WebContent.withHashes(contentHashList)
    contents.map{
      case css:ExternalCSSContent=>
        CSSMinify.CSSMinExport(css, removeImports)
      case js:ExternalJavascriptContent=>
        JSMinify.JSMinExport(js)
    }
  }
  
  private object Invoker{
    def invoke(optimizeRule:OptimizeRule, oldAndNew:Seq[OldAndNew])(htmlContents: Seq[HtmlContent]) = DB.withSession{ implicit session=>
      oldAndNew.map(OnN=>Replace(OnN.oldContent,OnN.newContent)).foreach(htmlContents.foreach)
      optimizeRule
    }
  }
  
}
