package com.feona.feo.optimize.minify

import java.io.File

import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.webcontents.impl.{JavascriptContent, ExternalJavascriptContent}
import play.api.Play.current
import play.api.libs.ws.WS
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
 * Created by CK-Kim on 2014. 8. 26..
 */
private[minify] object JSMinify {
  def JSMinExport(js:ExternalJavascriptContent):OldAndNew = {
    val old = js.dbRow
    val minify = Await.result(JSMinify(js.toString),30000 millis)
    js.minify=true
    js.contentChange(minify)
    OldAndNew(old,js.databaseExport())
  }

  private[minify] def JSMinify(code:String):Future[String] = {
    val ws = WS.client
    ws.url("http://closure-compiler.appspot.com/compile")
      .post(Map(
        "js_code" -> Seq(code),
        "output_info" -> Seq("compiled_code"),
        "output_format" -> Seq("text"),
        "compilation_level" -> Seq("SIMPLE_OPTIMIZATIONS")
      ))
    .map{ response=>
      response.body
    }
  }

}
