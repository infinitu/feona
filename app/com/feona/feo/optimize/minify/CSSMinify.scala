package com.feona.feo.optimize.minify

import java.io.File

import scala.collection.JavaConversions._

import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.webcontents.impl.{CSSContent, ExternalCSSContent}

/**
 * Created by CK-Kim on 2014. 8. 26..
 */
private[minify] object CSSMinify {
  def CSSMinExport(css:ExternalCSSContent, removeImports:Boolean):OldAndNew = {
    val old = css.dbRow
    if(removeImports) CSSRemoveImport(css)
    CSSMinify(css)
    OldAndNew(old,css.databaseExport())
  }

  def CSSRemoveImport(css:CSSContent){
    val allRules = css.allStyleRules
    css.parsedCSS.getAllStyleRules.clear()
    css.parsedCSS.removeAllImportRules()
    css.setChanged()
    allRules.foreach(r=>css.parsedCSS.addRule(r._1))
  }

  def CSSMinify(css:CSSContent){
    css.minify=true
    css.setChanged()
  }

}
