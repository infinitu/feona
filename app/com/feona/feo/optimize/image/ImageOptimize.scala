package com.feona.feo.optimize.image

import java.awt.image.BufferedImage
import java.io._
import java.nio.ByteBuffer
import java.sql.Timestamp
import java.util.{Iterator=>JIterator}
import javax.imageio.stream.ImageOutputStream
import javax.imageio.{IIOImage, ImageWriteParam, ImageWriter, ImageIO}

import com.feona.feo.io.GenContentManager
import com.feona.feo.optimize.OptimizeType
import com.feona.feo.optimize.replace.Replace
import com.feona.feo.optimize.replace.Replace.OldAndNew
import com.feona.feo.webcontents.ContentType
import com.feona.feo.webcontents.impl.HtmlContent
import com.googlecode.pngtastic.core.{PngImage, PngOptimizer}
import models.{WebContent => dbWebContent, OptimizeRules, OptimizeRule}
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple._
import play.api.db.slick._

import scala.slick.jdbc.JdbcBackend


/**
 * Created by CK-Kim on 2014. 8. 28..
 */
object ImageOptimize {

  private val pngOptimizer = new PngOptimizer("NONE")

  private def pngOptimize(origin:File,output:OutputStream,removeGamma:Boolean=true, compressionLevel : Int = 5){
    val image = new PngImage(origin.getAbsolutePath)
    val optimized = pngOptimizer.optimize(image, removeGamma, compressionLevel)

    val optimizedBytes = new ByteArrayOutputStream()
    val optimizedSize = optimized.writeDataOutputStream(optimizedBytes).size()

    val originalFileSize = origin.length()

    val optimalBytes = if(optimizedSize < originalFileSize) optimizedBytes.toByteArray
                          else getFileBytes(origin, originalFileSize)

    output.write(optimalBytes)
    output.flush()
  }

  private def getFileBytes(originalFile:File, originalFileSize:Long) = {
    val buffer = ByteBuffer.allocate(originalFileSize.asInstanceOf[Int])
    val ins = new FileInputStream(originalFile)
    try {
      ins.getChannel.read(buffer)
    } finally {
      if (ins != null) {
        ins.close()
      }
    }
    buffer.array()
  }

  private def jpegCompress(origin:File, output:OutputStream, compressionLevel:Int = 5) = {

    val is : InputStream = new FileInputStream(origin)

    val quality : Float = 0.1f * compressionLevel

    // create a BufferedImage as the result of decoding the supplied InputStream
    val image : BufferedImage = ImageIO.read(is)

    // get all image writers for JPG format
    val writers : JIterator[ImageWriter] = ImageIO.getImageWritersByFormatName("jpg")

    if (!writers.hasNext)
      throw new IllegalStateException("No writers found")

    val writer : ImageWriter = writers.next()
    val ios : ImageOutputStream = ImageIO.createImageOutputStream(output)
    writer.setOutput(ios)

    val param : ImageWriteParam = writer.getDefaultWriteParam

    // compress to a given quality
    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT)
    param.setCompressionQuality(quality)

    // appends a complete image stream containing a single image and
    //associated stream and image metadata and thumbnails to the output
    writer.write(null, new IIOImage(image, null, null), param)

    // close all streams
        is.close()
    output.close()
    ios.close()
    writer.dispose()
  }

  def mkResampleImage(projectId:Int, webContent:Seq[dbWebContent],compressionLevel:Int=5)(implicit session:JdbcBackend.Session)={
    val newOptRule = OptimizeRules.tQuery.returning(OptimizeRules.tQuery) +=
      new OptimizeRule(0, projectId,OptimizeType.Image, new Timestamp(System.currentTimeMillis))

    //TODO Migration용 DB등록.

    val oldAndNew =
      webContent.filter(_.contentType == ContentType.IMAGE_PNG).map{content=>
        val newContent:dbWebContent =
          GenContentManager.save(ContentType.IMAGE_PNG,content.projectId){stream=>
            pngOptimize(content.localFile,stream,compressionLevel=compressionLevel)
          }
        OldAndNew(content,newContent)
      } ++
      webContent.filter(_.contentType == ContentType.IMAGE_JPEG).map{content=>
        val newContent:dbWebContent =
          GenContentManager.save(ContentType.IMAGE_JPEG,content.projectId){stream=>
            jpegCompress(content.localFile,stream,compressionLevel=compressionLevel)
          }
        OldAndNew(content,newContent)
      }
    Invoker.invoke(newOptRule,oldAndNew) _
  }

  private object Invoker{
    def invoke(optimizeRule:OptimizeRule, oldAndNew:Seq[OldAndNew])(htmlContents: Seq[HtmlContent])={
      oldAndNew.map(OnN=>Replace(OnN.oldContent,OnN.newContent)).foreach(htmlContents.foreach)
      optimizeRule
    }
  }

}
