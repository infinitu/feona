package com.feona.feo

import java.io.File
import java.net.URL
import java.nio.charset.MalformedInputException

import scala.collection.mutable
import scala.io.Source

/**
 * Created by CK-Kim on 2014. 8. 25..
 */
object Util {

  /**
   * full url regex pattern
   */
  val urlPattern = "((http)|(https)|.+)://.+".r
  /**
   * url path regex pattern
   */
  val pathPattern = "(/)(.*)".r
  /**
   * url scheme regex pattern
   */
  val schemePattern = "(//)(.*)".r

  def AbsoluteURL(hostUrl:URL,subUrl:String):URL = subUrl match {
    case urlPattern(_,_,_)=>new URL(subUrl)
    case schemePattern(mark,url)  => new URL(hostUrl.getProtocol+"://"+url)
    case pathPattern(mark,paths)  => new URL(hostUrl.getProtocol,hostUrl.getHost,hostUrl.getPort,subUrl)
    case _          =>
      val pathStack = mutable.Stack[String]()
      hostUrl.getPath.split('/').foreach{x=>pathStack push x}
      if(pathStack.nonEmpty)pathStack.pop()
      subUrl.split('/').foreach {
        case ".." => if (pathStack.nonEmpty) pathStack.pop()
        case "." =>
        case x => pathStack push x
      }

      val pathBuilder = mutable.StringBuilder.newBuilder
      pathStack.reverse.foreach{x=>if(!x.equals("")){pathBuilder.append("/");pathBuilder.append(x)}}

      new URL(hostUrl.getProtocol,hostUrl.getHost,hostUrl.getPort,pathBuilder.mkString)
  }

  def fileToString(file:File):String = {
    try{
      Source.fromFile(file,"UTF-8").mkString
    }catch {
      case t:MalformedInputException =>
        Source.fromFile(file,"ISO_8859_1").mkString
    }
  }

  def RelativeURL(hostURL:URL,abstractURL:URL):String={
    if(!hostURL.getProtocol.equals(abstractURL.getProtocol))
      abstractURL.toString
    else if(!hostURL.getHost.equals(abstractURL.getHost))
      "//"+abstractURL.getHost+abstractURL.getPath
    else
      abstractURL.getPath
  }

  implicit def URL2String(url:URL):String = url.toString
  implicit def String2URL(str:String):URL = new URL(str)

}
