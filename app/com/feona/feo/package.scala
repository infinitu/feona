package com.feona

/**
 * Created by CK-Kim on 2014. 9. 1..
 */
package object feo {
  implicit def wrapper2Int(wrap:ProjectIdWrapper)=wrap.value
  implicit def int2Wrapper(pid:Int)=ProjectIdWrapper(pid)
  case class ProjectIdWrapper(value:Int)
}
