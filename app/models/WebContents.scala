package models

import java.io.File
import java.net.URL
import com.feona.feo.Util
import com.feona.feo.webcontents.ContentType
import ContentType.ContentType
import com.feona.feo.webcontents.ContentType
import play.api.db.slick.Config.driver.simple._
import play.api.libs.json.Json

import scala.slick.jdbc.{GetResult => GR}
import scala.slick.model.ForeignKeyAction

/** Entity class storing rows of table WebContents
  *  @param contentHash Database column content_hash PrimaryKey
  *  @param originUrlStr Database column origin_url
  *  @param localPath Database column local_path
  *  @param contentTypeVal Database column content_type
  *  @param projectId Database column project_id  */
case class WebContent(contentHash: String, originUrlStr: String, localPath: String, deployPath: String, contentTypeVal: Int, projectId: Int, isGen: Boolean = true) {
  lazy val originUrl : URL = new URL(originUrlStr)
  lazy val localFile : File = new File(localPath)
  lazy val contentType : ContentType = ContentType.fromInt(contentTypeVal)

  def resetLocalPath(new_localPath : String) = new WebContent(this.contentHash, this.originUrlStr, new_localPath, this.deployPath, this.contentTypeVal, this.projectId, this.isGen)
  override def toString : String = {
    s"contentHash : $contentHash originUrl : $originUrl localPath : $localPath deployPath : $deployPath contentTypeVal : $contentTypeVal projectId : $contentTypeVal"
  }

  lazy val json = {
    Json.obj(
      "contentHash" -> this.contentHash,
      "originUrl" -> this.originUrlStr,
      "contentType" -> ContentType.fromInt(this.contentTypeVal).name,
      "deploy" -> this.deployPath,
      "deployAbs" -> Util.AbsoluteURL(this.originUrl,this.deployPath).toString
    )
  }
}
object WebContents {

  /** GetResult implicit for fetching WebContent objects using plain SQL queries */
  implicit def GetResultWebContent(implicit e0: GR[String], e1: GR[Int]): GR[WebContent] = GR{
    prs => import prs._
      WebContent.tupled((<<[String], <<[String], <<[String], <<[String], <<[Int], <<[Int], <<[Boolean]))
  }
  /** Collection-like TableQuery object for table WebContents */
  lazy val tQuery = new TableQuery(tag => new WebContents(tag))
}
/** Table description of table web_contents. Objects of this class serve as prototypes for rows in queries. */
class WebContents(tag: Tag) extends Table[WebContent](tag, "web_contents") {
  def * = (contentHash, originUrl, localPath, deployPath, contentType, projectId,isGen) <> (WebContent.tupled, WebContent.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (contentHash.?, originUrl.?, localPath.?, deployPath.?, contentType.?, projectId.?, isGen.?).shaped.<>({r=>import r._; _1.map(_=> WebContent.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column content_hash PrimaryKey */
  val contentHash: Column[String] = column[String]("content_hash", O.PrimaryKey)
  /** Database column origin_url  */
  val originUrl: Column[String] = column[String]("origin_url")
  /** Database column local_path  */
  val localPath: Column[String] = column[String]("local_path")
  /** Database column local_path  */
  val deployPath: Column[String] = column[String]("deploy_path")
  /** Database column content_type  */
  val contentType: Column[Int] = column[Int]("content_type")
  /** Database column project_id  */
  val projectId: Column[Int] = column[Int]("project_id")
  /** Database column is_gen  */
  val isGen: Column[Boolean] = column[Boolean]("is_gen")

  /** Foreign key referencing Projects (database name web_contents_ibfk_1) */
  lazy val projectsFk = foreignKey("web_contents_ibfk_1", projectId, Projects.tQuery)(r => r.projectId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}

