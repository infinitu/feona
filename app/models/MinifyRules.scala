package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table MinifyRules
  *  @param contentHash Database column content_hash
  *  @param optimizeId Database column optimize_id  */
case class MinifyRule(contentHash: String, optimizeId: Int)
object MinifyRules{
  /** GetResult implicit for fetching MinifyRule objects using plain SQL queries */
  implicit def GetResultMinifyRule(implicit e0: GR[String], e1: GR[Int]): GR[MinifyRule] = GR{
    prs => import prs._
      MinifyRule.tupled((<<[String], <<[Int]))
  }

  /** Collection-like TableQuery object for table MinifyRules */
  lazy val tQuery = new TableQuery(tag => new MinifyRules(tag))
}
/** Table description of table minify_rules. Objects of this class serve as prototypes for rows in queries. */
class MinifyRules(tag: Tag) extends Table[MinifyRule](tag, "minify_rules") {
  def * = (contentHash, optimizeId) <> (MinifyRule.tupled, MinifyRule.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (contentHash.?, optimizeId.?).shaped.<>({r=>import r._; _1.map(_=> MinifyRule.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column content_hash  */
  val contentHash: Column[String] = column[String]("content_hash")
  /** Database column optimize_id  */
  val optimizeId: Column[Int] = column[Int]("optimize_id")

  /** Foreign key referencing OptimizeRules (database name minify_rules_ibfk_2) */
  lazy val optimizeRulesFk = foreignKey("minify_rules_ibfk_2", optimizeId, OptimizeRules.tQuery)(r => r.optimizeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing WebContents (database name minify_rules_ibfk_1) */
  lazy val webContentsFk = foreignKey("minify_rules_ibfk_1", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)

  /** Index over (contentHash,optimizeId) (database name content_hash) */
  val index1 = index("content_hash", (contentHash, optimizeId))
}