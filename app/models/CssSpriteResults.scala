package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table CssSpriteResults
  *  @param optimizeId Database column optimize_id
  *  @param contentHash Database column content_hash  */
case class CssSpriteResult(optimizeId: Int, contentHash: String)

object CssSpriteResults {
  /** GetResult implicit for fetching CssSpriteResult objects using plain SQL queries */
  implicit def GetResultCssSpriteResult(implicit e0: GR[Int], e1: GR[String]): GR[CssSpriteResult] = GR{
    prs => import prs._
      CssSpriteResult.tupled((<<[Int], <<[String]))
  }

  /** Collection-like TableQuery object for table CssSpriteResults */
  lazy val tQuery = new TableQuery(tag => new CssSpriteResults(tag))
}
/** Table description of table css_sprite_results. Objects of this class serve as prototypes for rows in queries. */
class CssSpriteResults(tag: Tag) extends Table[CssSpriteResult](tag, "css_sprite_results") {
  def * = (optimizeId, contentHash) <> (CssSpriteResult.tupled, CssSpriteResult.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (optimizeId.?, contentHash.?).shaped.<>({r=>import r._; _1.map(_=> CssSpriteResult.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column optimize_id  */
  val optimizeId: Column[Int] = column[Int]("optimize_id")
  /** Database column content_hash  */
  val contentHash: Column[String] = column[String]("content_hash")

  /** Foreign key referencing OptimizeRules (database name css_sprite_results_ibfk_1) */
  lazy val optimizeRulesFk = foreignKey("css_sprite_results_ibfk_1", optimizeId, OptimizeRules.tQuery)(r => r.optimizeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing WebContents (database name css_sprite_results_ibfk_2) */
  lazy val webContentsFk = foreignKey("css_sprite_results_ibfk_2", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}

