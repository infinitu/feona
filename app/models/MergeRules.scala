package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}


/** Entity class storing rows of table MergeRules
  *  @param contentHash Database column content_hash
  *  @param optimizeId Database column optimizeid
  *  @param mergeIdx Database column merge_idx  */
case class MergeRule(contentHash: String, optimizeId: Int, mergeIdx: Int)
object MergeRules {
  /** GetResult implicit for fetching MergeRule objects using plain SQL queries */
  implicit def GetResultMergeRule(implicit e0: GR[String], e1: GR[Int]): GR[MergeRule] = GR{
    prs => import prs._
      MergeRule.tupled((<<[String], <<[Int], <<[Int]))
  }

  /** Collection-like TableQuery object for table MergeRules */
  lazy val tQuery = new TableQuery(tag => new MergeRules(tag))
}

/** Table description of table merge_rules. Objects of this class serve as prototypes for rows in queries. */
class MergeRules(tag: Tag) extends Table[MergeRule](tag, "merge_rules") {
  def * = (contentHash, optimizeId, mergeIdx) <> (MergeRule.tupled, MergeRule.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (contentHash.?, optimizeId.?, mergeIdx.?).shaped.<>({r=>import r._; _1.map(_=> MergeRule.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column content_hash  */
  val contentHash: Column[String] = column[String]("content_hash")
  /** Database column optimizeid  */
  val optimizeId: Column[Int] = column[Int]("optimizeid")
  /** Database column merge_idx  */
  val mergeIdx: Column[Int] = column[Int]("merge_idx")

  /** Foreign key referencing OptimizeRules (database name merge_rules_ibfk_2) */
  lazy val optimizeRulesFk = foreignKey("merge_rules_ibfk_2", optimizeId, OptimizeRules.tQuery)(r => r.optimizeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing WebContents (database name merge_rules_ibfk_1) */
  lazy val webContentsFk = foreignKey("merge_rules_ibfk_1", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)

  /** Index over (contentHash,optimizeId) (database name content_hash) */
  val index1 = index("content_hash", (contentHash, optimizeId))
}