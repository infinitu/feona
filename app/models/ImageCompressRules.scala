package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table ImageCompressRules
  *  @param contentHash Database column content_hash
  *  @param optimizeId Database column optimize_id  */
case class ImageCompressRule(contentHash: String, optimizeId: Int)
  object ImageCompressRules {
    /** GetResult implicit for fetching ImageCompressRule objects using plain SQL queries */
    implicit def GetResultImageCompressRule(implicit e0: GR[String], e1: GR[Int]): GR[ImageCompressRule] = GR{
      prs => import prs._
        ImageCompressRule.tupled((<<[String], <<[Int]))
    }

    /** Collection-like TableQuery object for table ImageCompressRules */
    lazy val tQuery = new TableQuery(tag => new ImageCompressRules(tag))

  }

/** Table description of table image_compress_rules. Objects of this class serve as prototypes for rows in queries. */
class ImageCompressRules(tag: Tag) extends Table[ImageCompressRule](tag, "image_compress_rules") {
  def * = (contentHash, optimizeId) <> (ImageCompressRule.tupled, ImageCompressRule.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (contentHash.?, optimizeId.?).shaped.<>({r=>import r._; _1.map(_=> ImageCompressRule.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column content_hash  */
  val contentHash: Column[String] = column[String]("content_hash")
  /** Database column optimize_id  */
  val optimizeId: Column[Int] = column[Int]("optimize_id")

  /** Foreign key referencing OptimizeRules (database name ioptimize_id) */
  lazy val optimizeRulesFk = foreignKey("ioptimize_id", optimizeId, OptimizeRules.tQuery)(r => r.optimizeId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing WebContents (database name icontent_hash) */
  lazy val webContentsFk = foreignKey("icontent_hash", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
}
