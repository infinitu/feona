package models

import java.net.URL

import com.feona.feo.webcontents.ContentType
import play.api.db.slick.Config.driver.simple._
import play.api.libs.json.Json
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table Pages
  *  @param pageId Database column page_id AutoInc, PrimaryKey
  *  @param projectId Database column project_id
  *  @param urlStr Database column url
  *  @param query Database column query
  *  @param header Database column header  */
case class Page(pageId: Int, projectId: Int, urlStr: String, query: String, header: String) {
  lazy val url = new URL(urlStr)

  lazy val json = {
    Json.obj(
      "pageId" -> this.pageId,
//      "projectId" -> this.projectId,
      "url" -> this.urlStr
//      "query" -> this.query,
//      "header" -> this.header
    )
  }
}
object Pages {
  /** GetResult implicit for fetching Page objects using plain SQL queries */
  implicit def GetResultPage(implicit e0: GR[Int], e1: GR[String]): GR[Page] = GR{
    prs => import prs._
      Page.tupled((<<[Int], <<[Int], <<[String], <<[String], <<[String]))
  }

  /** Collection-like TableQuery object for table Pages */
  lazy val tQuery = new TableQuery(tag => new Pages(tag))
}

/** Table description of table pages. Objects of this class serve as prototypes for rows in queries. */
class Pages(tag: Tag) extends Table[Page](tag, "pages") {
  def * = (pageId, projectId, url, query, header) <> (Page.tupled, Page.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (pageId.?, projectId.?, url.?, query.?, header.?).shaped.<>({r=>import r._; _1.map(_=> Page.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column page_id AutoInc, PrimaryKey */
  val pageId: Column[Int] = column[Int]("page_id", O.AutoInc, O.PrimaryKey)
  /** Database column project_id  */
  val projectId: Column[Int] = column[Int]("project_id")
  /** Database column url  */
  val url: Column[String] = column[String]("url")
  /** Database column query  */
  val query: Column[String] = column[String]("query")
  /** Database column header  */
  val header: Column[String] = column[String]("header")

  /** Foreign key referencing Projects (database name pages_ibfk_1) */
  lazy val projectsFk = foreignKey("pages_ibfk_1", projectId, Projects.tQuery)(r => r.projectId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}