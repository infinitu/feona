package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table Users
  *  @param userId Database column user_id AutoInc
  *  @param name Database column name
  *  @param pwd Database column pwd
  *  @param email Database column email
  *  @param phoneNumber Database column phoneNumber  */
case class User(userId: Int, name: String, pwd: String, email: String, phoneNumber: String)
object Users {
  /** GetResult implicit for fetching User objects using plain SQL queries */
  implicit def GetResultUser(implicit e0: GR[Int], e1: GR[String]): GR[User] = GR{
    prs => import prs._
      User.tupled((<<[Int], <<[String], <<[String], <<[String], <<[String]))
  }
  /** Collection-like TableQuery object for table Users */
  lazy val tQuery = new TableQuery(tag => new Users(tag))
}

/** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
class Users(tag: Tag) extends Table[User](tag, "users") {
  def * = (userId, name, pwd, email, phoneNumber) <> (User.tupled, User.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (userId.?, name.?, pwd.?, email.?, phoneNumber.?).shaped.<>({r=>import r._; _1.map(_=> User.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column user_id AutoInc */
  val userId: Column[Int] = column[Int]("user_id", O.AutoInc)
  /** Database column name  */
  val name: Column[String] = column[String]("name")
  /** Database column pwd  */
  val pwd: Column[String] = column[String]("pwd")
  /** Database column email  */
  val email: Column[String] = column[String]("email")
  /** Database column phoneNumber  */
  val phoneNumber: Column[String] = column[String]("phonenumber")

  /** Primary key of Users (database name users_PK) */
  val pk = primaryKey("users_PK", (userId, name))
}

