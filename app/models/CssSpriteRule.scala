package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table CssSpriteRules
  *  @param contentHash Database column content_hash
  *  @param optimizeId Database column optimize_id
  *  @param posX Database column pos_x
  *  @param posY Database column pos_y
  *  @param width Database column width
  *  @param height Database column height  */
case class CssSpriteRule(contentHash: String, optimizeId: Int, posX: Int, posY: Int, width: Int, height: Int)

object CssSpriteRules{
  /** GetResult implicit for fetching CssSpriteRulesRow objects using plain SQL queries */
  implicit def GetResultCssSpriteRule(implicit e0: GR[String], e1: GR[Option[Int]]): GR[CssSpriteRule] = GR{
    prs => import prs._
      CssSpriteRule.tupled((<<[String], <<[Int], <<[Int], <<[Int], <<[Int], <<[Int]))
  }

  /** Collection-like TableQuery object for table CssSpriteRules */
  lazy val tQuery = new TableQuery(tag => new CssSpriteRules(tag))
}

/** Table description of table css_sprite_rules. Objects of this class serve as prototypes for rows in queries. */
class CssSpriteRules(tag: Tag) extends Table[CssSpriteRule](tag, "css_sprite_rules") {
  def * = (contentHash, optimizeId, posX, posY, width, height) <> (CssSpriteRule.tupled, CssSpriteRule.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (contentHash.?, optimizeId.?, posX.?, posY.?, width.?, height.?).shaped.<>({r=>import r._; _1.map(_=> CssSpriteRule.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column content_hash  */
  val contentHash: Column[String] = column[String]("content_hash")
  /** Database column optimize_id  */
  val optimizeId: Column[Int] = column[Int]("optimize_id")
  /** Database column pos_x  */
  val posX: Column[Int] = column[Int]("pos_x")
  /** Database column pos_y  */
  val posY: Column[Int] = column[Int]("pos_y")
  /** Database column width  */
  val width: Column[Int] = column[Int]("width")
  /** Database column height  */
  val height: Column[Int] = column[Int]("height")

  /** Foreign key referencing OptimizeRules (database name merge_rules_ibfk_2) */
  lazy val optimizeRulesFk = foreignKey("css_sprite_rules_ibfk_1", optimizeId, OptimizeRules.tQuery)(r => r.optimizeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing WebContents (database name merge_rules_ibfk_1) */
  lazy val webContentsFk = foreignKey("css_sprite_rules_ibfk_2", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}