package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table DeployRules
  *  @param contentHash Database column content_hash PrimaryKey
  *  @param localPath Database column local_path  */
case class DeployRule(contentHash: String, localPath: String)

object DeployRules {
  /** GetResult implicit for fetching DeployRulesRow objects using plain SQL queries */
  implicit def GetResultDeployRule(implicit e0: GR[String]): GR[DeployRule] = GR{
    prs => import prs._
      DeployRule.tupled((<<[String], <<[String]))
  }
  /** Collection-like TableQuery object for table DeployRules */
  lazy val tQuery = new TableQuery(tag => new DeployRules(tag))
}

/** Table description of table deploy_rules. Objects of this class serve as prototypes for rows in queries. */
class DeployRules(tag: Tag) extends Table[DeployRule](tag, "deploy_rules") {
  def * = (contentHash, localPath) <> (DeployRule.tupled, DeployRule.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (contentHash.?, localPath.?).shaped.<>({r=>import r._; _1.map(_=> DeployRule.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column content_hash PrimaryKey */
  val contentHash: Column[String] = column[String]("content_hash", O.PrimaryKey)
  /** Database column local_path  */
  val localPath: Column[String] = column[String]("local_path")

  /** Foreign key referencing WebContents (database name dcontent_hash) */
  lazy val webContentsFk = foreignKey("dcontent_hash", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.Cascade)
}
