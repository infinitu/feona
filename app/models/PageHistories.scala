package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table PageHistories
  *  @param pageId Database column page_id
  *  @param contentHash Database column content_hash
  *  @param optId Database column opt_id  */
case class PageHistory(pageId: Int, contentHash: String, optId: Int)
object PageHistories {

  /** GetResult implicit for fetching PageHistory objects using plain SQL queries */
  implicit def GetResultPageHistory(implicit e0: GR[Int], e1: GR[String]): GR[PageHistory] = GR{
    prs => import prs._
      PageHistory.tupled((<<[Int], <<[String], <<[Int]))
  }
  /** Collection-like TableQuery object for table PageHistories */
  lazy val tQuery = new TableQuery(tag => new PageHistories(tag))
}
/** Table description of table page_histories. Objects of this class serve as prototypes for rows in queries. */
class PageHistories(tag: Tag) extends Table[PageHistory](tag, "page_histories") {
  def * = (pageId, contentHash, optId) <> (PageHistory.tupled, PageHistory.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (pageId.?, contentHash.?, optId.?).shaped.<>({r=>import r._; _1.map(_=> PageHistory.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column page_id  */
  val pageId: Column[Int] = column[Int]("page_id")
  /** Database column content_hash  */
  val contentHash: Column[String] = column[String]("content_hash")
  /** Database column opt_id  */
  val optId: Column[Int] = column[Int]("opt_id")

  /** Foreign key referencing OptimizeRules (database name page_histories_ibfk_3) */
  lazy val optimizeRulesFk = foreignKey("page_histories_ibfk_3", optId, OptimizeRules.tQuery)(r => r.optimizeId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing Pages (database name page_histories_ibfk_1) */
  lazy val pagesFk = foreignKey("page_histories_ibfk_1", pageId, Pages.tQuery)(r => r.pageId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
  /** Foreign key referencing WebContents (database name page_histories_ibfk_2) */
  lazy val webContentsFk = foreignKey("page_histories_ibfk_2", contentHash, WebContents.tQuery)(r => r.contentHash, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
}
