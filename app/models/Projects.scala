package models

import play.api.db.slick.Config.driver.simple._
import play.api.libs.json.{JsNull, JsObject, Json}
import scala.slick.model.ForeignKeyAction
// NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
import scala.slick.jdbc.{GetResult => GR}

/** Entity class storing rows of table Projects
  *  @param projectId Database column project_id AutoInc, PrimaryKey
  *  @param userId Database column user_id
  *  @param domain Database column domain
  *  @param crawlUrl Database column crawl_url
  *  @param snapshotId Database column snapshot_id
  *  @param thumbnail Database column thumbnail  */
case class Project(projectId: Int, userId: Int, domain: String, crawlUrl: Option[String], snapshotId: Int, thumbnail: Option[String]) {
  lazy val crawlServerUrl = crawlUrl.getOrElse(domain)
  lazy val json : JsObject= {
      Json.obj(
        "projectId" -> this.projectId,
//        "userId" -> this.userId,
        "domain" -> this.domain,
        "crawlUrl" -> this.crawlUrl.getOrElse(null)
//        "snapshotId" -> this.snapshotId,
//        "thumbnail" -> this.thumbnail.orNull
      )}
}
object Projects{

  /** GetResult implicit for fetching Project objects using plain SQL queries */
  implicit def GetResultProject(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]]): GR[Project] = GR{
    prs => import prs._
      Project.tupled((<<[Int], <<[Int], <<[String], <<?[String], <<[Int], <<?[String]))
  }

  /** Collection-like TableQuery object for table Projects */
  lazy val tQuery = new TableQuery(tag => new Projects(tag))
}
/** Table description of table projects. Objects of this class serve as prototypes for rows in queries. */
class Projects(tag: Tag) extends Table[Project](tag, "projects") {
  def * = (projectId, userId, domain, crawlUrl, snapshotId, thumbnail) <> (Project.tupled, Project.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (projectId.?, userId.?, domain.?, crawlUrl, snapshotId.?, thumbnail).shaped.<>({r=>import r._; _1.map(_=> Project.tupled((_1.get, _2.get, _3.get, _4, _5.get, _6)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column project_id AutoInc, PrimaryKey */
  val projectId: Column[Int] = column[Int]("project_id", O.AutoInc, O.PrimaryKey)
  /** Database column user_id  */
  val userId: Column[Int] = column[Int]("user_id")
  /** Database column domain  */
  val domain: Column[String] = column[String]("domain")
  /** Database column crawl_url  */
  val crawlUrl: Column[Option[String]] = column[Option[String]]("crawl_url")
  /** Database column snapshot_id  */
  val snapshotId: Column[Int] = column[Int]("snapshot_id")
  /** Database column thumbnail  */
  val thumbnail: Column[Option[String]] = column[Option[String]]("thumbnail")

  /** Foreign key referencing Users (database name projects_ibfk_1) */
  lazy val usersFk = foreignKey("projects_ibfk_1", userId, Users.tQuery)(r => r.userId, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)

  /** Uniqueness Index over (userId) (database name user_id) */
  val index1 = index("user_id", userId, unique=true)
}
